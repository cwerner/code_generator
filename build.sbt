import sbt.Keys.{libraryDependencies, scalacOptions, version}

val emfcommonVersion = "2.12.0"
val emfecoreVersion = "2.12.0"
val scrollVersion = "1.8"
val scoptVersion = "3.7.0"
val liftVersion = "3.3.0"
val gsonVersion = "2.8.5"

val modelmanagementprovider = ProjectRef(uri("https://git-st.inf.tu-dresden.de/cwerner/role_model_management_provider.git#master"), "modelmanagementprovider")
val uiProvider = ProjectRef(uri("https://git-st.inf.tu-dresden.de/cwerner/role_ui_provider.git#master"), "uiprovider")
//val modeljoin = ProjectRef(uri("https://git-st.inf.tu-dresden.de/cwerner/model_join.git#master"), "model_join")


javacOptions ++= Seq("-encoding", "UTF-8")

lazy val generator = (project in file("."))
  .settings(
    name := "CodeGenerator",
    version := "0.1",
    scalaVersion := "2.12.6",
    libraryDependencies ++= Seq(
      "com.github.max-leuthaeuser" %% "scroll" % scrollVersion,
      "org.scala-lang" % "scala-reflect" % scalaVersion.value,
      "org.scala-lang" % "scala-compiler" % scalaVersion.value,
      "org.eclipse.emf" % "org.eclipse.emf.common" % emfcommonVersion,
      "org.eclipse.emf" % "org.eclipse.emf.ecore" % emfecoreVersion,
      "com.github.scopt" %% "scopt" % scoptVersion,
      "net.liftweb" %% "lift-json" % liftVersion,
      "com.google.code.gson" % "gson" % gsonVersion,

      //"org.antlr" % "antlr4-runtime" % "4.7.2",

      "org.junit.platform" % "junit-platform-runner" % "1.0.0" % "test",
      "org.junit.jupiter" % "junit-jupiter-engine" % "5.0.0" % "test",
      "org.junit.vintage" % "junit-vintage-engine" % "4.12.0" % "test",
      "org.assertj" % "assertj-core" % "3.12.2" % "test",

      "net.aichler" % "jupiter-interface" % JupiterKeys.jupiterVersion.value % Test
    ),
    scalacOptions ++= Seq(
      "-language:implicitConversions"
    ),
    mainClass in assembly := Some("org.rosi_project.model_sync.generator.Generator"),
    assemblyMergeStrategy in assembly := {
      case "MANIFEST.MF" => MergeStrategy.first
      case "plugin.xml" => MergeStrategy.discard
      case "plugin.properties" => MergeStrategy.discard
      case "generated_package.exsd" => MergeStrategy.discard
      case "dynamic_package.exsd" => MergeStrategy.discard
      case PathList("schema", ps @ _ *) if ps.lastOption.exists(_.endsWith("generated_package.exsd")) => MergeStrategy.discard
      case PathList("schema", ps @ _ *) if ps.lastOption.exists(_.endsWith("dynamic_package.exsd")) => MergeStrategy.discard
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    },
  ).dependsOn(
    //modeljoin,
    modelmanagementprovider,
    uiProvider
  )
