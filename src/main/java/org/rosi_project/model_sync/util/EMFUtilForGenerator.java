package org.rosi_project.model_sync.util;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreEList;

public class EMFUtilForGenerator {
	
	static public List<EObject> getList(Object obj) {
		List<EObject> newList = new LinkedList<EObject>();
		EcoreEList<EObject> list = (EcoreEList<EObject>) obj;	
		for (EObject l : list) {
			newList.add(l);
		}
		return newList;
	}

}
