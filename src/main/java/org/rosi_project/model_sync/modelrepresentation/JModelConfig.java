package org.rosi_project.model_sync.modelrepresentation;

import java.util.LinkedList;
import java.util.List;

public class JModelConfig {
	
	public JModelConfig(String name) {
		this.name = name;		 
	}
	
	private String name;
	private JModel init;
	private List<JModel> integration;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public JModel getInit() {
		return init;
	}
	public void setInit(JModel init) {
		this.init = init;
	}
	
	public List<JModel> getIntegration() {
		return integration;
	}
	public boolean addIntegration(JModel value) {
		if (value == null)
			return false;
		if (integration == null)
			this.integration = new LinkedList<JModel>();
		return integration.add(value);
	}

}
