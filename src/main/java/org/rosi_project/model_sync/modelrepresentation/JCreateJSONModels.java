package org.rosi_project.model_sync.modelrepresentation;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import com.google.gson.Gson;

public class JCreateJSONModels {
	
	private static Gson gson = new Gson();

	public static void main(String[] args) {
		//Type t = gson.fromJson(bytesRead, Type.class);
		JCreateJSONModels creation = new JCreateJSONModels();
		
		//Create old TTC Model
		JModelConfig ttc = creation.createOldTTCModel();
		creation.writeJModelConfigToFile(ttc);
		
		//Create Family Model
		JModelConfig family = creation.createFamilyModel();
		creation.writeJModelConfigToFile(family);
		
		//Create Person Model
		JModelConfig person = creation.createPersonModel();
		creation.writeJModelConfigToFile(person);
		
		//Create SimplePerson Model
		JModelConfig simplePerson = creation.createSimplePersonModel();
		creation.writeJModelConfigToFile(simplePerson);		
		
		//Create Library Model
		JModelConfig library = creation.createLibraryModel();
		creation.writeJModelConfigToFile(library);
		
		//Create Shrinking Model
		JModelConfig shrinking = creation.createShrinkingModel();
		creation.writeJModelConfigToFile(shrinking);

	}
	
	public void writeJModelConfigToFile (JModelConfig value) {
		String s = gson.toJson(value);
		//System.out.println(s);
		try {
			PrintWriter p = new PrintWriter("assets/models/" + value.getName() + ".sync.json");
			p.write(s);
			p.flush();
			p.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public JModelConfig createShrinkingModel () {
		JModelConfig conf = new JModelConfig("ShrinkingModel");
		
		JModel classifier = new JModel("Classifier", "ShrinkingExample.Classifier");
		classifier.setImage("Library.png");	
		classifier.addAdditionalClass("ShrinkingExample.StructuredClassifier");
		classifier.addAdditionalClass("ShrinkingExample.EncapsulatedClassifier");
		classifier.addAdditionalClass("ShrinkingExample.Class");
		
		JModel element = new JModel("ConnectableElement", "ShrinkingExample.ConnectableElement");
		element.addAdditionalClass("ShrinkingExample.Property");
		classifier.addNested(element);
		
		conf.setInit(classifier);
		return conf;
	}
	
	public JModelConfig createLibraryModel () {
		JModelConfig conf = new JModelConfig("Library");
		
		JModel library = new JModel("Library", "LibraryExample.Library");
		library.setImage("Library.png");		
		
		JModel person = new JModel("Person", "LibraryExample.Person");
		person.addAdditionalClass("LibraryExample.Employee");
		library.addNested(person);
		
		conf.setInit(library);
		return conf;
	}
	
	public JModelConfig createOldTTCModel () {
		JModelConfig conf = new JModelConfig("TTC 17");
		
		JModel family = new JModel("ModelB", "ttc17.Family");
		family.setImage("family.png");
		JModel member = new JModel("Model B - members", "ttc17.Member");
		family.addNested(member);
		
		JModel person = new JModel("ModelA", "ttc17.Person");
		person.setRule("person.atl");
		person.addAdditionalClass("ttc17.Female");
		person.addAdditionalClass("ttc17.Male");
		
		JModel simpleperson = new JModel("ModelC", "ttc17.SimplePerson");
		
		conf.setInit(family);
		conf.addIntegration(person);
		conf.addIntegration(simpleperson);
		return conf;
	}
	
	public JModelConfig createFamilyModel () {
		JModelConfig conf = new JModelConfig("Family");
		
		JModel family = new JModel("FamilyModel", "Family.Family");
		family.setImage("Family.png");
		
		//family.addAdditionalClass("Family.Member");		
		JModel member = new JModel("MemberModel", "Family.Member");
		family.addNested(member);
		
		conf.setInit(family);
		return conf;
	}
	
	public JModelConfig createPersonModel () {
		JModelConfig conf = new JModelConfig("Person");
				
		JModel person = new JModel("PersonModel", "Person.Person");
		person.setImage("Person.png");
		person.addAdditionalClass("Person.Female");
		person.addAdditionalClass("Person.Male");		
		
		conf.setInit(person);
		return conf;
	}
	
	public JModelConfig createSimplePersonModel () {
		JModelConfig conf = new JModelConfig("SimplePerson");
				
		JModel simpleperson = new JModel("SimplePersonModel", "SimplePerson.SimplePerson");
		simpleperson.setImage("SimplePerson.png");
		
		conf.setInit(simpleperson);
		return conf;
	}	
	

}
