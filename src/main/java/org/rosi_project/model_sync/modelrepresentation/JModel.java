package org.rosi_project.model_sync.modelrepresentation;

import java.util.LinkedList;
import java.util.List;

public class JModel {
	
	public JModel(String name, String primaryClass) {
		this.name = name;
		this.primaryClass = primaryClass;
	}

	private String name;	
	private String primaryClass;
	private List<String> additionalClasses;
	private String image;
	private List<JModel> nested;
	private String rule;
		
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPrimaryClass() {
		return primaryClass;
	}
	public void setPrimaryClass(String primaryClass) {
		this.primaryClass = primaryClass;
	}
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	public List<String> getAdditionalClasses() {
		return additionalClasses;
	}
	public boolean addAdditionalClass(String value) {
		if (value == null)
			return false;
		if (additionalClasses == null)
			this.additionalClasses = new LinkedList<String>();
		return additionalClasses.add(value);
	}
	
	public List<JModel> getNested() {
		return nested;
	}
	public boolean addNested(JModel value) {
		if (value == null)
			return false;
		if (nested == null)
			this.nested = new LinkedList<JModel>();
		return nested.add(value);
	}
	
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
}
