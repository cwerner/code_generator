package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.acr_model.SModel
import org.rosi_project.model_sync.generator.sync.ReferenceMethodCreationVisitor

/**
 * Add a method to get all references in the RSUM case.
 *  Sync functions necessary because we need compartment functions in the naturals.
 *
 * @author Christopher Werner
 */
class SModelCombiRefMethodService {

  /**
   * Remove reference from model classes.
   *
   * @param sModel the model to augment
   */
  def prepareModel(sModel: SModel): Unit = {
    val refMethodVisitor = new ReferenceMethodCreationVisitor
    sModel.accept(refMethodVisitor)
  }
}