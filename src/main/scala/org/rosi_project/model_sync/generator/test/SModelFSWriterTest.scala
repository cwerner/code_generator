package org.rosi_project.model_sync.generator.test

import org.rosi_project.model_sync.generator.acr_model.{SimpleSModel, _}
import org.rosi_project.model_sync.generator.sync.{GetterSetterGeneratingVisitor, SyncEnhancingVisitor}
import scala.collection.Seq

/** @author Rico Bergmann
  */
object SModelFSWriterTest/* extends App */{

  val model = new SimpleSModel("Foo", "source", "http.//test.test")

  val stringType = SType("String")

  val personAttrs = Seq(SAttribute("name", stringType))
  val personSayHelloMethod = new SMethod("sayHello", stringType, Seq.empty, Seq(SMethodStatement("s\"Hello $name\"")))
  val personClass = new SClass("Person", "foo")
  personClass.setAttributes(personAttrs)
  personClass.addMethod(personSayHelloMethod)

  val getterSetterVisitor = new GetterSetterGeneratingVisitor
  val syncNotificationVisitor = new SyncEnhancingVisitor

  model.addModelClass(personClass)

  model.accept(getterSetterVisitor)
  model.accept(syncNotificationVisitor)
  //model.accept(new SModelFSWriter(workingDir = File("output/raw"), keepClassFiles = true))
}
