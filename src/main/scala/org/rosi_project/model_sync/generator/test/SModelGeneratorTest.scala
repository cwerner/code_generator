package org.rosi_project.model_sync.generator.test

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.{EObject, EPackage}
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.rosi_project.model_sync.generator.conversion.SModelGenerator

/**
  * @author Rico Bergmann
  */
object SModelGeneratorTest/* extends App */{
  
  val path: String = "assets/ttc17.ecore"
  
  val resourceSet = new ResourceSetImpl()
  val _ = resourceSet.getResourceFactoryRegistry.getExtensionToFactoryMap.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl())
  val res = resourceSet.getResource(URI.createFileURI(path), true)
  
  require(null != res)
  require(!res.getContents.isEmpty)
  println(s"We done: $res")

  res.getContents.toArray(new Array[EObject](0)).toList.find(_.isInstanceOf[EPackage]).foreach(e => (new SModelGenerator).convert(e.asInstanceOf[EPackage], "source"))
}
