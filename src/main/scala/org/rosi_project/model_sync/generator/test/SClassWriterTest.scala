package org.rosi_project.model_sync.generator.test

import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.sync.{GetterSetterGeneratingVisitor, SyncEnhancingVisitor}
import org.rosi_project.model_sync.generator.io.SClassWriter
import scala.collection.Seq

/**
  * @author Rico Bergmann
  */
object SClassWriterTest/* extends App */{

  val stringType = SType("String")
  val attrs = Seq(SAttribute("name", stringType))
  val sayHelloMethod = new SMethod("sayHello", stringType, Seq.empty, Seq(SMethodStatement("s\"Hello $name\"")))
  val modelClass = new SClass("Person", "foo")
  modelClass.setAttributes(attrs)
  modelClass.addMethod(sayHelloMethod)

  val getterSetterVisitor = new GetterSetterGeneratingVisitor
  modelClass.accept(getterSetterVisitor)
  
  val syncNotificationVisitor = new SyncEnhancingVisitor
  modelClass.accept(syncNotificationVisitor)

  val writer = new SClassWriter(modelClass)

  println(writer.stringify)

}
