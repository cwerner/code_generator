package org.rosi_project.model_sync.generator.test

import org.rosi_project.model_sync.generator.GeneratorConfig
import org.rosi_project.model_sync.generator.Generator
import java.io.File
import org.rosi_project.model_sync.generator.Creation

object ApplicationTest extends App {
  
  runLibraries()
      
  //runTestLibrary(Creation.rolesum)
  //runCombinedTest(Creation.rolecomb)
  //runShrinkingModel(Creation.rolesum)
  //runTestAML(Creation.rolesum)
  
  //TTC Case examples
  //runTestEmfLibrary(Creation.rolecomb)
  //runTTC2019(Creation.rolecomb)
  //runTTC2019(Creation.rolesync)  
  //runTTCLive2019(Creation.rolesync)

  //Run all tests
  //runAllTests(Creation.rsum)
  //runAllTests(Creation.rsync)
  
  def runLibraries(): Unit = {
    println("SUMM #####################################################################################")
    runCombinedTest(Creation.rolesum)
    runTestLibrary(Creation.rolesum)
    
    println("SYNC #####################################################################################")
    runCombinedTest(Creation.rolesync)
    runTestLibrary(Creation.rolesync)
    
    println("COMB #####################################################################################")
    runCombinedTest(Creation.rolecomb)
    runTestLibrary(Creation.rolecomb)
  }
  
  def runAllTests(cre: Creation.Value): Unit = {
    println("#####################################################################################")
    runTestFamily(cre)
    println("#####################################################################################")
    runTestSimplePerson(cre)
    println("#####################################################################################")
    runTestPerson(cre)
    println("#####################################################################################")
    runTestLibrary(cre)
    println("#####################################################################################")
    runTestShrinkingModel(cre)
    println("#####################################################################################")
    runTestAML(cre)
    println("#####################################################################################")
    runTestIMDB(cre)
    println("#####################################################################################")
    runTestModelJoinLib(cre)
    println("#####################################################################################")
    runCombinedTest(cre)
    println("#####################################################################################")
  }
  
  def runTTCLive2019(cre: Creation.Value): Unit = {
    var config: GeneratorConfig = GeneratorConfig(Seq("assets/models/BibTeX.ecore", "assets/models/DocBook.ecore", "assets/models/NMetaChanges.ecore"), false, new File("assets/models"), create = cre);
    new Generator(config).run()
  }
  
  def runTTC2019(cre: Creation.Value): Unit = {
    var config: GeneratorConfig = GeneratorConfig(Seq("assets/models/BDD.ecore", "assets/models/BDDv2.ecore", "assets/models/TT.ecore"), false, new File("assets/models"), create = cre);
    new Generator(config).run()
  }
  
  def runShrinkingModel(cre: Creation.Value): Unit = {
    var config: GeneratorConfig = GeneratorConfig(Seq("assets/models/ShrinkingModel.ecore"), false, new File("assets/models"), modelJoin = "assets/model_join/shrinking.modeljoin", create = cre);
    new Generator(config).run()
  }
  
  def runCombinedTest(cre: Creation.Value): Unit = {
    var config: GeneratorConfig = GeneratorConfig(Seq("assets/models/IMDBDatabase.ecore", "assets/models/ModelJoinLibrary.ecore"), false, new File("assets/models"), modelJoin = "assets/model_join/simple.modeljoin", create = cre);
    new Generator(config).run()
  }
  
  def runTestFamily(cre: Creation.Value): Unit = {
    var config: GeneratorConfig = GeneratorConfig(Seq("assets/models/Family.ecore"), false, new File("assets/models"), create = cre);
    new Generator(config).run()
  }
  
  def runTestSimplePerson(cre: Creation.Value): Unit = {
    var config: GeneratorConfig = GeneratorConfig(Seq("assets/models/SimplePerson.ecore"), false, new File("assets/models"), create = cre);
    new Generator(config).run()
  }
  
  def runTestPerson(cre: Creation.Value): Unit = {
    var config: GeneratorConfig = GeneratorConfig(Seq("assets/models/Person.ecore"), false, new File("assets/models"), create = cre);
    new Generator(config).run()
  }
  
  def runTestLibrary(cre: Creation.Value): Unit = {
    var config: GeneratorConfig = GeneratorConfig(Seq("assets/models/Library.ecore"), false, new File("assets/models"), modelJoin = "assets/model_join/manager.modeljoin", create = cre);
    new Generator(config).run()
  }
  
  def runTestEmfLibrary(cre: Creation.Value): Unit = {
    var config: GeneratorConfig = GeneratorConfig(Seq("assets/models/EMFLibrary.ecore"), false, new File("assets/models"), create = cre);
    new Generator(config).run()
  }
  
  def runTestShrinkingModel(cre: Creation.Value): Unit = {
    var config: GeneratorConfig = GeneratorConfig(Seq("assets/models/ShrinkingModel.ecore"), false, new File("assets/models"), create = cre);
    new Generator(config).run()
  }
  
  def runTestAML(cre: Creation.Value): Unit = {
    var config: GeneratorConfig = GeneratorConfig(Seq("assets/models/AML.ecore"), false, new File("assets/models"), create = cre);
    new Generator(config).run()
  }
  
  def runTestIMDB(cre: Creation.Value): Unit = {
    var config: GeneratorConfig = GeneratorConfig(Seq("assets/models/IMDBDatabase.ecore"), false, new File("assets/models"), create = cre);
    new Generator(config).run()
  }
  
  def runTestModelJoinLib(cre: Creation.Value): Unit = {
    var config: GeneratorConfig = GeneratorConfig(Seq("assets/models/ModelJoinLibrary.ecore"), false, new File("assets/models"), create = cre);
    new Generator(config).run()
  }
    
}