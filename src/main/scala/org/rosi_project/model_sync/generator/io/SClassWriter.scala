package org.rosi_project.model_sync.generator.io

import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.support.ExtendedString.stringToExtended
import org.rosi_project.model_sync.generator.sync.PredefRsumTypes
import org.rosi_project.model_sync.generator.acr_model.types.GenericSequence
import org.rosi_project.model_sync.generator.sync.HelperFunctions
import org.rosi_project.model_sync.generator.PackageNames

/**
 * The `Writer` generates the source code of a [[SClass]] and provides it as a `String`.
 *
 * @author Rico Bergmann
 */
class SClassWriter(val modelClass: SClass) {

  private val isView: Boolean = modelClass.isInstanceOf[SViewClass]
  private val pckg: String = if (modelClass.isDefaultPackage) "" else s"package ${modelClass.sPackage}"
  private val imports: Seq[String] = modelClass.collectImports.toSeq
  private val viewImports: String = {
    if (isView) {
      s"""
        | import ${PredefRsumTypes.VIEWTYPE_INFO_STYPE.getPackage}.${PredefRsumTypes.VIEWTYPE_INFO_STYPE.getName}
        | import ${PredefRsumTypes.JOIN_INFO_STYPE.getPackage}.${PredefRsumTypes.JOIN_INFO_STYPE.getName}
        | ${modelClass.asInstanceOf[SViewClass].getJoinObject().map(j => s"import ${j.joinObject.getPackage}.${j.joinObject.getName}").mkString(" \n")}
        """.stripMargin
    } else {
      ""
    }
  }

  private val clazzFixture: String = generateClassFixture
  private val internalClazzFixture: String = generateInternalClazzFixture
  private val companionObject: String = generateCompanionFixture

  /**
   * Provides a source code representation of the `modelClass` as a `String`.
   *
   * When writing it to a file it will be able to be compiled with the `scalac` (assuming the
   * ''classpath'' is set-up correctly).
   */
  def stringify: String = {
    s"""$pckg
       |
       |${imports.map(i => s"import $i").mkString("\n")}
       |${viewImports}
       |
       |/**
       | * This file is automatically generated from the code generator
       | * for the role-based model management framework.
       | */
       |${clazzFixture} {
       |
       |  ${if (PackageNames.multiInhertitanceWithTraits) s"${generateAttributeFixture}" else ""}
       |
       |  ${modelClass.getAdditionalConstructorStatements.map(_.getContent).mkString("\n")}
       |
       |  ${modelClass.getMethods.map(stringifyMethod).mkString("\n")}
       |
       |  ${internalClazzFixture}
       |}
       |
       |${if (isView) companionObject else ""}
       |
    """.stripMargin
  }

  /**
   * Provides a source code representation of an internal class used as role in SCROLL.
   */
  def internalStringify: String = {
    s"""
       |$clazzFixture {
       |
       |  ${modelClass.getAdditionalConstructorStatements.map(_.getContent).mkString("\n")}
       |
       |  ${modelClass.getMethods.map(stringifyMethod).mkString("\n")}
       |
       |}
    """.stripMargin
  }

  /**
   * Writes a method as source code.
   *
   * @param m the method to write
   * @return the `String` representation of `m`
   */
  protected def stringifyMethod(m: SMethod): String = {
    var result = s"""${if (m.overrides) "override " else ""}${getVisibilityString(m.getVisibility)}def ${m.getName}(${m.params.map(param => s"${param.getName}: ${param.getDeepTypeName}").mkString(", ")}): ${m.getResultType} """
    if (!m.implementation.isEmpty) {
      result = result + s""" = {
         |  ${m.implementation.map(_.getContent).mkString("\n")}
         |}
        """
    }
    result.stripMargin
  }

  /**
   * Writes the "''companion fixture''" for views.
   */
  protected def generateCompanionFixture: String = {
    if (isView) {
      val viewClass = modelClass.asInstanceOf[SViewClass]
      s"""
       |/**
       | * This file is automatically generated from the code generator
       | * for the role-based model management framework.
       | */
       |object ${modelClass.getName} extends ${PredefRsumTypes.VIEWTYPE_INFO_STYPE.getName} {  
       |
       |  override def getViewName(): String = "${modelClass.getName}"
       |  
       |  def getJoinInfos(): Set[${PredefRsumTypes.JOIN_INFO_STYPE.getName}] = ${if (viewClass.getJoinObject.isEmpty) "Set.empty" else s"Set(${viewClass.getJoinObject.map(_.joinObject.getName).mkString(", ")})"}
       |
       |  protected def getNewInstance(): ${PredefRsumTypes.IVIEW_COMPARTMENT_STYPE.getName} = new ${modelClass.getName}()
       |
       |  def getNewView(): ${modelClass.getName} = getNewViewTypeInstance().asInstanceOf[${modelClass.getName}]
       |}
      """.stripMargin
    } else {
      ""
    }
  }

  /**
   * Writes the internal classes.
   */
  protected def generateInternalClazzFixture: String = {
    var result = "";
    modelClass.getInternalClasses.foreach(intCls => {
      var sw: SClassWriter = new SClassWriter(intCls)
      var s = sw.internalStringify;
      result = result + s + "\n";
    })
    result
  }

  private def getVisibilityString(vis: MethodVisibility.Value): String = {
    var visibility = ""
    vis match {
      case MethodVisibility.privateVis   => visibility = "private "
      case MethodVisibility.protectedVis => visibility = "protected "
      case MethodVisibility.privateExternalClass =>
        if (modelClass.isInstanceOf[SInnerClass]) {
          visibility = s"private[${modelClass.asInstanceOf[SInnerClass].externalClass.getName}] "
        }
      case _ =>
    }
    visibility
  }

  /**
   * Generate the attribute fixtures to add them in the body.
   */
  protected def generateAttributeFixture: String = {
    modelClass.getStructuralFeatures.map(attr => {
      val finalS: String = if (attr.isFinal) "val" else "var"
      val instanziationS: String = if (attr.getTypeElement.isInstanceOf[GenericSequence]) "= Set.empty" else s"= ${HelperFunctions.classEmptyType(attr.getTypeElement.getName)}"
      s"${getVisibilityString(attr.getVisibility)}${finalS} ${attr.getName}: ${attr.getTypeElement.getDeepName} ${instanziationS} \n"
    }).mkString(" \n")
  }

  /**
   * Writes the "''class fixture''", i.e. the `class` identifier followed by the constructor and
   * optionally a parent class. The parent constructor will be called correctly.
   */
  protected def generateClassFixture: String = {
    //println("**************************************************")
    var params: Seq[String] = modelClass.getStructuralFeatures.map(attr => {
      val finalS: String = if (attr.isFinal) "val" else "var"
      s"${getVisibilityString(attr.getVisibility)}${finalS} ${attr.getName}: ${attr.getTypeElement.getDeepName}"
    }).toList
    var baseFixture: String = ""
    var parentConstructorParams: String = ""
    var parentConstructor: String = ""
    var allInterfaces: String = ""
    val parent: STypedElement = modelClass.getClassParent

    if (!modelClass.isRootClass) {
      parentConstructorParams = parent.getAllConstructorParameters.map(param => s"${constructorParamForParentConstructor(parent, param)}: ${param.getDeepTypeName}").mkString(", ")
      parentConstructor = s"(${parent.getAllConstructorParameters.map(constructorParamForParentConstructor(parent, _)).mkString(", ")})"
    }

    if (!modelClass.getInterfaceParents.isEmpty) {
      allInterfaces = s"${modelClass.getInterfaceParents.map(namesOfInterfaces(_)).mkString(" with ")}"
    }

    if (parentConstructorParams != "") {
      params = params :+ parentConstructorParams
    }

    val constructor: String = if (params.isEmpty) "" else s"(${params.mkString(", ")})"

    if (modelClass.isInterface) {
      baseFixture = s"trait ${modelClass.getName}"
    } else if (modelClass.isObject) {
      baseFixture = s"object ${modelClass.getName}"
    } else {
      baseFixture = s"class ${modelClass.getName}"
      if (!PackageNames.multiInhertitanceWithTraits) {
        baseFixture = s"$baseFixture$constructor"
      }
      if (modelClass.isAbstract) {
        baseFixture = s"abstract $baseFixture"
      }
    }

    if (modelClass.isRootClass) {
      if (allInterfaces != "") {
        if (isView) {
          baseFixture = s"$baseFixture private"
        }
        baseFixture = s"$baseFixture extends $allInterfaces"
      }
    } else {
      baseFixture = s"$baseFixture extends ${parent.getName}"
      if (!PackageNames.multiInhertitanceWithTraits) {
        baseFixture = s"${baseFixture}${parentConstructor}"
      }
      if (allInterfaces != "") {
        baseFixture = s"$baseFixture with $allInterfaces"
      }
    }
    baseFixture
  }

  /**
   * Generates a ''class parameter's'' name that will be used to initialize a field of the super class.
   *
   * This is purely to prevent naming conflicts and shadowing when trying to access a parent's field
   * from the subclass.
   */
  private def constructorParamForParentConstructor(parent: STypedElement, param: SMethodParameter): String = {
    var parName = s"${parent.getName.head.toLower}_${param.getName.firstLetterToUpperCase}"

    // we need to make sure that the generate name does not by chance appear as a regular attribute
    // of the model class. If this is the case we add some number to make it unique again.
    if (modelClass.getDeepStructuralFeatures.exists(_.getName == parName)) {
      var uniqueIdx = 2
      var uniqueParName = parName + uniqueIdx

      while (modelClass.getDeepStructuralFeatures.exists(_.getName == uniqueParName)) {
        uniqueIdx += 1
        uniqueParName = parName + uniqueIdx
      }

      parName = uniqueParName
    }

    parName
  }

  /**
   * Returns the name of the interface.
   */
  private def namesOfInterfaces(interface: STypedElement): String = {
    interface.getName
  }

}
