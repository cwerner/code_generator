package org.rosi_project.model_sync.generator.io

import java.nio.file.{Files, StandardCopyOption}
import java.{io => jio}

import org.rosi_project.model_sync.generator.ModelConfig
import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.env.{FilesCompiler, JarPackager}

import scala.reflect.io.{Directory, File, Path}
import org.rosi_project.model_sync.generator.GeneratorConfig
import org.rosi_project.model_sync.generator.Creation

/** The `FSWriter` writes a [[SModel]] as a single compiled ''JAR'' file to the File System.
  *
  * @param workingDir directory to write the `.scala` files to. Will default to a temporary dir.
  * @param outputDir directory to write the generated `model.jar` to
  * @param keepClassFiles whether the `.scala` files should be added to the ''JAR'' as well
  *
  * @author Rico Bergmann
  */
class SModelFSWriter(
    generatorConfig: GeneratorConfig,
    workingDir: File = File(Files.createTempDirectory("model").toFile),
    outputDir: Directory = File("output").toDirectory,
    keepClassFiles: Boolean = false,
    modelCfgs: Set[ModelConfig],
    currentDir: jio.File = new jio.File("").getAbsoluteFile) extends SModelVisitor {

  workingDir.jfile.mkdirs()
  outputDir.jfile.mkdirs()

  private var sFilesToCompile: List[File] = List.empty

  println(s"Temp dir (sources) is $workingDir")
  println(s"Output dir is $outputDir")

  println("... Copying model images")
  private var images: List[jio.File] = List.empty
  private var imagesTargetDir: jio.File = _
  try {    
    if (modelCfgs.isEmpty) {
      //TODO: get files  with same source Name
      images = List.empty
    } else {      
      modelCfgs.foreach(con => {
        images = images ++ collectAllModelImages(con, currentDir)        
      })
    }
    imagesTargetDir = new jio.File(workingDir.toAbsolute.toString() + File.separator + "res")
    imagesTargetDir.mkdirs()
    images.foreach(img => {
      //println("##########Images: " + img.getName)
      Files.copy(img.toPath, imagesTargetDir.toPath.resolve(img.getName), StandardCopyOption.REPLACE_EXISTING)
    })
  } catch {
    case e: Exception => throw new ModelImagePreparationException(e)
  }


  override def visit(sModel: SModel): Unit = {
    sModel.getModelEnums.foreach(writeEnum(_))
    sModel.getModelClasses.foreach(writeClass(_))
    sModel.getProviderClasses.foreach(writeClass(_))
    if (generatorConfig.create != Creation.rolesync) {
      sModel.getJoinClasses.foreach(writeClass(_))
      sModel.getRelationalCompartments.foreach(writeClass(_))
      sModel.getViewCompartments.foreach(writeClass(_))
    }
    //println(s"... Wrote files (sources) $sFilesToCompile")

    println("... Starting compilation")
    new FilesCompiler(workingDir).run(sFilesToCompile)
    println("... Compilation done")

    if (!keepClassFiles) {
      println("... Cleaning up")
      sFilesToCompile.foreach {
        _.delete
      }
    } else {
      println("... No clean-up requested")
    }

    println("... Generating JAR")
    println(s"... Successfully wrote JAR ${outputDir + File.separator + generatorConfig.getCombinedEcoreName + ".jar"}")    
    new JarPackager(workingDir, outputDir.toFile, generatorConfig.getCombinedEcoreName + ".jar").run()
    println(s"... Successfully wrote JAR ${outputDir + File.separator + generatorConfig.getCombinedEcoreName + ".jar"}")
  }
  
  private def writeEnum(sEnum: SEnum): Unit = {
    try {
      println(s"Writing class $sEnum")
      val classNameWithPath = workingDir.toAbsolute.toString() + File.separator + pckg2Path(sEnum.getPackage) + File.separator + s"${sEnum.getName}.scala"
      val writer = new SEnumWriter(sEnum)

      val classFile = File(classNameWithPath)

      classFile.jfile.getParentFile.mkdirs()
      classFile.writeAll(writer.stringify)
      sFilesToCompile ::= classFile
    } catch {
      case e: Exception => throw new ClassWritingException(e)
    }
  }
  
  private def writeClass(sClass: SClass): Unit = {
    try {
      println(s"Writing class $sClass")
      val classNameWithPath = workingDir.toAbsolute.toString() + File.separator + pckg2Path(sClass.getPackage) + File.separator + s"${sClass.getName}.scala"
      val writer = new SClassWriter(sClass)

      val classFile = File(classNameWithPath)

      classFile.jfile.getParentFile.mkdirs()
      classFile.writeAll(writer.stringify)
      sFilesToCompile ::= classFile
    } catch {
      case e: Exception => throw new ClassWritingException(e)
    }
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }
  
  override def visit(sRef: SReference): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }

  /** Maps a Scala package to an relative path on the File System.
    *
    * @return the OS-specific path
    */
  private def pckg2Path(pckg: String): Path = Path(pckg.replace(".", File.separator))

  private def collectAllModelImages(modelConfig: ModelConfig, workingPath: jio.File): List[jio.File] = {
    var images: List[jio.File] = List.empty

    modelConfig.init.image.foreach(img => {
      images = images :+ new jio.File(workingPath.getPath + File.separator + img)
    })

    modelConfig.init.nested.foreach(nested => {
      nested.foreach(model => {
        model.image.foreach(img => {
          images = images :+ new jio.File(workingPath.getPath + File.separator + img)
        })
      })
    })

    modelConfig.integration.foreach(integration => {
      integration.foreach(model => {
        model.image.foreach(img => {
          images = images :+ new jio.File(workingPath.getPath + File.separator + img)
        })
      })
    })
    println(modelConfig + " I: " + images)
    images
  }

}

/** Exception to indicate that the model images may not be copied into the JAR.
  *
  * @param cause the causing exception
  */
class ModelImagePreparationException(cause: Exception) extends RuntimeException(cause)

/** Exception to indicate that a class may not be written.
  *
  * @param cause the causing exception
  */
class ClassWritingException(cause: Exception) extends RuntimeException(cause)
