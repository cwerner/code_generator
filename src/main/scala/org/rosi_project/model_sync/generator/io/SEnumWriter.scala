package org.rosi_project.model_sync.generator.io

import org.rosi_project.model_sync.generator.acr_model.SEnum

class SEnumWriter(val modelEnum: SEnum) {

  private val pckg: String = if (modelEnum.isDefaultPackage) "" else s"package ${modelEnum.sPackage}"

  /** Provides a source code representation of the `modelEnums` as a `String`.
    *
    * When writing it to a file it will be able to be compiled with the `scalac` (assuming the
    * ''classpath'' is set-up correctly).
    */  
  def stringify: String = {
    s"""$pckg
       |
       |/**
       | * This file is automatically generated from the code generator
       | * for the role-based model management framework.
       | */
       |object ${modelEnum.getName} extends Enumeration {
       |
       |  val ${modelEnum.enums.mkString(", ")} = Value
       |}
       |
    """.stripMargin
  }
}