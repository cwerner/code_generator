package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.acr_model.SModel

/** Contains services to write [[SModel]] instances to the File system.
  *
  * @author Rico Bergmann
  */
package object io {

}
