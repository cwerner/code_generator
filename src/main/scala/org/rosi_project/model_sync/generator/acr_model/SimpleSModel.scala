package org.rosi_project.model_sync.generator.acr_model

import org.rosi_project.model_sync.generator.support.Assert

/** Default implementation of the [[SModel]].
  *
  * @author Rico Bergmann
  */
class SimpleSModel(name: String, sourceName: String, nsUri: String) extends SModel(name, sourceName, nsUri) {
  
  private var sClasses: Set[SClass] = Set.empty
  private var sEnums: Set[SEnum] = Set.empty

  /** Provides all the classes in `this` model. */
  override def getAllClasses: Set[SClass] = sClasses
  
  /** Provides all model classes in `this` model. */
  override def getModelClasses: Set[SClass] = sClasses
  
  /** Provides all provider classes in `this` model. */
  override def getProviderClasses: Set[SClass] = sClasses
  
  /** Provides all relational compartments in `this` model. */
  override def getRelationalCompartments: Set[SClass] = sClasses
  
  /** Provides all view compartments in `this` model. */
  override def getViewCompartments: Set[SClass] = sClasses
  
  /** Provides all join classes in `this` model. */
  override def getJoinClasses: Set[SClass] = Set.empty
  
  /** Provides all model enums in `this` model. */
  override def getModelEnums: Set[SEnum] = sEnums

  /** Extends the model by a new class.
    *
    * @param mClass the class to add. May never `null`.
    */
  override def addModelClass(mClass: SClass): Unit = {
    Assert.notNull(mClass, "Class may not be null")
    sClasses += mClass
  }
  
  /** Extends the model by a new provder class.
    *
    * @param mClass the class to add. May never `null`.
    */
  override def addProviderClass(mClass: SClass): Unit = {
    Assert.notNull(mClass, "Class may not be null")
    sClasses += mClass
  }
  
  /** Extends the model by a new enum.
    *
    * @param mEnum the enum to add. May never `null`.
    */
  override def addModelEnums(mEnum: SEnum): Unit = {
    Assert.notNull(mEnum, "Enum may not be null")
    sEnums += mEnum
  }
  
  /** Extends the model by a new relational compartment.
    *
    * @param mClass the class to add. May never `null`.
    */
  override def addRelationalCompartment(mClass: SClass): Unit = {
    Assert.notNull(mClass, "Class may not be null")
    sClasses += mClass
  }
  
  /** Extends the model by a new view compartment.
    *
    * @param mClass the class to add. May never `null`.
    */
  override def addViewCompartment(mClass: SClass): Unit = {
    Assert.notNull(mClass, "Class may not be null")
    sClasses += mClass
  }
  
  /** Extends the model by a new join class.
    *
    * @param mClass the class to add. May never `null`.
    */
  override def addJoinClass(mClass: SClass): Unit = {/*pass*/}

  override def accept(visitor: SModelVisitor): Unit = {
    sClasses.foreach(_.accept(visitor))
    visitor.visit(this)
  }

  def canEqual(other: Any): Boolean = other.isInstanceOf[SimpleSModel]

  override def equals(other: Any): Boolean = other match {
    case that: SimpleSModel =>
      (that canEqual this) &&
        sEnums == that.sEnums &&
        sClasses == that.sClasses
    case _ => false
  }
  
  override def addOtherModel(model: SModel): SModel = {
    sClasses = sClasses ++ model.getModelClasses
    sEnums = sEnums ++ model.getModelEnums
    return this
  }

  override def hashCode(): Int = {
    val state = Seq(sClasses)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}
