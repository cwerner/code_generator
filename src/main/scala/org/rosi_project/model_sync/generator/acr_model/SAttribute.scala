package org.rosi_project.model_sync.generator.acr_model

/** Representation of class attributes. This abstraction is a simplification of an actual scala
  * attribute: it is impossible to distinguish between ''final'' and ''non-final'' fields. Instead,
  * all attributes will be treated mutable.
  *
  * @author Rico Bergmann
  */
case class SAttribute(_name: String, _type: STypedElement, val unique: Boolean = false) extends SStructuralFeature(_name, _type) {

  override def accept(visitor: SModelVisitor): Unit = visitor.visit(this)

  override def toString: String = s"$getName: ${getTypeElement.getName} $unique"

}
