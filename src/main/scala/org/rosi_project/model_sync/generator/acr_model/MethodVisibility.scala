package org.rosi_project.model_sync.generator.acr_model

object MethodVisibility extends Enumeration {
  val privateVis, protectedVis, publicVis, privateExternalClass = Value
}