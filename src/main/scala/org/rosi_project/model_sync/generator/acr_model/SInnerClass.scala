package org.rosi_project.model_sync.generator.acr_model

import java.util.Objects

class SInnerClass(_name: String,
             _sPackage: String = "",
             _sClassType: SClassType.Value = SClassType.normalClass,
             val externalClass: SClass) extends SClass(_name, _sPackage, _sClassType) {
  
  Objects.requireNonNull(externalClass, "External class may not be null")
  
  override def getDeepName: String = externalClass.getName + "#" + _name
}