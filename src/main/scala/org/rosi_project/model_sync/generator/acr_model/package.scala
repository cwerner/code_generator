package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.acr_model.{SClass, SModel}

/** Abstract class representation of the Scala code used within a model.
  *
  * This package specifies a simplified meta-model of scala classes. Most parts of (our take on) a
  * Scala class have a corresponding wrapper class that may be modified for further adaptations.
  *
  * @see [[SClass]]
  * @see [[SModel]]
  * @author Rico Bergmann
  */
package object acr_model {

}
