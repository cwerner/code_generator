package org.rosi_project.model_sync.generator.acr_model

import org.rosi_project.model_management.sum.join.RsumJoinType

class SJoinClass(_name: String,
             _sPackage: String,
             val base: SClass,
             val other: SClass,
             val joinType: RsumJoinType.Value,
             val joinAttributes: Set[SStructuralFeature],
             val innerAttributes: Set[SStructuralFeature],
             val joinObject: SClass) extends SClass(_name, _sPackage) {
    
  override def getRootClassWithNameAndPackage(n: String, p: String): SClass = {
    val bparent = base.getRootClassWithNameAndPackage(n, p)
    val oparent = other.getRootClassWithNameAndPackage(n, p)
    if (bparent != null) {
      return bparent
    }
    oparent
  }
  
}