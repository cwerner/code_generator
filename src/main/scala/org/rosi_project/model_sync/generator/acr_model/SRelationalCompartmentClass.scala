package org.rosi_project.model_sync.generator.acr_model

import java.util.Objects

class SRelationalCompartmentClass(_name: String,
             _sPackage: String = "",
             _sClassType: SClassType.Value = SClassType.normalClass,
             val connectedRef: SReference,
             val sClass: STypedElement,
             val tClass: STypedElement) extends SClass(_name, _sPackage, _sClassType) {
  
  Objects.requireNonNull(connectedRef, "Connected Reference may not be null")
}