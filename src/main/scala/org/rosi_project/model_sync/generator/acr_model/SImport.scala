package org.rosi_project.model_sync.generator.acr_model

/** Wraps a type that needs to be imported in order for some class to compile.
  *
  * @param pckg the package that contains the type to import
  * @param name the name of the type to import
  * @author Rico Bergmann
  */
case class SImport(pckg: String, name: String)

/** The companion provides convenience methods to deal with sets of imports.
  */
object SImport {

  /** Creates the imports necessary to access the specified types.
    */
  def generateImports(types: STypedElement*): Set[SImport] = removeUnnecessaryImports(types.map(t => SImport(t.getPackage, t.getName)).toSet)

  /** Creates the imports necessary to access the specified types. Necessity to imports will be
    * assessed from a base package.
    */
  def generateImportsForBasePackage(basePckg: String, types: STypedElement*) = removeUnnecessaryImports(generateImports(types: _*), basePckg)

  /** Deletes all the imports which refer to types in the default package and are therefore not
    * necessary.
    */
  def removeUnnecessaryImports(imports: Set[SImport]): Set[SImport] = imports.filter(_.pckg != "")

  /** Deletes all the imports which refer to types in the default package or the same package as
    * specified and are therefore not necessary.
    */
  def removeUnnecessaryImports(imports: Set[SImport], basePckg: String): Set[SImport] = imports.filter(imp => imp.pckg != "" && imp.pckg != basePckg)

}
