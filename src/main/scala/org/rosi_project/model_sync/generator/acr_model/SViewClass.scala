package org.rosi_project.model_sync.generator.acr_model

class SViewClass(_name: String,
             _sPackage: String) extends SClass(_name, _sPackage) {
  
  private var joinObjects: Set[SJoinClass] = Set.empty
  
  def getJoinObject(): Set[SJoinClass] = joinObjects
  
  def addJoinObject(v: SJoinClass): Unit = {
    require(v != null)
    joinObjects += v
  }
}