package org.rosi_project.model_sync.generator.acr_model

/** Represents types provided by the Java runtime (either as part of the Java/Scala standard
  * libraries or other third-party projects).
  *
  * In general as a rule of thumb when loading a model the classes defined by the model should be
  * mapped to instances of [[SClass]] whereas all those types that are used (but not defined) within
  * the model and therefore function as some kind of "support-type" should become instances of this
  * class.
  *
  * @see [[SClass]]
  * @author Rico Bergmann
  */
case class SType(_name: String, _sPackage: String = "", _sClassType: SClassType.Value = SClassType.normalTrait) extends STypedElement (_name, _sPackage, _sClassType) {

  override def accept(visitor: SModelVisitor): Unit = visitor.visit(this)
  
  override def toString: String = s"ST: $getName($sPackage, $isInterface)"
}
