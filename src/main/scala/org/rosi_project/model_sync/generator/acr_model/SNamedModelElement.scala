package org.rosi_project.model_sync.generator.acr_model

/** Representation of named element like class, types, attributes and references.
  *
  * @author Christopher Werner
  */
abstract class SNamedModelElement (name: String) extends SModelElement {
  
  /** The name of 'this' element.
    *
    * @return the name. Will never be `null` nor empty.
    */
  def getName: String = name
  
  /** The deep name with external class name + # + this class name.
   * If there is no external class then only the name is returned.
   * 
   *  @return the name. Will never be `null` nor empty.
   */
  def getDeepName: String = name
}