package org.rosi_project.model_sync.generator.acr_model

import org.rosi_project.model_sync.generator.support.ExtendedString.stringToExtended
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes

/** Simple representation of a setter method.
  *
  * @author Rico Bergmann
  */
class SSetter(struc: SStructuralFeature) extends SMethod(
  name = s"set${struc.getName.firstLetterToUpperCase}",
  result = PredefTypes.Unit,
  params = Seq(SMethodParameter(struc.getName.head.toString, struc.getTypeElement)),
  implementation = Seq(SMethodStatement(content = s"${struc.getName} = ${struc.getName.head}", usedTypes = Set(struc.getTypeElement)))) {

  /*if (!STypeRegistry.isDefaultNotNullType(struc.getTypeElement.getName)) {
    implementation = Seq(SMethodStatement(content = s"require(${struc.getName.head} != null)"),
      SMethodStatement(content = s"${struc.getName} = ${struc.getName.head}", usedTypes = Set(struc.getTypeElement)))
  } else {
    implementation = Seq(SMethodStatement(content = s"${struc.getName} = ${struc.getName.head}", usedTypes = Set(struc.getTypeElement)))
  } */
}
