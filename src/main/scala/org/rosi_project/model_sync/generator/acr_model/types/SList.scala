package org.rosi_project.model_sync.generator.acr_model.types

import org.rosi_project.model_sync.generator.acr_model._

/** Wraps a Scala `List` for some type.
  */
class SList(elemType: STypedElement) extends GenericSequence("List", elemType) {

}

/** The companion provides `apply` and `unapply` methods.
  */
object SList {

  def apply(elemType: STypedElement): SList = new SList(elemType)

  def unapply(arg: SList): Option[STypedElement] = Some(arg.typeParam)

}
