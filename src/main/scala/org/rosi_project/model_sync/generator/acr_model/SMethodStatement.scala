package org.rosi_project.model_sync.generator.acr_model

/** Wrapper for the statements that form a method.
  *
  * As for now we represent the actual statement as a simple `String` the types used in the
  * statement need to be given explicitly and cannot be derived as e.g. in the case of [[SClass]]
  * or [[SMethod]].
  *
  * To get the actual statement [[getContent]] should be used instead of accessing `content`
  * directly. This will make usage more robust if the content will be replaced by a more
  * sophisticated structure later on (which is likely to happen).
  *
  * @author Rico Bergmann
  */
case class SMethodStatement(private val content: String, usedTypes: Set[STypedElement] = Set.empty) {

  /** The actual statement as a `String`.
    *
    * We provide this method as a stable API to the statement. Accessing the content directly
    * through the corresponding property will couple code too tightly to this current
    * implementation of `content` -  which is likely to be replaced by a more sophisticated
    * structure to represent method statements.
    *
    * @return the statement. Will never be `null` nor empty.
    */
  def getContent: String = content

  override def toString: String = getContent

}
