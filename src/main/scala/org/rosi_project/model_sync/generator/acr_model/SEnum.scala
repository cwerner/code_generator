package org.rosi_project.model_sync.generator.acr_model

class SEnum (_name: String, _sPackage: String, val enums: Set[String]) extends STypedElement(_name, _sPackage, SClassType.normalClass) {
    
  override def getDeepName: String = _name + ".Value"
  
  override def accept(visitor: SModelVisitor): Unit = {
    //pass never change an incoming enum
  }  
}