package org.rosi_project.model_sync.generator.acr_model

/** Base class for all parts of the ''ACR''. It merely exists to enable the ''Visitor pattern''.
  *
  * @see [[https://en.wikipedia.org/wiki/Visitor_pattern Visitor pattern @ Wikipedia]]
  * @author Rico Bergmann
  */
trait SModelElement {

  /** Executes the `visitor` on the structure of `this` element.
    *
    * Implementations will differ naturally but the most common approach is to recursively call
    * `accept` on all composed elements and run `visitor.visit(this)` at the end.
    *
    * @param visitor the visitor. Might not be `null`.
    */
  def accept(visitor: SModelVisitor)

}
