package org.rosi_project.model_sync.generator.acr_model

/** A model is a collection of classes that represents a specific data-driven view on the
  * application.
  *
  * @author Rico Bergmann
  */
abstract class SModel(private val name: String, private val sourceName: String, private val nsUri: String) extends SModelElement {
  
  /** Get NsURI from the model. */
  def getNsURI: String = nsUri
  
  /** Get source name from the model. */
  def getSourceName: String = sourceName
  
  /** Get the name from the model. */
  def getName: String = name
  
  /** Provides all the classes in `this` model. */
  def getAllClasses: Set[SClass]
  
  /** Provides all model classes in `this` model. */
  def getModelClasses: Set[SClass]
  
  /** Provides all model enums in `this` model. */
  def getModelEnums: Set[SEnum]
  
  /** Provides all provider classes in `this` model. */
  def getProviderClasses: Set[SClass]
  
  /** Provides all relational compartments in `this` model. */
  def getRelationalCompartments: Set[SClass]
  
  /** Provides all view compartments in `this` model. */
  def getViewCompartments: Set[SClass]
  
  /** Provides all join classes in `this` model. */
  def getJoinClasses: Set[SClass]
  
  
  
  /** Extends the model by a new provder class.
    *
    * @param mClass the class to add. May never `null`.
    */
  def addProviderClass(mClass: SClass): Unit
  
  /** Extends the model by a new relational compartment.
    *
    * @param mClass the class to add. May never `null`.
    */
  def addRelationalCompartment(mClass: SClass): Unit
  
  /** Extends the model by a new view compartment.
    *
    * @param mClass the class to add. May never `null`.
    */
  def addViewCompartment(mClass: SClass): Unit
  
  /** Extends the model by a new class.
    *
    * @param mClass the class to add. May never `null`.
    */
  def addModelClass(mClass: SClass): Unit
  
  /** Extends the model by a new join class.
    *
    * @param mClass the class to add. May never `null`.
    */
  def addJoinClass(mClass: SClass): Unit
  
  /** Extends the model by a new enum.
    *
    * @param mEnum the enum to add. May never `null`.
    */
  def addModelEnums(mEnum: SEnum): Unit
  
  /**
   * Add another model to this on and return this one.
   */
  def addOtherModel(model: SModel): SModel

  override def toString: String = s"SModel: classes=$getAllClasses"

}
