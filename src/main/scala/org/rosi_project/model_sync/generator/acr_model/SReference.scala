package org.rosi_project.model_sync.generator.acr_model

/** Representation of class reference. This abstraction is a simplification of an actual scala
  * reference: it is impossible to distinguish between ''final'' and ''non-final'' fields. Instead,
  * all attributes will be treated mutable.
  *
  * @author Christopher Werner
  */
case class SReference(_name: String, /*val stype: STypedElement,*/ _ttype: STypedElement, upperBound: Int, lowerBound: Int, containment: Boolean = false, var oppositeRef: SReference = null) extends SStructuralFeature (_name, _ttype) {
  
  def hasOpposite: Boolean = oppositeRef != null
  
  def getImportant: SReference = {
    //return if one is containment
    if (containment) {
      return this
    }
    if (oppositeRef.containment) {
      return oppositeRef
    }
    //return if one has bigger lowerBound
    if (lowerBound > oppositeRef.lowerBound) {
      return this
    }
    if (lowerBound < oppositeRef.lowerBound) {
      return oppositeRef
    }
    //return if one has smaller upperBound but bigger than -1
    if (upperBound > -1 && oppositeRef.upperBound > -1 && upperBound > oppositeRef.upperBound) {
      return oppositeRef
    }    
    this
  }
  
  def getMinimalConnection: SReference = {
    if (lowerBound == 0 && upperBound == 1) {
      return this
    }
    if (hasOpposite && oppositeRef.lowerBound == 0 && oppositeRef.upperBound == 1) {
      return oppositeRef
    }
    if (lowerBound == 1 && upperBound == 1) {
      return this
    }
    if (hasOpposite && oppositeRef.lowerBound == 1 && oppositeRef.upperBound == 1) {
      return oppositeRef
    }
    return null
  }

  override def accept(visitor: SModelVisitor): Unit = visitor.visit(this)

  override def toString: String = s"$getName: ${getTypeElement.getName} $containment $lowerBound $upperBound"

}