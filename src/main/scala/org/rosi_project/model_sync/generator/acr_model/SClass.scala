package org.rosi_project.model_sync.generator.acr_model

import java.util.Objects
import org.rosi_project.model_sync.generator.acr_model.types.SSeq
import org.rosi_project.model_sync.generator.acr_model.types.GenericSequence
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes
import org.rosi_project.model_sync.generator.sync.ToStringMethods

/**
 * Representation of Scala classes. Attributes, methods, etc. have their own wrappers which should
 * be used to modify these individual parts of the class.
 *
 * @see [[SType]]
 * @author Rico Bergmann
 */
class SClass(_name: String,
             _sPackage: String = "",
             _sClassType: SClassType.Value = SClassType.normalClass) extends STypedElement(_name, _sPackage, _sClassType) {

  protected var parentClass: STypedElement = null
  protected var parentInterfaces: Seq[STypedElement] = Seq.empty
  protected var internalClasses: Seq[SInnerClass] = Seq.empty

  protected var attributes: Seq[SAttribute] = Seq.empty
  protected var references: Seq[SReference] = Seq.empty

  protected var methods: Seq[SMethod] = Seq.empty
  protected var constructorStatements: Seq[SMethodStatement] = Seq.empty

  def getClassParent: STypedElement = parentClass

  def getInterfaceParents: Seq[STypedElement] = parentInterfaces

  def getStructuralFeatures: Seq[SStructuralFeature] = attributes ++ references

  def getInternalClasses: Seq[SInnerClass] = internalClasses

  def getDeepStructuralFeatures: Seq[SStructuralFeature] = {
    val cls = getClassParent
    if (cls == null) {
      attributes ++ references
    } else {
      cls match {
        case parentAsSClass: SClass =>
          attributes ++ references ++ parentAsSClass.getDeepStructuralFeatures
        case _ =>
          attributes ++ references
      }
    }
  }

  def getOnlyDeepAttributes: Seq[SAttribute] = {
    val cls = getClassParent
    if (cls == null) {
      attributes
    } else {
      cls match {
        case parentAsSClass: SClass =>
          attributes ++ parentAsSClass.getOnlyDeepAttributes
        case _ =>
          attributes
      }
    }
  }

  def getOnlyDeepReferences: Seq[SReference] = {
    val cls = getClassParent
    if (cls == null) {
      references
    } else {
      cls match {
        case parentAsSClass: SClass =>
          references ++ parentAsSClass.getOnlyDeepReferences
        case _ =>
          references
      }
    }
  }
  
  override def proofHierarchicalEquality(sType: STypedElement): Boolean = {
    if (sType == this) {
      return true
    } else {
      this.getAllParents().foreach(p => {
        if (p.proofHierarchicalEquality(sType)) {
          return true
        }
      })
      return false
    }
    
  }

  /**
   * Augments `this` class with another method.
   *
   * @param m the additional method. May not be `null`
   */
  def addMethod(m: SMethod): Unit = {
    Objects.requireNonNull(m, "Method to add may not be null")
    if (methods.contains(m)) {
      return
    }
    methods = methods :+ m
  }

  /**
   * Gets the methods of `this` class.
   *  And adds a to String if not exists.
   */
  def getMethods: Seq[SMethod] = {
    if (methods.exists(_.getName == "toString")) {
      methods
    } else {
      methods :+ ToStringMethods.normalToStringMethod(this)
    }
  }

  /** Gets the parents of `this` class. */
  def getAllParents(): Seq[STypedElement] = {
    if (parentInterfaces.isEmpty && parentClass == null) {
      return Seq.empty
    }
    if (parentInterfaces.isEmpty) {
      return Seq(parentClass)
    }
    if (parentClass == null) {
      return parentInterfaces
    }
    parentInterfaces :+ parentClass
  }

  /**
   * Adds a internal class to `this` class.
   *
   * If `parent == this or null` nothing will happen.
   */
  def addInternalClass(internal: SInnerClass): Unit = {
    if (internal == null || internal == this) {
      return
    }
    if (internalClasses.contains(internal)) {
      return
    }
    internalClasses = internalClasses :+ internal
  }

  /**
   * Makes `this` class a subclass of `parent`.
   *
   * If `parent == this` nothing will happen.
   *
   * @param parent the superclass. May be `null` to indicate that there is no superclass (other
   *               than `AnyRef`)
   */
  def addParent(parent: STypedElement): Unit = {
    if (parent == null || parent == this || parentClass == parent || parentInterfaces.contains(parent)) {
      return
    }
    if (parent.isInterface) {
      parentInterfaces = parentInterfaces :+ parent
    } else {
      if (parentClass == null) {
        parentClass = parent
      } else {
        //print error message here
        println("!!!Cannot add parent because their is one parent that is no interface!!!")
      }
    }
    
  }

  /** Gets the references of `this` class. */
  def getReferences(): Seq[SReference] = references

  /**
   * Sets the references of `this` class.
   *
   * @param refs the references. May be `null` to remove all current references.
   */
  def setReferences(refs: Seq[SReference]): Unit = {
    if (refs == null) {
      this.references = Seq.empty
    } else {
      this.references = refs
    }
  }

  /** Gets the attributes of `this` class. */
  def getAttributes(): Seq[SAttribute] = attributes

  /**
   * Sets the attributes of `this` class.
   *
   * @param attrs the attributes. May be `null` to remove all current attributes.
   */
  def setAttributes(attrs: Seq[SAttribute]): Unit = {
    if (attrs == null) {
      this.attributes = Seq.empty
    } else {
      this.attributes = attrs
    }
  }

  /** Provides code that should be executed when invoking the constructor. */
  def getAdditionalConstructorStatements: Seq[SMethodStatement] = constructorStatements

  /**
   * Checks, whether `this` extends any other classes. If this is the case, it is '''not'''
   * considered a ''root class'', otherwise it is.
   * Interfaces are not checked.
   */
  def isRootClass: Boolean = parentClass == null
  
  def isAbsoluteRootClassOrInterface: Boolean = parentClass == null && parentInterfaces.isEmpty

  def getRootClassWithNameAndPackage(n: String, p: String): SClass = {
    var parent = this
    while (!parent.isRootClass) {
      if (parent.getClassParent.isInstanceOf[SClass]) {
        parent = parent.getClassParent.asInstanceOf[SClass]
        if (parent.getName == n && parent.getPackage == p) {
          return parent
        }
      }
    }
    return null
  }

  /**
   * Provides the set of all symbols that have to be imported in order to use `this` complete
   * class.
   */
  def collectImports: Set[String] = getNecessaryImports.map(imp => s"${imp.pckg}.${imp.name}")

  /**
   * Augments the constructor by another statement. The statement will be executed lastly (though
   * this may change due to further modifications of the constructor).
   *
   * @param statement the new statement. May not be `null`
   */
  def augmentConstructor(statement: SMethodStatement): Unit = {
    Objects.requireNonNull(statement, "Statement to add may not be null")
    constructorStatements = constructorStatements :+ statement
  }

  override def getAllConstructorParameters: Seq[SMethodParameter] = {
    val ownParams = getStructuralFeatures.map(attr => SMethodParameter(attr.getName, attr.getTypeElement))
    val parentParams = if (isRootClass) List() else getClassParent.getAllConstructorParameters
    ownParams ++ parentParams
  }

  override def getAttributeConstructorParameters: Seq[SMethodParameter] = {
    val ownParams = getAttributes.map(attr => SMethodParameter(attr.getName, attr.getTypeElement))
    val parentParams = if (isRootClass) List() else getClassParent.getAttributeConstructorParameters
    ownParams ++ parentParams
  }

  override def getInheritanceHierarchy: Seq[STypedElement] = if (isRootClass) List(this) else this +: getClassParent.getInheritanceHierarchy

  /** Provides all types `this` classes accesses in some way. */
  def getUsedTypes: Set[STypedElement] = {
    //iterate over construction features of parent types
    val parentConstructorParamImports: Seq[STypedElement] = if (isRootClass) List() else getClassParent match {
      case parent: SClass =>
        parent.getDeepStructuralFeatures.map(_.getTypeElement)
      case _ => List()
    }

    //attribute types
    val attrTypeImports: Seq[STypedElement] = attributes.map(_.getTypeElement)

    //reference types
    val refTypeImports: Seq[STypedElement] = references.map(_.getTypeElement)

    //method types
    val methodResultImports: Seq[STypedElement] = methods.map(_.result)

    val methodParamImports: Seq[STypedElement] = methods.flatMap(_.params).map(_.getType)

    val methodImplImports: Seq[STypedElement] = methods.flatMap(_.getUsedTypes.toList)

    val allImports: Seq[STypedElement] = (getAllParents
      ++ parentConstructorParamImports
      ++ attrTypeImports
      ++ refTypeImports
      ++ methodResultImports
      ++ methodParamImports
      ++ methodImplImports)

    var allCleanImports: Seq[STypedElement] = Seq.empty

    allImports.foreach(i => {
      i match {
        case seq: GenericSequence =>
          allCleanImports = allCleanImports :+ seq.typeParam
        case t: STypedElement =>
          allCleanImports = allCleanImports :+ t
        //case _ => 
      }
    })

    // create a set to eliminate duplicates
    allCleanImports.toSet
  }

  override def getNecessaryImports: Set[SImport] = {
    var allImports: List[SImport] = List.empty
    //iterate over all parent types
    getAllParents().foreach(p => {
      allImports = includeImportIfNecessary(p, allImports)
    })

    //iterate over construction features of parent types
    if (!isRootClass) {
      getClassParent match {
        case parent: SClass =>
          parent.getDeepStructuralFeatures.foreach(p => {
            allImports = includeImportIfNecessary(p.getTypeElement, allImports)
          })
        case _ =>
      }
    }

    //iterate over all attributes
    attributes.foreach(a => {
      allImports = includeImportIfNecessary(a.getTypeElement, allImports)
    })

    //iterate over all references
    references.foreach(r => {
      allImports = includeImportIfNecessary(r.getTypeElement, allImports)
    })

    //iterated over all methods
    methods.foreach(m => {
      //iterate over result types
      allImports = includeImportIfNecessary(m.result, allImports)
      //iterate over parameter types
      m.params.foreach(p => {
        allImports = includeImportIfNecessary(p.getType, allImports)
      })
      //iterate over used types in the method
      m.getUsedTypes.foreach(t => {
        allImports = includeImportIfNecessary(t, allImports)
      })
    })

    //iterate over all construction statements
    constructorStatements.foreach(s => {
      s.usedTypes.foreach(t => {
        allImports = includeImportIfNecessary(t, allImports)
      })
    })

    // create a set to eliminate duplicates
    (allImports).toSet
  }

  override def accept(visitor: SModelVisitor): Unit = {
    attributes.foreach(_.accept(visitor))
    references.foreach(_.accept(visitor))
    methods.foreach(_.accept(visitor))
    visitor.visit(this)
  }

  protected def canEqual(other: Any): Boolean = other.isInstanceOf[SClass]

  /**
   * Checks if some class has to be imported in order to be usable from `this` class. If this
   * is the case it will wrap the necessary import in a `List` for further usage.
   *
   * @param sPackage the package of the class
   * @param sClass the class
   * @return an empty list if the class does not need to be imported, or the necessary import
   *         otherwise
   */
  private def includeImportIfNecessary(sTypeElement: STypedElement, list: List[SImport]): List[SImport] = {
    //catch GenericSequence types and there use internal type param
    var t = sTypeElement
    sTypeElement match {
      case seq: GenericSequence =>
        t = seq.typeParam
      case _ =>
    }
    //return only if new package
    if (t.sPackage != this.sPackage && t.sPackage != "") list :+ SImport(t.sPackage, t.getName) else list
  }

  override def equals(other: Any): Boolean = other match {
    case that: SClass =>
      (that canEqual this) &&
        getName == that.getName &&
        sPackage == that.sPackage
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(getName, sPackage)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }

  override def toString: String = s"SC: $getName($sPackage, ${attributes.map(_.getName).mkString(", ")} ## ${getOnlyDeepAttributes.map(_.getName).mkString(", ")} || $isAbstract, $isInterface)"

}
