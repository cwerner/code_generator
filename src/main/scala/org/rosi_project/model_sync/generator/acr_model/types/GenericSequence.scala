package org.rosi_project.model_sync.generator.acr_model.types

import org.rosi_project.model_sync.generator.acr_model.{GenericSType, SMethodParameter, STypedElement}

abstract class GenericSequence(name: String, elemType: STypedElement) extends GenericSType(name, typeParam = elemType) {
  
  override def getAllConstructorParameters: Seq[SMethodParameter] = Seq(SMethodParameter("elems", elemType))

  override def toString: String = s"Generic Sequence: ${elemType}"
}