package org.rosi_project.model_sync.generator.acr_model.types

import org.rosi_project.model_sync.generator.acr_model._

/** Wraps a Scala `Set` for some type.
  */
class SSet(elemType: STypedElement) extends GenericSequence("Set", elemType) {

}

/** The companion provides `apply` and `unapply` methods.
  */
object SSet {

  def apply(elemType: STypedElement): SSet = new SSet(elemType)

  def unapply(arg: SSet): Option[STypedElement] = Some(arg.typeParam)

}