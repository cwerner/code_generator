package org.rosi_project.model_sync.generator.acr_model

object SClassType extends Enumeration {
  val normalClass, caseClass, abstactClass, normalTrait, normalObject = Value
}