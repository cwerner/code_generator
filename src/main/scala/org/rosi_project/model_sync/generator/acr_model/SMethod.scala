package org.rosi_project.model_sync.generator.acr_model

import java.util.Objects
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes

// TODO use a simple DSL to represent the method body?

/** Abstraction of a method.
  *
  * @author Rico Bergmann
  */
class SMethod(name: String,
              val result: STypedElement,
              val params: Seq[SMethodParameter],
              var implementation: Seq[SMethodStatement],
              var overrides: Boolean = false)
  extends SNamedModelElement(name) {
  
  private var visibility = MethodVisibility.publicVis
  
  def setVisibility(v: MethodVisibility.Value): Unit = {
    visibility = v
  }
  
  def getVisibility: MethodVisibility.Value = visibility;

  /** Provides the return-type as a `String`.
    *
    * @return the type. Will never be `null` nor empty.
    */
  def getResultType: String = result.getDeepName

  /** Replaces the current body of `this` method.
    *
    * @param impl the new body. May be `null` to remove all current statements, leaving the body
    *             empty.
    */
  def updateImplementation(impl: Seq[SMethodStatement]): Unit = {
    if (impl == null) {
      this.implementation = Seq.empty
    } else {
      this.implementation = impl
    }
  }

  /** Adds another statement to the current method body.
    *
    * @param statement the statement. May not be `null`
    */
  def augmentImplementation(statement: SMethodStatement): Unit = {
    Objects.requireNonNull(statement, "Statement to add may not be null")
    this.implementation = this.implementation :+ statement
  }

  /** Collects all types which need to be imported for `this` method to use.
    */
  def getUsedTypes: Set[STypedElement] = {
    val resultType = result match {
      case GenericSType(_, _, typeParam) => Seq(result, typeParam)
      case PredefTypes.Unit => Seq.empty
      case _ => Seq(result)
    }
    val paramTypes = params.map(_.getType)
    val implementationTypes = implementation.map(_.usedTypes).fold(Set.empty)((s1, s2) => s1 ++ s2)
    (resultType ++ paramTypes).toSet ++ implementationTypes
  }

  override def accept(visitor: SModelVisitor): Unit = visitor.visit(this)

  protected def canEqual(other: Any): Boolean = other.isInstanceOf[SMethod]

  override def equals(other: Any): Boolean = other match {
    case that: SMethod =>
      (that canEqual this) &&
        getName == that.getName &&
        result == that.result &&
        params == that.params
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(getName, result, params)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }

  override def toString: String = s"$getName(${params.map(_.getName).mkString(", ")}): ${result.getName}"

}
