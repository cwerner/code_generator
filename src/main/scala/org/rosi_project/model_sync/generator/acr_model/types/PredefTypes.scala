package org.rosi_project.model_sync.generator.acr_model.types

import org.rosi_project.model_sync.generator.acr_model.{GenericSType, SType, STypedElement}

/** Contains a number of types that are part of the JSE as well as some more advanced Scala types.
  *
  * This should prevent creating them over and over again every time a `SType` should be set to any
  * of these types.
  *
  * @author Rico Bergmann
  */
object PredefTypes {
  
  /** `Convert Java list to Scala listApp` */
  val ScalaConverter = SType("_", "scala.collection.JavaConverters")
  
  /** `App` */
  val App = SType("App")
  
  /** `Char` */
  val Char = SType("Char")
  
  /** `Byte` */
  val Byte = SType("Byte")
  
  /** `Short` */
  val Short = SType("Short")
    
  /** `Integer` */
  val Integer = SType("Int")
    
  /** `Long` */
  val Long = SType("Long")
    
  /** `Float` */
  val Float = SType("Float")
    
  /** `Double` */
  val Double = SType("Double")
  
  /** `Boolean` */
  val Boolean = SType("Boolean")
  
  /** `String` */
  val String = SType("String")  

  /** `java.lang.Object` */
  val Object = SType("Object")

  /** `java.io.File` */
  val File = SType("File", "java.io")
    
  /** `java.util.Date` */
  val Date = SType("Date", "java.util")

  /**  `java.lang.reflect.Parameter` */
  val Parameter = SType("Parameter", "java.lang.reflect")
  
  /** `Unit` The empty type. */
  val Unit = SType("Unit")

  /** Wrapper for `AnyRef`. */
  val AnyRef = SType("AnyRef")
  
  /** Wrapper for `Any`. */
  val Any = SType("Any")

  /** `java.lang.Class[typ]`
    *
    * @param typ the type parameter for the class type
    */
  def classOf(typ: STypedElement): GenericSType = new GenericSType("Class", typeParam = typ)

  /** `Option[typ]`
    *
    * @param typ the type parameter for the Option type
    */
  def option(typ: STypedElement): GenericSType = new GenericSType("Option", typeParam = typ)

}
