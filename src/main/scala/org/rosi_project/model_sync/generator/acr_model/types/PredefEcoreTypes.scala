package org.rosi_project.model_sync.generator.acr_model.types

import org.rosi_project.model_sync.generator.acr_model.SType

/** Contains a number of types that are part of the Ecore package.
  *
  * This should prevent creating them over and over again every time a `SType` should be set to any
  * of these types.
  *
  * @author Christopher Werner
  */
object PredefEcoreTypes {
  
  /**  `org.eclipse.emf.common.util.URI` */
  val URI = SType("URI", "org.eclipse.emf.common.util")
  
  /**  `org.eclipse.emf.ecore.resource.Resource` */
  val Resource = SType("Resource", "org.eclipse.emf.ecore.resource")
  
  /**  `org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl` */
  val XMIResourceFactoryImpl = SType("XMIResourceFactoryImpl", "org.eclipse.emf.ecore.xmi.impl")
  
  /**  `org.eclipse.emf.ecore.EObject` */
  val EcoreObject = SType("EObject", "org.eclipse.emf.ecore")
  
  /**  `org.eclipse.emf.ecore.EStructuralFeature` */
  val EcoreStructuralFeature = SType("EStructuralFeature", "org.eclipse.emf.ecore")
  
  /**  `org.eclipse.emf.ecore.EOperation` */
  val EcoreOperation = SType("EOperation", "org.eclipse.emf.ecore")
  
  val ResourceSetImpl = SType("ResourceSetImpl", "org.eclipse.emf.ecore.resource.impl")
}