package org.rosi_project.model_sync.generator.acr_model

import org.rosi_project.model_sync.generator.support.Assert

/**
 * Complex implementation of the [[SModel]].
 *
 * @author Christopher Werner
 */
class ComplexSModel(name: String, sourceName: String, nsUri: String) extends SModel(name, sourceName, nsUri) {

  private var modelClasses: Set[SClass] = Set.empty
  private var providerClasses: Set[SClass] = Set.empty
  private var relationalCompartments: Set[SClass] = Set.empty
  private var viewCompartments: Set[SClass] = Set.empty
  private var joinClasses: Set[SClass] = Set.empty
  private var sEnums: Set[SEnum] = Set.empty

  /** Provides all model classes in `this` model. */
  override def getModelClasses: Set[SClass] = modelClasses

  /** Provides all provider classes in `this` model. */
  override def getProviderClasses: Set[SClass] = providerClasses

  /** Provides all relational compartments in `this` model. */
  override def getRelationalCompartments: Set[SClass] = relationalCompartments

  /** Provides all view compartments in `this` model. */
  override def getViewCompartments: Set[SClass] = viewCompartments

  /** Provides all the classes in `this` model. */
  override def getAllClasses: Set[SClass] = modelClasses ++ providerClasses ++ relationalCompartments ++ viewCompartments

  /** Provides all join classes in `this` model. */
  override def getJoinClasses: Set[SClass] = joinClasses

  /** Provides all model enums in `this` model. */
  override def getModelEnums: Set[SEnum] = sEnums

  /**
   * Extends the model by a new provder class.
   *
   * @param mClass the class to add. May never `null`.
   */
  override def addProviderClass(mClass: SClass): Unit = {
    Assert.notNull(mClass, "Class may not be null")
    providerClasses += mClass
  }

  /**
   * Extends the model by a new relational compartment.
   *
   * @param mClass the class to add. May never `null`.
   */
  override def addRelationalCompartment(mClass: SClass): Unit = {
    Assert.notNull(mClass, "Class may not be null")
    relationalCompartments += mClass
  }

  /**
   * Extends the model by a new view compartment.
   *
   * @param mClass the class to add. May never `null`.
   */
  override def addViewCompartment(mClass: SClass): Unit = {
    Assert.notNull(mClass, "Class may not be null")
    viewCompartments += mClass
  }

  /**
   * Extends the model by a new class.
   *
   * @param mClass the class to add. May never `null`.
   */
  override def addModelClass(mClass: SClass): Unit = {
    Assert.notNull(mClass, "Class may not be null")
    modelClasses += mClass
  }

  /**
   * Extends the model by a new join class.
   *
   * @param mClass the class to add. May never `null`.
   */
  override def addJoinClass(mClass: SClass): Unit = {
    Assert.notNull(mClass, "Class may not be null")
    joinClasses += mClass
  }

  /**
   * Extends the model by a new enum.
   *
   * @param mEnum the enum to add. May never `null`.
   */
  override def addModelEnums(mEnum: SEnum): Unit = {
    Assert.notNull(mEnum, "Enum may not be null")
    sEnums += mEnum
  }

  override def accept(visitor: SModelVisitor): Unit = {
    getAllClasses.foreach(_.accept(visitor))
    visitor.visit(this)
  }

  def canEqual(other: Any): Boolean = other.isInstanceOf[SimpleSModel]

  override def equals(other: Any): Boolean = other match {
    case that: ComplexSModel =>
      (that canEqual this) &&
        modelClasses == that.modelClasses &&
        providerClasses == that.providerClasses &&
        relationalCompartments == that.relationalCompartments &&
        viewCompartments == that.viewCompartments &&
        joinClasses == that.joinClasses &&
        sEnums == that.sEnums
    case _ => false
  }

  override def addOtherModel(model: SModel): SModel = {
    modelClasses = modelClasses ++ model.getModelClasses
    providerClasses = providerClasses ++ model.getProviderClasses
    relationalCompartments = relationalCompartments ++ model.getRelationalCompartments
    viewCompartments = viewCompartments ++ model.getViewCompartments
    joinClasses = joinClasses ++ model.getJoinClasses
    sEnums = sEnums ++ model.getModelEnums
    return this
  }

  override def hashCode(): Int = {
    val state = Seq(getAllClasses)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}