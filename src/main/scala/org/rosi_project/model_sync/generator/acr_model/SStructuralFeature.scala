package org.rosi_project.model_sync.generator.acr_model

/** Representation of structural features like attributes and references.
  *
  * @author Christopher Werner
  */
abstract class SStructuralFeature (_name: String, private val sType: STypedElement) extends SNamedModelElement(_name) {
  
  var isFinal: Boolean = false 
  private var visibility = MethodVisibility.publicVis
  
  def setVisibility(v: MethodVisibility.Value): Unit = {
    visibility = v
  }
  
  def getVisibility: MethodVisibility.Value = visibility;
  
  /** The type of 'this' structural feature.
    *
    * @return
    */
  def getTypeElement: STypedElement = sType
  
}