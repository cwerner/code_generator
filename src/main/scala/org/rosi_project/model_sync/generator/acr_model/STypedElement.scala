package org.rosi_project.model_sync.generator.acr_model

/** Abstraction of "elements" that may be used in source code where a type is expected.
  *
  * Currently there are two different kinds of such elements:
  *
  *  - instances of [[SClass]] are supplied by the user
  *  - instances of [[SType]] are provided by the Java runtime (they are classes of the standard
  *    Java libraries or others)
  *
  * @author Rico Bergmann
  */
abstract class STypedElement (_name: String,
             val sPackage: String,
             val sClassType: SClassType.Value) extends SNamedModelElement(_name) {
  
  /** Checks, whether `this` is part of the default package */
  def isDefaultPackage: Boolean = sPackage == ""

  /** The package containing `this` type.
    *
    * @return
    */
  def getPackage: String = sPackage

  /** The constructor with all parameters of `this` type for construction. */
  def getAllConstructorParameters: Seq[SMethodParameter] = Seq.empty
  
  /** The constructor with only attribute parameters of `this` type for construction. */
  def getAttributeConstructorParameters: Seq[SMethodParameter] = Seq.empty

  /** The inheritance hierarchy provides information about the types `this` extends. It will start
    * with `this` (thus the hierarchy will 'never' be empty) and end with the root type. `AnyRef`
    * will be ignored however.
    */
  def getInheritanceHierarchy: Seq[STypedElement] = Seq.empty
  
  /**
   * Proof the hierarchical equality between two STypedElement
   */
  def proofHierarchicalEquality(sType: STypedElement): Boolean = sType == this

  /** Provides all classes that need to be imported for `this` type.
    */
  def getNecessaryImports: Set[SImport] = Set.empty
  
  def isInterface: Boolean = sClassType == SClassType.normalTrait
  
  def isAbstract: Boolean = sClassType == SClassType.abstactClass
  
  def isObject: Boolean = sClassType == SClassType.normalObject
  
  def isCaseClass: Boolean = sClassType == SClassType.caseClass
  
  def isNormalClass: Boolean = sClassType == SClassType.normalClass
  
  override def toString: String = s"STE: $getName($sPackage, $isInterface)"

}
