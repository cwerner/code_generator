package org.rosi_project.model_sync.generator.acr_model

import org.rosi_project.model_sync.generator.support.ExtendedString.stringToExtended


/** Simple representation of a getter method.
  *
  * @author Rico Bergmann
  */
class SGetter(struc: SStructuralFeature) extends SMethod(
  name = s"get${struc.getName.firstLetterToUpperCase}",
  result = struc.getTypeElement,
  params = Seq.empty,
  implementation = Seq(SMethodStatement(content = struc.getName, usedTypes = Set(struc.getTypeElement)))) {

}
