package org.rosi_project.model_sync.generator.acr_model

/** Adds support for generic types to [[SType]] instances.
  *
  * @param name `this` type's name
  * @param sPackage the package in which `this` type resides
  * @param typeParam the type parameter (a.k.a.  the ''Generic'')
  *
  * @author Rico Bergmann
  */
class GenericSType(name: String, sPackage: String = "", val typeParam: STypedElement) extends SType(name, sPackage) {

  override def getName: String = s"$name[${typeParam.getName}]"
  
  override def getDeepName: String = s"$name[${typeParam.getDeepName}]"

  override def getNecessaryImports: Set[SImport] = if (typeParam.getPackage.isEmpty || typeParam.getPackage == sPackage)
    typeParam.getNecessaryImports + SImport(typeParam.getPackage, typeParam.getName) else typeParam.getNecessaryImports
}

/** The companion provides an `unapply` implementation.
  */
object GenericSType {

  def unapply(arg: GenericSType): Option[(String, String, STypedElement)] = Some((arg.getName, arg.sPackage, arg.typeParam))

}
