package org.rosi_project.model_sync.generator.acr_model

import org.rosi_project.model_sync.generator.support.ExtendedString.stringToExtended
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes

/** Remove an element from a Set of elements.
  *
  * @author Christopher Werner
  */
class SSetterRemove(struc: SStructuralFeature, inner: STypedElement) extends SMethod(
  name = s"remove${struc.getName.firstLetterToUpperCase}",
  result = PredefTypes.Unit,
  params = Seq(SMethodParameter(struc.getName.head.toString, inner)),
  implementation = Seq(SMethodStatement(content = s"${struc.getName} -= ${struc.getName.head}", usedTypes = Set(struc.getTypeElement)))) {

  if (!STypeRegistry.isDefaultNotNullType(struc.getTypeElement.getName)) {
    implementation = Seq(SMethodStatement(content = s"require(${struc.getName.head} != null)"),
      SMethodStatement(content = s"require(${struc.getName}.contains(${struc.getName.head}))"),
      SMethodStatement(content = s"${struc.getName} -= ${struc.getName.head}", usedTypes = Set(struc.getTypeElement)))
  } else {
    implementation = Seq(SMethodStatement(content = s"${struc.getName} -= ${struc.getName.head}", usedTypes = Set(struc.getTypeElement)))
  }
}