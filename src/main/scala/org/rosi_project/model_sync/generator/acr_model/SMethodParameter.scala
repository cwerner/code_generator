package org.rosi_project.model_sync.generator.acr_model

/** Wrapper for parameters that need to be passed to a method.
  *
  * @author Rico Bergmann
  */
case class SMethodParameter(private val name: String, private val paramType: STypedElement) {

  /** Provides the type of `this` parameter as a `String`.
    *
    * @return the type. Will never be `null` nor empty.
    */
  def getTypeName: String = paramType.getName
  
  /** Provides the deep type name of `this` parameter as a `String`.
    *
    * @return the type. Will never be `null` nor empty.
    */
  def getDeepTypeName: String = paramType.getDeepName
  
  /** Provides the type of `this` parameter as a `STypedElement`.
    *
    * @return the type. Will never be `null` nor empty.
    */
  def getType: STypedElement = paramType
  
  /** Provides the name of `this` parameter as a `String`.
    *
    * @return the name. Will never be `null` nor empty.
    */
  def getName: String = name

  override def toString: String = s"$getName: $getTypeName"

}
