package org.rosi_project.model_sync.generator.acr_model

import java.util.Objects

class SInnerViewRelationalClass(_name: String,
             _sPackage: String = "",
             _sClassType: SClassType.Value = SClassType.normalClass,
             _externalClass: SClass,
             val sumSource: SRelationalCompartmentClass,
             val viewSource: SInnerViewNaturalClass,
             val viewTarget: SInnerViewNaturalClass) extends SInnerClass(_name, _sPackage, _sClassType, _externalClass) {
  
  Objects.requireNonNull(sumSource, "SUM Source class may not be null")
  Objects.requireNonNull(viewSource, "View Source class may not be null")
  Objects.requireNonNull(viewTarget, "View Target class may not be null")
}