package org.rosi_project.model_sync.generator.acr_model

// TODO fix cyclic imports with classes from acr package

/** Runs an algorithm on model instances.
  *
  * @see [[https://en.wikipedia.org/wiki/Visitor_pattern Visitor pattern @ Wikipedia]]
  * @author Rico Bergmann
  */
trait SModelVisitor {

  /** Runs the algorithm as appropriate for the whole model.
    *
    * @param sModel the model to run the algorithm on. Whether it may be `null` is implementation
    *               specific.
    */
  def visit(sModel: SModel): Unit

  /** Runs the algorithm as appropriate for a single class of the model.
    *
    * @param sClass the class to run the algorithm on
    */
  def visit(sClass: SClass): Unit

  /** Runs the algorithm as appropriate for an attribute of a model class.
    *
    * @param sAttr the attribute to run the algorithm on. Whether it may be `null` is implementation
    *              specific.
    */
  def visit(sAttr: SAttribute): Unit
  
  /** Runs the algorithm as appropriate for a reference of a model class.
    *
    * @param sRef the reference to run the algorithm on. Whether it may be `null` is implementation
    *             specific.
    */
  def visit(sRef: SReference): Unit

  /** Runs the algorithm as appropriate for a method of a model class.
    *
    * @param sMethod the method to run the algorithm on. Whether it may be `null` is implementation
    *                specific.
    */
  def visit(sMethod: SMethod): Unit

  /** Runs the algorithm as appropriate for a type of the model.
    *
    * @param sType the type to run the algorithm on. Whether it may be `null` is implementation
    *              specific.
    */
  def visit(sType: SType): Unit

}
