package org.rosi_project.model_sync.generator.acr_model

import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes
import org.eclipse.emf.ecore.EClass
import org.rosi_project.model_sync.generator.acr_model.types.PredefEcoreTypes

/** Repository to keep track of all types and classes of model. It ensures that all attributes,
  * methods, etc. reference the same type instances and thus prevent duplication and conflicting
  * states. The registry should be treated as "''single point of truth''".
  *
  * @author Rico Bergmann
  */
object STypeRegistry {
  
  private var registeredTypes: Map[STypedElement, EClass] = Map.empty
  
  private val defaultTypesNull: Map[STypedElement, EClass] = Map(
    PredefTypes.Date -> null,
    PredefTypes.Object -> null,
    PredefTypes.String -> null,
  )
  
  private val defaultTypesNotNull: Map[STypedElement, EClass] = Map(
    PredefTypes.Boolean -> null,
    PredefTypes.Byte -> null,
    PredefTypes.Char -> null,
    PredefTypes.Double -> null,
    PredefTypes.Float -> null,
    PredefTypes.Integer -> null,
    PredefTypes.Long -> null,
    PredefTypes.Short -> null,
  )
  
  /*private val ecoreTypes: Map[STypedElement, EClass] = Map(
    PredefEcoreTypes.EcoreObject -> null,
    PredefEcoreTypes.EcoreStructuralFeature -> null,
  )*/
  
  def getFromClass(cls: EClass): STypedElement = {
    registeredTypes.foreach(r => {
      if (r._2 == cls) {
        return r._1
      }
    })
    null
  }

  registeredTypes ++= defaultTypesNull
  registeredTypes ++= defaultTypesNotNull
  //registeredTypes ++= ecoreTypes
  
  /**
   * Return true if this name comes from a standard null type.
   * 
   * @param name the name that must be proven.
   * @return `true` if it is a default type. Otherwise return false.
   */
  def isDefaultNullType(name: String): Boolean = {
    val existingNull: Option[STypedElement] = defaultTypesNull.keySet.find(existing => existing.getName == name)
    return !existingNull.isEmpty
  }
  
  /**
   * Return true if this name comes from a standard not null type.
   * 
   * @param name the name that must be proven.
   * @return `true` if it is a default type. Otherwise return false.
   */
  def isDefaultNotNullType(name: String): Boolean = {
    val existingNotNull: Option[STypedElement] = defaultTypesNotNull.keySet.find(existing => existing.getName == name)
    return !existingNotNull.isEmpty
  }
  
  /**
   * Return true if this name comes from a standard type.
   * 
   * @param name the name that must be proven.
   * @return `true` if it is a default type. Otherwise return false.
   */
  def isDefaultType(name: String): Boolean = {
    val existingNotNull: Option[STypedElement] = defaultTypesNotNull.keySet.find(existing => existing.getName == name)
    val existingNull: Option[STypedElement] = defaultTypesNull.keySet.find(existing => existing.getName == name)
    return !existingNotNull.isEmpty || !existingNull.isEmpty
  }

  /** Registers a type if it is not already known.
    *
    * @param theType the new type. May never be `null`.
    * @return `theType` if it was indeed unknown before. Otherwise the currently registered type.
    */
  def addType(theType: STypedElement, cls: EClass): STypedElement = {
    val existing: Option[STypedElement] = registeredTypes.keySet.find(existing => existing.getName == theType.getName && existing.getPackage == theType.getPackage)
    if (existing.isEmpty) {
      registeredTypes = registeredTypes + (theType -> cls)
      //registeredTypes += theType
      theType
    } else {
      existing.get
    }
  }

  /** Searches for a type.
    *
    * @param name the type's name
    * @param sPackage the package that contains the type
    * @return the type if it was found
    */
  def query(name: String, sPackage: String): Option[STypedElement] = {  
    registeredTypes.keySet.find(t => t.getName == name && t.getPackage == sPackage)
  }

  /** Provides all types that are currently in the repository.
    */
  def allTypes: Set[STypedElement] = registeredTypes.keySet

  override def toString: String = s"Registry: $registeredTypes"
}
