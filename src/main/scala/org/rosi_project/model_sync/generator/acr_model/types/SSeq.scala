package org.rosi_project.model_sync.generator.acr_model.types

import org.rosi_project.model_sync.generator.acr_model.{GenericSType, SMethodParameter, STypedElement}

/** Wraps a Scala `Seq` for some type.
  */
class SSeq(elemType: STypedElement) extends GenericSequence("Seq", elemType) {

}

/** The companion provides `apply` and `unapply` methods.
  */
object SSeq {

  def apply(elemType: STypedElement): SSeq = new SSeq(elemType)

  def unapply(arg: SSeq): Option[STypedElement] = Some(arg.typeParam)

}
