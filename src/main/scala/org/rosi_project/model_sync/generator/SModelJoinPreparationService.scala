package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression
import org.rosi_project.model_sync.generator.acr_model.SModel
import org.rosi_project.model_sync.generator.sync.JoinGeneratingVisitor
import org.rosi_project.model_sync.generator.sync.ModelJoinViewGeneratingVisitor

/** Simple service to create join classes and objects for ModelJoin.
  *
  * @author Christopher Werner
  */
class SModelJoinPreparationService {
  
  /** Augments a [[SModel]] with new JoinClasses and JoinObjects.
    *
    * @param sModel the model to augment
    * modelJoin input
    */
  def prepareModel(sModel: SModel, joinExpression: ModelJoinExpression): Unit = {
    val joinVisitor = new JoinGeneratingVisitor(joinExpression)
    val modelJoinVisitor = new ModelJoinViewGeneratingVisitor(joinExpression)
    //order is important !!!!    
    sModel.accept(joinVisitor)
    sModel.accept(modelJoinVisitor)
  }
}