package org.rosi_project.model_sync.generator.env

import java.net.URLClassLoader

/** Utility to provide high level access to the application's ''classpath'' as well as some basic
  * transformations.
  *
  * @author Rico Bergmann
  */
object JClassPath {

  /** Provides a platform-specific version of the ''classpath''. All entries may than be resolved as
    * files on the current OS.
    *
    * @param cp the ''classpath'' to adapt
    */
  def adaptClassPathToOSEnv(cp: List[String]): List[String] = {
    sys.props("os.name").toLowerCase match {
      case win if win contains "windows" =>
        cp.map(entry => {
          val WinUrl = """file:/(.*)""".r
          entry match {
            case WinUrl(path) => path
            case p => p
          }
        }).map(_.replaceAll("/", "\\\\").replaceAll("%20", " "))
      case unix =>
        val UnixUrl = """file:(.*)""".r
        cp.map {
          case UnixUrl(path) => path
          case p => p
        }
    }
  }

  /** Gets the ''classpath'' as it is currently used by this application.
    */
  def fetchCurrentClassPath: List[String] = {
    val ctxLoader = Thread.currentThread().getContextClassLoader
    ctxLoader match {
      case urlCL: URLClassLoader =>
        urlCL.getURLs.toList.map {
          _.toString
        }
      case wrappedCL =>
        wrappedCL.getParent match {
          case urlCL: URLClassLoader =>
            urlCL.getURLs.toList.map {
              _.toString
            }
        }
      case something => sys.error(s"Could not unwrap class loader: $something")
    }
  }

}
