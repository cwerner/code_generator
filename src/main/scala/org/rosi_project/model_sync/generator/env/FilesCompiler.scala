package org.rosi_project.model_sync.generator.env

import java.io.PrintWriter

import scala.reflect.io.File
import scala.tools.nsc.reporters.ConsoleReporter
import scala.tools.nsc.{GenericRunnerSettings, Global, Settings}

/** Utility to compile all set of Scala source files to Java byte code.
  *
  * @param outDir the directory where the compiled files should placed
  * @author Rico Bergmann
  */
class FilesCompiler(outDir: File) {

  /** Compiles all given files.
    */
  def run(filesToCompile: List[File]): Unit = {
    // see: https://stackoverflow.com/a/20323371/5161760
    try {
      val out = new PrintWriter(System.out)
      val compilationSettings: Settings = new GenericRunnerSettings(out.println)
      // just re-use the whole classpath
      compilationSettings.classpath.value = JClassPath.adaptClassPathToOSEnv(JClassPath.fetchCurrentClassPath).distinct.mkString(File.pathSeparator)
      //println(s"Using classpath ${compilationSettings.classpath.value}")
      compilationSettings.outdir.value = outDir.toAbsolute.toString
      val reporter = new ConsoleReporter(compilationSettings)
      val compiler = new Global(compilationSettings, reporter)
      val runner = new compiler.Run
      runner.compile(filesToCompile.map(_.toAbsolute.toString))
    } catch {
      case e: Exception => throw new CompilationException(e)
    }
  }

}

/** Exception to indicate that the compilation process failed.
  *
  * @param cause the causing exception
  */
class CompilationException(cause: Exception) extends RuntimeException(cause)
