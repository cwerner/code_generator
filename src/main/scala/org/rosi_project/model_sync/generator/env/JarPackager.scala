package org.rosi_project.model_sync.generator.env

import java.io.{BufferedInputStream, FileInputStream, FileOutputStream}
import java.util.jar.{Attributes, JarEntry, JarOutputStream, Manifest}

import scala.reflect.io.{Directory, File}
import scala.util.control.Breaks.{break, breakable}

/** Utility to put all files from some directory into a JAR.
  *
  * This will simply copy all files from the source directory into the JAR at the target directory.
  *
  * @param inputDir  the directory where all source files reside.
  * @param outputDir the target directory where the JAR will be created
  * @param jarName   the name of the JAR file to create
  * @author Rico Bergmann
  */
class JarPackager(inputDir: File, outputDir: File, jarName: String = "model.jar") {

  /** Starts the packaging process.
    */
  def run(): Unit = {
    // see: https://stackoverflow.com/a/1281295/5161760
    try {
      val mf = new Manifest
      mf.getMainAttributes.put(Attributes.Name.MANIFEST_VERSION, "1.0")
      val targetJar = new JarOutputStream(new FileOutputStream(outputDir + File.separator + jarName), mf)
      var filesToAdd: List[java.io.File] = inputDir.jfile.listFiles.toList

      while (filesToAdd.nonEmpty) {
        val file = filesToAdd.head
        var fname = file.getAbsolutePath.replace(inputDir.jfile.getAbsolutePath + File.separator, "").replaceAll("\\\\", "/")
        //println(s"Adding to JAR: $fname")
        if (file.isDirectory) {
          if (!fname.endsWith("/")) {
            fname += "/"
          }
          filesToAdd ++= file.listFiles.toList

          val entry = new JarEntry(fname)
          entry.setTime(file.lastModified)
          targetJar.putNextEntry(entry)
          targetJar.closeEntry()
        } else {
          val entry = new JarEntry(fname)
          entry.setTime(file.lastModified)
          targetJar.putNextEntry(entry)

          val in = new BufferedInputStream(new FileInputStream(file))
          var buffer = new Array[Byte](1024)
          breakable {
            while (true) {
              val bytesRead = in.read(buffer)
              if (bytesRead == -1) {
                break
              }
              targetJar.write(buffer, 0, bytesRead)
            }
          }
          targetJar.closeEntry()
          in.close()
        }

        filesToAdd = filesToAdd.tail
      }

      targetJar.close()
    } catch {
      case e: Exception => throw new JarPackaginException(e)
    }
  }

}

/** Exception to indicate that the packaging process failed.
  *
  * @param cause the causing exception
  */
class JarPackaginException(cause: Exception) extends RuntimeException(cause)
