package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model.SMethod
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes
import org.rosi_project.model_sync.generator.acr_model.SMethodStatement
import org.rosi_project.model_sync.generator.acr_model.SClass

object ToStringMethods {
  
  def normalToStringMethod(cls: SClass): SMethod = {
    new SMethod(
        name = "toString",
        result = PredefTypes.String,
        params = Seq.empty,
        implementation = Seq(SMethodStatement((List(s"""  "${cls.getName}:" """) ++ cls.getOnlyDeepAttributes.map(attr => s""" " ${attr.getName}=" + ${attr.getName} """)).mkString(" + "))),
        overrides = true
      )
  } 
  
  def joinToStringMethod(s: String): SMethod = {
    new SMethod(
        name = "toString",
        result = PredefTypes.String,
        params = Seq.empty,
        implementation = Seq(SMethodStatement(s"""  "JOIN ${s}: " + baseObj + " " + otherObj """)),
        overrides = true
      )
  }
  
  def onlyStringToStringMethod(s: String): SMethod = {
    new SMethod(
        name = "toString",
        result = PredefTypes.String,
        params = Seq.empty,
        implementation = Seq(SMethodStatement(s"""  "${s}" """)),
        overrides = true
      )
  }
}