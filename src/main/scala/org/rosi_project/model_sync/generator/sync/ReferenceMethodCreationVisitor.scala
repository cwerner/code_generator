package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.acr_model.types.GenericSequence

/**
 * Create the methods to get the references from each instance. Is only possible in combination with sync, because of role functionality.
 */
class ReferenceMethodCreationVisitor extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {
    var typeName = "Set.empty"
    var addElement = "result +="

    sModel.getRelationalCompartments.foreach(cls => {
      val relComp = cls.asInstanceOf[SRelationalCompartmentClass]
      //iterate over all relational compartments
      val methodSrc = new SMethod(
        name = s"get${relComp.connectedRef.getName.capitalize}",
        result = relComp.connectedRef.getTypeElement,
        params = Seq.empty,
        implementation = Seq.empty)

      //proof return type
      if (relComp.connectedRef.getTypeElement.isInstanceOf[GenericSequence]) {
        typeName = "Set.empty"
        addElement = "result +="
      } else {
        typeName = "null"
        addElement = "result ="
      }
      //make impl
      var implSrc: Seq[SMethodStatement] = Seq.empty
      implSrc = implSrc :+ SMethodStatement(content = s"var result: ${relComp.connectedRef.getTypeElement.getName} = ${typeName}", usedTypes = Set(PredefRsumTypes.IRELATIONROLE_STYPE))
      implSrc = implSrc :+ SMethodStatement(content = s"""this.roles.filter(r => r.isInstanceOf[${PredefRsumTypes.IRELATIONROLE_STYPE.getName}] && 
        r.asInstanceOf[${PredefRsumTypes.IRELATIONROLE_STYPE.getName}].getOuterCompartment.isInstanceOf[${relComp.getName}])
        .foreach(r => {
          ${addElement} r.asInstanceOf[${PredefRsumTypes.IRELATIONROLE_STYPE.getName}].getOuterCompartment.getTargetIns.asInstanceOf[${relComp.tClass.getName}]
        })""")
      implSrc = implSrc :+ SMethodStatement(content = "result")
      methodSrc.implementation = implSrc
      relComp.sClass.asInstanceOf[SClass].addMethod(methodSrc)
      //proof opposite reference
      if (relComp.connectedRef.hasOpposite) {
        val methodTrg = new SMethod(
          name = s"get${relComp.connectedRef.oppositeRef.getName.capitalize}",
          result = relComp.connectedRef.oppositeRef.getTypeElement,
          params = Seq.empty,
          implementation = Seq.empty)

        //proof return type
        if (relComp.connectedRef.oppositeRef.getTypeElement.isInstanceOf[GenericSequence]) {
          typeName = "Set.empty"
          addElement = "result +="
        } else {
          typeName = "null"
          addElement = "result ="
        }
        //make impl
        var implTrg: Seq[SMethodStatement] = Seq.empty
        implTrg = implTrg :+ SMethodStatement(content = s"var result: ${relComp.connectedRef.oppositeRef.getTypeElement.getName} = ${typeName}", usedTypes = Set(PredefRsumTypes.IRELATIONROLE_STYPE))
        implTrg = implTrg :+ SMethodStatement(content = s"""this.roles.filter(r => r.isInstanceOf[${PredefRsumTypes.IRELATIONROLE_STYPE.getName}] && 
          r.asInstanceOf[${PredefRsumTypes.IRELATIONROLE_STYPE.getName}].getOuterCompartment.isInstanceOf[${relComp.getName}])
          .foreach(r => {
            ${addElement} r.asInstanceOf[${PredefRsumTypes.IRELATIONROLE_STYPE.getName}].getOuterCompartment.getSourceIns.asInstanceOf[${relComp.sClass.getName}]
          })""")
        implTrg = implTrg :+ SMethodStatement(content = "result")
        methodTrg.implementation = implTrg
        relComp.tClass.asInstanceOf[SClass].addMethod(methodTrg)
      }
    })
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }

  override def visit(sRef: SReference): Unit = {
    // pass
  }
}