package org.rosi_project.model_sync.generator.sync

import scala.collection.JavaConverters._
import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.util.ClassAndInstance
import org.eclipse.emf.ecore.EObject
import org.rosi_project.model_sync.generator.util.InstanceLine
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.common.util.EList
import org.rosi_project.model_sync.util.EMFUtilForGenerator
import java.util.List
import org.rosi_project.model_sync.generator.PackageNames

class InstanceCombiGenerator(val clsins: ClassAndInstance) extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {
    //println("++++++++++++++++++++++++++++++++++++++++++++++")
    val example = new SClass("ExampleCombiCase", PackageNames.examplePkgName, SClassType.normalObject)
    val contents = clsins.obj.eAllContents().asScala
    var counter = 0
    var lines: Seq[InstanceLine] = Seq.empty
    var generationLines: Seq[InstanceLine] = Seq.empty
    var sType = STypeRegistry.getFromClass(clsins.obj.eClass())
    if (sType != null) {
      lines = lines :+ new InstanceLine(counter, clsins.obj, sType)
      counter += 1
    }
    contents.foreach(o => {
      sType = STypeRegistry.getFromClass(o.eClass())
      if (sType != null) {
        lines = lines :+ new InstanceLine(counter, o, sType)
        counter += 1
      }
    })    
    //add values for instances    
    lines.foreach(l => {
      l.usedType.getAllConstructorParameters.foreach(cp => {
        if (!STypeRegistry.isDefaultType(cp.getType.getName)) {
          val structs: Seq[EStructuralFeature] = l.obj.eClass().getEAllStructuralFeatures.asScala
          structs.foreach(att => {
            if (att.getName == cp.getName) {
              val obj = l.obj.eGet(att)
              if (obj.isInstanceOf[EObject]) {
                lines.foreach(lnow => {
                  if (lnow.obj == obj.asInstanceOf[EObject]) {
                    //println("Found: " + lnow.counter) .filter(_.isInstanceOf[SRelationalCompartmentClass])
                    sModel.getRelationalCompartments.foreach(rc => {
                      val realRc = rc.asInstanceOf[SRelationalCompartmentClass]
                      //println(" AN: " + att.getName + " l1: " + l.usedType.getName + " l2: " + lnow.usedType.getName + " RC: " + realRc.connectedRef.getName + " SN: " + realRc.sClass.getName + " TN: " + realRc.tClass.getName)
                      if (realRc.connectedRef.getName == att.getName && l.usedType.proofHierarchicalEquality(realRc.sClass) && lnow.usedType.proofHierarchicalEquality(realRc.tClass)) {
                        val s = s"(new ${realRc.getName}(${l.getName()}, ${lnow.getName()})).initialize()"
                        generationLines = generationLines :+ new InstanceLine(0, null, realRc, s)
                      }
                    })
                  }
                })
              } else {
                val listi: List[EObject] = EMFUtilForGenerator.getList(obj)
                val liste = listi.asScala
                liste.foreach(eo => {
                  lines.foreach(lnow => {
                    if (lnow.obj == eo) {
                      sModel.getRelationalCompartments.foreach(rc => {
                        val realRc = rc.asInstanceOf[SRelationalCompartmentClass]
                        //println(" AN: " + att.getName + " l1: " + l.usedType.getName + " l2: " + lnow.usedType.getName + " RC: " + realRc.connectedRef.getName + " SN: " + realRc.sClass.getName + " TN: " + realRc.tClass.getName)
                        if (realRc.connectedRef.getName == att.getName && l.usedType.proofHierarchicalEquality(realRc.sClass) && lnow.usedType.proofHierarchicalEquality(realRc.tClass)) {
                          val s = s"(new ${realRc.getName}(${l.getName()}, ${lnow.getName()})).initialize()"
                          generationLines = generationLines :+ new InstanceLine(0, null, realRc, s)
                        }
                      })
                    }
                  })
                })
              }
            }
          })
        }
      })
    })
    
    //construction of instances
    lines.foreach(l => {
      example.augmentConstructor(new SMethodStatement(l.getLine(), Set(l.usedType)))
    })
    
    //construction of connections
    generationLines.foreach(l => {
      example.augmentConstructor(new SMethodStatement(l.getLine(), Set(l.usedType)))
    })
    
    
    example.addParent(PredefTypes.App)
    sModel.addProviderClass(example)
  }

  def setValues(eo: EObject, lines: Seq[InstanceLine]): String = {
    lines.foreach(lnow => {
      if (lnow.obj == eo) {
        return lnow.getName()
      }
    })
    ""
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sRef: SReference): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }

}