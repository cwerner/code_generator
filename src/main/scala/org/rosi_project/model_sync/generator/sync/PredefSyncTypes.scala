package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model.SType
import org.rosi_project.model_management.core.PlayerSync

object PredefSyncTypes {
  
  private val PLAYER_SYNC_CLASS = classOf[PlayerSync]
  val         PLAYER_SYNC_STYPE = SType(PLAYER_SYNC_CLASS.getSimpleName, PLAYER_SYNC_CLASS.getPackage.getName)
}