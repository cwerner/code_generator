package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.support.ExtendedString.stringToExtended

object HelperFunctions {
  def classNameCreation(t: STypedElement): String = s"className.isInstanceOf[${t.getName}]"
  
  def initialAttributeDoing(mp: SMethodParameter, sfs: Seq[SStructuralFeature]): String = {
    sfs.foreach(s => {
      if (s.getName == mp.getName) {
        return mp.getName
      }
    })
    return classEmptyConstructorParameterCreation(mp)
  }
  
  def attributeEqualCreation(a: SStructuralFeature): String = s"base.get${a.getName.firstLetterToUpperCase}() == other.get${a.getName.firstLetterToUpperCase}()"
  
  def classEmptyConstructorParameterCreation(t: SMethodParameter): String = {
    classEmptyType(t.getTypeName)
  }
  
  def classEmptyType(s: String): String = {
    s match {
      case "Boolean" => return "false"
      case "Double" => return "0"
      case "Float" => return "0"
      case "Long" => return "0"
      case "Integer" => return "0"
      case "Int" => return "0"
      case "Short" => return "0"
      case "Byte" => return "0"
      case _ => return "null"
    } 
  }
}