package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.acr_model.types._
import org.rosi_project.model_sync.model_join.representation.grammar._
import scala.collection.JavaConverters._
import scala.collection.convert.AsScalaConverters
import org.rosi_project.model_management.sum.join.RsumJoinType
import org.rosi_project.model_sync.generator.PackageNames

class ModelJoinViewGeneratingVisitor(joinExpression: ModelJoinExpression) extends SModelVisitor {

  var viewCompartment: SViewClass = null
  var refClasses: Seq[SClass] = Seq.empty
  var refRelationalClasses: Seq[SClass] = Seq.empty
  var newInternalRoles: Seq[SInnerViewNaturalClass] = Seq.empty
  var newInternalRelationalRoles: Seq[SInnerViewRelationalClass] = Seq.empty
  var allImportClasses: Set[STypedElement] = Set.empty

  override def visit(sModel: SModel): Unit = {
    var counter = 0
    joinExpression.getJoins.forEach(j => {
      refClasses = Seq.empty
      refRelationalClasses = Seq.empty
      newInternalRoles = Seq.empty
      newInternalRelationalRoles = Seq.empty
      allImportClasses = Set.empty
      viewCompartment = new SViewClass(joinExpression.getName() + counter, PackageNames.viewPkgName)
      counter += 1
      viewCompartment.addParent(PredefRsumTypes.IVIEW_COMPARTMENT_STYPE)

      val joinType = STypeRegistry.query(j.getTarget.getResourceName, j.getTarget.getResourcePath)
      if (!joinType.isEmpty) {
        if (joinType.get.isInstanceOf[SJoinClass]) {
          viewCompartment.addJoinObject(joinType.get.asInstanceOf[SJoinClass])
        }
        createClasses(asScalaBuffer(j.getKeepsList).toSet, joinType.get.asInstanceOf[SClass], sModel)
      }

      //Now fill all natural internal roles with functionality
      newInternalRoles.foreach(inner => {
        //set inheritance to abstract view role
        if (inner.isRootClass) {
          inner.addParent(PredefRsumTypes.AVIEW_ROLE_STYPE)
        }
        //add initialize method if necessary
        if (!inner.isAbstract && !inner.isInterface) {
          inner.addMethod(ViewMethods.getIsRelationalMethod(false))
          inner.addMethod(ViewMethods.getCreationObjectMethod(inner))
        }
        //add to string method
        inner.addMethod(ToStringMethods.onlyStringToStringMethod("VNR: " + inner.getName))
        //add delete method
        inner.addMethod(ViewMethods.getDeleteElementNaturalMethod(inner))
        //add getter and setter for attributes in the inner class
        inner.getAttributes.foreach(attr => {
          inner.addMethod(ViewMethods.getViewAttributeGetter(attr))
          inner.addMethod(ViewMethods.getViewAttributeSetter(attr))
        })
      })

      //add methods that must be override from the view trait
      viewCompartment.addMethod(ViewMethods.getIsViewableMethod((refClasses ++ refRelationalClasses).toSet))
      viewCompartment.addMethod(ViewMethods.getViewNameMethod(viewCompartment))
      viewCompartment.addMethod(ViewMethods.getGetNaturalRoleMethod(newInternalRoles.toSet))
      viewCompartment.addMethod(ViewMethods.getGetRelationalRoleMethod(newInternalRelationalRoles.toSet))
      viewCompartment.addMethod(ToStringMethods.onlyStringToStringMethod("VC: " + viewCompartment.getName))
      //Create methods to create elements from the view
      ViewMethods.getCreateNaturalRoleMethods(newInternalRoles.toSet).foreach(viewCompartment.addMethod(_))

      viewCompartment.augmentConstructor(new SMethodStatement("", allImportClasses))

      //add the view to the list for generating views
      sModel.addViewCompartment(viewCompartment)
    })
  }

  def createClasses(keeps: Set[KeepExpression], cls: SClass, sModel: SModel): SInnerViewNaturalClass = {
    var internalClass: SInnerViewNaturalClass = null
    var newAtts: Seq[SAttribute] = Seq.empty
    if (refClasses.contains(cls)) {
      newInternalRoles.foreach(nir => {
        if (nir.sumSource == cls) {
          internalClass = nir
          newAtts = nir.getAttributes()
        }
      })
    } else {
      refClasses = refClasses :+ cls
      internalClass = new SInnerViewNaturalClass(cls.getName + PackageNames.viewRolePostName,
        _sClassType = cls.sClassType,
        _externalClass = viewCompartment,
        sumSource = cls)
      newInternalRoles = newInternalRoles :+ internalClass
      allImportClasses = allImportClasses + cls
      viewCompartment.addInternalClass(internalClass)
    }

    //without keep lists
    //KeepAggregateExpression KeepAttributesExpression KeepCalculatedExpression
    //sets the attributes from the source class
    var features: Set[SStructuralFeature] = Set.empty
    if (cls.isInstanceOf[SJoinClass]) {
      val jCls = cls.asInstanceOf[SJoinClass]
      features = jCls.innerAttributes
      if (jCls.joinType != RsumJoinType.theta) {
        //if natural or outer join add all join attributes
        jCls.joinAttributes.foreach(a => {
          var newAtt = SAttribute(a.getName, a.getTypeElement)
          newAtt.setVisibility(MethodVisibility.protectedVis)
          newAtt.isFinal = true
          newAtts = newAtts :+ newAtt
        })
      }
    } else {
      features = cls.getDeepStructuralFeatures.toSet
    }

    //println("+++ KeepAttributesExpression")
    keeps.filter(_.isInstanceOf[KeepAttributesExpression]).foreach(ja => {
      val keepAtt = ja.asInstanceOf[KeepAttributesExpression]
      keepAtt.getAttributes.forEach(attExp => {
        //println("N: " + attExp.getAttributeName + " P: " + attExp.getContainingClass.getPackage + " CN: " + attExp.getContainingClass.getClassName)
        features.filter(_.getName == attExp.getAttributeName).foreach(a => {
          var isIn = false
          internalClass.getAttributes().foreach(ain => {
            if (ain.getName == a.getName && ain.getTypeElement.getName == a.getTypeElement.getName) {
              isIn = true
            }
          })
          if (!isIn) {
            //Proof if attribute must be in then add it to view class   
            var newAtt = SAttribute(a.getName, a.getTypeElement)
            newAtt.setVisibility(MethodVisibility.protectedVis)
            newAtt.isFinal = true
            newAtts = newAtts :+ newAtt
          }
        })
      })
    })
    internalClass.setAttributes(newAtts)
    //println("+++ KeepAggregateExpression")
    keeps.filter(_.isInstanceOf[KeepAggregateExpression]).foreach(ja => {
      //TODO: later
    })
    //println("+++ KeepCalculatedExpression")
    keeps.filter(_.isInstanceOf[KeepCalculatedExpression]).foreach(ja => {
      //TODO: later
    })
    //println("+++ KeepSubTypeExpression")
    //with keep lists
    //KeepSubTypeExpression KeepSuperTypeExpression KeepReferenceExpression (List of Keeps)
    keeps.filter(_.isInstanceOf[KeepSubTypeExpression]).foreach(ja => {
      val keepAtt = ja.asInstanceOf[KeepSubTypeExpression]
      sModel.getModelClasses.filter(m => m.getName == keepAtt.getType.getClassName && m.getPackage == keepAtt.getType.getPackage).foreach(mc => {
        val parent = mc.getRootClassWithNameAndPackage(cls.getName, cls.getPackage)
        if (parent != null) {
          val innerParent = createClasses(asScalaBuffer(keepAtt.getKeeps).toSet, mc, sModel)
          innerParent.addParent(internalClass)
        }
      })
    })
    //println("+++ KeepSuperTypeExpression")
    keeps.filter(_.isInstanceOf[KeepSuperTypeExpression]).foreach(ja => {
      val keepAtt = ja.asInstanceOf[KeepSuperTypeExpression]
      //println("TC: " + keepAtt.getTarget + " TC: " + keepAtt.getType)
      val parent = cls.getRootClassWithNameAndPackage(keepAtt.getType.getClassName, keepAtt.getType.getPackage)
      if (parent != null) {
        val innerParent = createClasses(asScalaBuffer(keepAtt.getKeeps).toSet, parent, sModel)
        internalClass.addParent(innerParent)
      }
    })
    //println("+++ KeepReferenceExpression")
    keeps.filter(_.isInstanceOf[KeepReferenceExpression]).foreach(ja => {
      val keepAtt = ja.asInstanceOf[KeepReferenceExpression]
      //println("AN: " + keepAtt.getAttribute.getAttributeName + " " + keepAtt.getAttribute.getContainingClass.getPackage + " " + keepAtt.getAttribute.getContainingClass.getClassName)
      sModel.getRelationalCompartments.foreach(me => {
        var rc = me.asInstanceOf[SRelationalCompartmentClass]
        if (rc.getPackage == keepAtt.getAttribute.getContainingClass.getPackage) {
          if (rc.sClass.getName == keepAtt.getAttribute.getContainingClass.getClassName || rc.tClass.getName == keepAtt.getAttribute.getContainingClass.getClassName) {
            if (rc.connectedRef.getName == keepAtt.getAttribute.getAttributeName || (rc.connectedRef.hasOpposite && rc.connectedRef.oppositeRef.getName == keepAtt.getAttribute.getAttributeName)) {
              var sourceRole: SInnerViewNaturalClass = null
              var targetRole: SInnerViewNaturalClass = null
              var sourceAtt: SAttribute = null
              var targetAtt: SAttribute = null
              var createNormal = false
              var createOpposite = false
              if (rc.sClass.getName == keepAtt.getAttribute.getContainingClass.getClassName) {
                //current is source
                sourceRole = internalClass
                var tClass = rc.tClass.asInstanceOf[SClass]
                sModel.getModelClasses.filter(m => m.getName == keepAtt.getTarget.getClassName && m.getPackage == keepAtt.getTarget.getPackage).foreach(cs => {
                  val parent = cs.getRootClassWithNameAndPackage(rc.tClass.getName, rc.tClass.getPackage)
                  if (parent != null) {
                    tClass = cs
                  }
                })
                targetRole = createClasses(asScalaBuffer(keepAtt.getKeeps).toSet, tClass, sModel)
                createNormal = true
              } else {
                //current is target
                var sClass = rc.sClass.asInstanceOf[SClass]
                sModel.getModelClasses.filter(m => m.getName == keepAtt.getTarget.getClassName && m.getPackage == keepAtt.getTarget.getPackage).foreach(cs => {
                  val parent = cs.getRootClassWithNameAndPackage(rc.sClass.getName, rc.sClass.getPackage)
                  if (parent != null) {
                    sClass = cs
                  }
                })
                sourceRole = createClasses(asScalaBuffer(keepAtt.getKeeps).toSet, sClass, sModel)
                targetRole = internalClass
                createOpposite = true
              }
              sourceAtt = new SAttribute("source", sourceRole)
              targetAtt = new SAttribute("target", targetRole)
              sourceAtt.isFinal = true
              sourceAtt.setVisibility(MethodVisibility.privateVis)
              targetAtt.isFinal = true
              targetAtt.setVisibility(MethodVisibility.privateVis)
              //now find the right relational compartment
              refRelationalClasses = refRelationalClasses :+ rc
              //Iterate over all Relational compartments and create relational internal role classes
              val internalRelClass = new SInnerViewRelationalClass(rc.getName + PackageNames.viewRolePostName, _externalClass = viewCompartment,
                sumSource = rc, viewSource = sourceRole, viewTarget = targetRole)
              internalRelClass.setAttributes(sourceAtt ++ targetAtt)
              internalRelClass.addMethod(ViewMethods.getIsRelationalMethod(true))
              internalRelClass.addMethod(ViewMethods.getCreationObjectMethod(internalRelClass, createNormal, createOpposite))
              internalRelClass.addMethod(ViewMethods.getDeleteElementRelationMethod(internalRelClass, createNormal, createOpposite))
              internalRelClass.addMethod(ToStringMethods.onlyStringToStringMethod("VRR: " + internalRelClass.getName))
              ViewMethods.getSourceAndTargetGetter(internalRelClass).foreach(internalRelClass.addMethod(_))
              internalRelClass.addParent(PredefRsumTypes.AVIEW_ROLE_STYPE)
              ViewMethods.createAllReferenceLinks(internalRelClass, createNormal, createOpposite)
              newInternalRelationalRoles = newInternalRelationalRoles :+ internalRelClass
              allImportClasses = allImportClasses + sourceRole.sumSource + targetRole.sumSource + rc.tClass + rc.sClass
              viewCompartment.addInternalClass(internalRelClass)
            }
          }
        }
      })
    })
    internalClass
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }

  override def visit(sRef: SReference): Unit = {
    // pass
  }
}