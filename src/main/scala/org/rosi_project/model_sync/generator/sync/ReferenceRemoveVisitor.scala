package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._

class ReferenceRemoveVisitor extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {
    sModel.getModelClasses.foreach(cls => {      
      //remove all references from the class
      cls.setReferences(null)
    })
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }
  
  override def visit(sRef: SReference): Unit = {
    // pass
  }

}