package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.acr_model.{SMethodStatement, SModel}

/** Contains services to adapt a [[SModel]] for usage in a ''Synchronization context''.
  *
  * @author Rico Bergmann
  */
package object sync {

  implicit def t2SeqT[T](t: T): Seq[T] = Seq(t)

  implicit def string2SeqMethodImpl(impl: String): Seq[SMethodStatement] = Seq(SMethodStatement(impl))

}
