package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.acr_model.types.SList
import org.rosi_project.model_sync.generator.acr_model.types.GenericSequence
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes

class RelationCompartmentGeneratingVisitor extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {
    var allReferences: Seq[SReference] = Seq.empty
    var newClasses: Seq[SRelationalCompartmentClass] = Seq.empty
    sModel.getModelClasses.foreach(cls => {      
      cls.getReferences.foreach(ref => {        
        //he should only create a relational compartment for one relation and not for both directions
        var shouldRun = true
        if (ref.hasOpposite) {          
          //has opposite
          if (!allReferences.contains(ref.oppositeRef) && !allReferences.contains(ref)) {
            //did not handle this reference yet
            if (ref.getImportant == ref.oppositeRef) {
              //should run the other one
              shouldRun = false         
              //add this element to the list
              allReferences = allReferences ++ ref
            } else {
              //should run this one
              shouldRun = true   
              //add the other one to the list
              allReferences = allReferences ++ ref.oppositeRef
            }
          } else if (allReferences.contains(ref)) {
            //this reference is in the list so he should not work with it
            shouldRun = false
          }     
        }                
        
        if (shouldRun) { 
          var attType = ref.getTypeElement
          if (attType.isInstanceOf[GenericSequence])
          {
            attType = (attType.asInstanceOf[GenericSequence]).typeParam
          }
            
          val className = cls.getName + ref.getName.capitalize + attType.getName;
          //println("ClassName: " + className)                                
          val newClass = new SRelationalCompartmentClass(className, cls.getPackage, connectedRef = ref, sClass = cls, tClass = attType)
          
          //attributes
          val sourceAtt = SAttribute("sInstance", cls)
          val targetAtt = SAttribute("tInstance", attType)
          sourceAtt.setVisibility(MethodVisibility.privateVis)
          sourceAtt.isFinal = true
          targetAtt.setVisibility(MethodVisibility.privateVis)
          targetAtt.isFinal = true
          newClass.setAttributes(sourceAtt ++ targetAtt)
                      
          //internal classes
          val sourceClass = new SInnerClass("Source", externalClass = newClass)
          val targetClass = new SInnerClass("Target", externalClass = newClass)
          
          //create own toString Method
          sourceClass.addMethod(new SMethod(
            name = "toString",
            result = PredefTypes.String,
            params = Seq.empty,
            implementation = Seq(SMethodStatement(s"""  "S: (" + sInstance + ")" """)),
            overrides = true))          
          targetClass.addMethod(new SMethod(
            name = "toString",
            result = PredefTypes.String,
            params = Seq.empty,
            implementation = Seq(SMethodStatement(s"""  "T: (" + tInstance + ")" """)),
            overrides = true))  
                    
          //relation types with containment and opposite values
          if (ref.containment) {
            if (ref.hasOpposite) {
              sourceClass.addParent(PredefRsumTypes.ICOMPOSITION_SOURCE_STYPE)
              targetClass.addParent(PredefRsumTypes.ICOMPOSITION_TARGET_STYPE)
              newClass.addParent(PredefRsumTypes.ICOMPOSITION_STYPE)
            } else {
              sourceClass.addParent(PredefRsumTypes.IDIRECT_COMPOSITION_SOURCE_STYPE)
              targetClass.addParent(PredefRsumTypes.IDIRECT_COMPOSITION_TARGET_STYPE)
              newClass.addParent(PredefRsumTypes.IDIRECT_COMPOSITION_STYPE)
            }
          } else {  
            if (ref.hasOpposite) {
              sourceClass.addParent(PredefRsumTypes.IASSOCIATION_SOURCE_STYPE)
              targetClass.addParent(PredefRsumTypes.IASSOCIATION_TARGET_STYPE)
              newClass.addParent(PredefRsumTypes.IASSOCIATION_STYPE)
            } else {
              sourceClass.addParent(PredefRsumTypes.IDIRECT_ASSOCIATION_SOURCE_STYPE)
              targetClass.addParent(PredefRsumTypes.IDIRECT_ASSOCIATION_TARGET_STYPE)
              newClass.addParent(PredefRsumTypes.IDIRECT_ASSOCIATION_STYPE)
            }
          }
            
          newClass.addInternalClass(sourceClass)
          newClass.addInternalClass(targetClass)
            
          //method implementation
          val impl: Seq[SMethodStatement] = (SMethodStatement("this.source = new Source()")
            ++ SMethodStatement("this.target = new Target()")
            ++ SMethodStatement("sInstance play this.source")
            ++ SMethodStatement("tInstance play this.target"))
                   
          //add methods to relational compartment
          newClass.addMethod(new SMethod (
            name = "internalInitialize", 
            result = PredefTypes.Unit, 
            params = Seq.empty, 
            implementation = impl, 
            overrides = true))            
          newClass.addMethod(new SMethod(
            name = "toString",
            result = PredefTypes.String,
            params = Seq.empty,
            implementation = Seq(SMethodStatement(s"""  "[$className " + source + ", " + target + "]" """)),
            overrides = true))
            
          newClass.addMethod(new SMethod (
            name = "getSourceIns", 
            result = cls, 
            params = Seq.empty, 
            implementation = Seq(SMethodStatement(s"return sInstance")), 
            overrides = false))
            
          newClass.addMethod(new SMethod (
            name = "getTargetIns", 
            result = attType, 
            params = Seq.empty, 
            implementation = Seq(SMethodStatement(s"return tInstance")), 
            overrides = false))
                        
          newClasses = newClasses :+ newClass
        }
      })
    })
    newClasses.foreach(classe => {      
      sModel.addRelationalCompartment(classe)
    })
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }
  
  override def visit(sRef: SReference): Unit = {
    // pass
  }

}