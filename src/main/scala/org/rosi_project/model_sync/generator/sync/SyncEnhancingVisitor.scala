package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._

/**
 * Augments [[SClass SClasses]] with the necessary method calls to make it usable in a
 * synchronization context.
 *
 * The following modifications are performed:
 *  - the class (or its furthest parent) becomes a subclass of [[PlayerSync]]
 *  - [[PlayerSync.buildClass()]] will be called in the constructor
 *  - each setter will notify the synchronization context about the change
 *
 * @see [[ISynchronizationCompartment]]
 * @author Rico Bergmann
 */
class SyncEnhancingVisitor() extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {
    sModel.getModelClasses.foreach(cls => {
      //TODO: do not have in mind trait extends abstract extends trait hierarchy    
      if (cls.isAbsoluteRootClassOrInterface) {
        cls.addParent(PredefSyncTypes.PLAYER_SYNC_STYPE)
      }
      cls.getMethods.foreach(m => {
        extractSetterAttr(m).foreach(attr => m.augmentImplementation(SMethodStatement(s"+this $attr")))
      })
    })
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sRef: SReference): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }

  /**
   * Tries to get the attribute's name from a setter method.
   *
   * A ''valid'' setter will have the following signature: `setXyz(x: T): Unit` (the parameter's
   * name does not matter).
   *
   * Mind that the first letter will be left uppercase (i.e. `Xyz` will be returned although the
   * actual attribute may be `xyz`)
   *
   * @param sMethod the method to analyze. May be any method (not necessarily a setter) but never
   *                `null`.
   * @return the attribute's name if `sMethod` was a valid setter. '''The first letter will be left
   *         uppercase.'''
   */
  private def extractSetterAttr(sMethod: SMethod): Option[String] = {
    sMethod.getName match {
      case SyncEnhancingVisitor.Setter(attrName) =>
        Option("syncSet" + attrName + " ()")
      case SyncEnhancingVisitor.Adder(attrName) =>
        Option("syncAdd" + attrName + s" (${attrName.charAt(0).toLower})")
      case SyncEnhancingVisitor.Remover(attrName) =>
        Option("syncRemove" + attrName + s" (${attrName.charAt(0).toLower})")
      case _ =>
        None
    }

  }

}

/**
 * The companion contains some static values.
 */
object SyncEnhancingVisitor {
  private val Setter = """set([A-Z][a-zA-z0-9]*)""".r
  private val Adder = """add([A-Z][a-zA-z0-9]*)""".r
  private val Remover = """remove([A-Z][a-zA-z0-9]*)""".r
  private val PLAYER_SYNC_INIT = SMethodStatement("buildClass()")
}
