package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.PackageNames
import org.rosi_project.model_sync.generator.acr_model.types.PredefEcoreTypes
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes
import org.rosi_project.model_sync.generator.acr_model.types.GenericSequence
import org.rosi_project.model_sync.generator.GeneratorConfig
import org.rosi_project.model_sync.generator.Creation

/**
 * Read EMF and Ecore models.
 *
 * @author Christopher Werner
 */
class SumModelReadingVisitor(config: GeneratorConfig) extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {
    //println("++++++++++++++++++++++++++++++++++++++++++++++")
    val creatorSum = new SClass("Creation" + sModel.getName + "Sum", PackageNames.creationPkgName)
    val creatorSync = new SClass("Creation" + sModel.getName + "Sync", PackageNames.creationPkgName)
    val creatorInterface = new SClass("ICreation" + sModel.getName, PackageNames.creationPkgName, SClassType.normalTrait)
    val loader = new SClass("Loader" + sModel.getName, PackageNames.creationPkgName)
    
    creatorSum.addParent(creatorInterface)
    creatorSync.addParent(creatorInterface)

    val loadEcore = new SMethod(
      name = "loadEcore",
      result = PredefEcoreTypes.EcoreObject,
      params = Seq(SMethodParameter("pathMeta", PredefTypes.String), SMethodParameter("pathInstance", PredefTypes.String)),
      implementation = Seq(
        SMethodStatement(content = s"require(null != pathMeta && pathMeta.nonEmpty && null != pathInstance && pathInstance.nonEmpty)"),
        SMethodStatement(content = s"val resourceSet = new ResourceSetImpl()", usedTypes = Set(PredefEcoreTypes.ResourceSetImpl)),
        SMethodStatement(content = s"resourceSet.getResourceFactoryRegistry.getExtensionToFactoryMap.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl())", usedTypes = Set(PredefEcoreTypes.XMIResourceFactoryImpl, PredefEcoreTypes.Resource)),
        SMethodStatement(content = s"val ressourceMeta = resourceSet.getResource(URI.createFileURI(pathMeta), true)", usedTypes = Set(PredefEcoreTypes.URI)),
        SMethodStatement(content = s"val packageMeta = ressourceMeta.getContents().get(0)"),
        SMethodStatement(content = s"require(null != ressourceMeta)"),
        SMethodStatement(content = s"require(!ressourceMeta.getContents.isEmpty)"),
        SMethodStatement(content = s"""resourceSet.getPackageRegistry().put("${sModel.getNsURI}", packageMeta);"""),
        SMethodStatement(content = s"val ressourceModel = resourceSet.getResource(URI.createURI(pathInstance), true);"),
        SMethodStatement(content = s"return ressourceModel.getContents().get(0)")))
    loader.addMethod(loadEcore)

    val createModelInstanceSum = new SMethod(
      name = s"create${sModel.getName}Instance",
      result = PredefTypes.Unit,
      params = Seq(SMethodParameter("obj", PredefEcoreTypes.EcoreObject), SMethodParameter("creator", creatorInterface)),
      implementation = Seq(
        SMethodStatement(content = s"createObj(obj, creator)"),
        SMethodStatement(content = s"obj.eAllContents().asScala.foreach(o => {", usedTypes = Set(PredefTypes.ScalaConverter)),
        SMethodStatement(content = s"createObj(o, creator)"),
        SMethodStatement(content = s"})"),
        SMethodStatement(content = s"createRef(obj, creator)"),
        SMethodStatement(content = s"obj.eAllContents().asScala.foreach(o => {"),
        SMethodStatement(content = s"createRef(o, creator)"),
        SMethodStatement(content = s"})")))
    loader.addMethod(createModelInstanceSum)

    val createObj = new SMethod(
      name = "createObj",
      result = PredefTypes.Unit,
      params = Seq(SMethodParameter("obj", PredefEcoreTypes.EcoreObject), SMethodParameter("creator", creatorInterface)),
      implementation = Seq.empty)
    createObj.setVisibility(MethodVisibility.privateVis)
    var createObjImpl = Seq(SMethodStatement(content = s"var objName = obj.eClass.getName"),
      SMethodStatement(content = s"objName match {"))

    val createRef = new SMethod(
      name = "createRef",
      result = PredefTypes.Unit,
      params = Seq(SMethodParameter("o1", PredefEcoreTypes.EcoreObject), SMethodParameter("creator", creatorInterface)),
      implementation = Seq.empty)
    createRef.setVisibility(MethodVisibility.privateVis)
    var stringList: Set[String] = Set.empty
    var createRefImpl = Seq(SMethodStatement(content = s"val o2 = o1.eGet(sf).asInstanceOf[EObject]"),
      SMethodStatement(content = s"val o1Name = o1.eClass().getName"),
      SMethodStatement(content = s"val o2Name = o2.eClass().getName"))

    creatorSum.augmentConstructor(SMethodStatement(content = s"var mapping: Map[EObject, Object] = Map.empty", usedTypes = Set(PredefEcoreTypes.EcoreObject)))
    creatorSync.augmentConstructor(SMethodStatement(content = s"var mapping: Map[EObject, Object] = Map.empty", usedTypes = Set(PredefEcoreTypes.EcoreObject)))

    sModel.getModelClasses.filter(c => !c.isAbstract && !c.isInterface).foreach(c => {
      //method for trait
      val methodTrait = new SMethod(
        name = s"create${c.getName}",
        result = PredefTypes.Unit,
        params = c.getAttributeConstructorParameters :+ SMethodParameter("id", PredefEcoreTypes.EcoreObject),
        implementation = Seq.empty)
      creatorInterface.addMethod(methodTrait)

      //method for sync
      val methodSum = new SMethod(
        name = s"create${c.getName}",
        result = PredefTypes.Unit,
        params = c.getAttributeConstructorParameters :+ SMethodParameter("id", PredefEcoreTypes.EcoreObject),
        implementation = Seq(
          SMethodStatement(content = s"mapping += (id -> new ${c.getName}(${c.getAttributeConstructorParameters.map(m => m.getName).mkString(", ")}))", usedTypes = Set(c))),
        overrides = true)
      creatorSum.addMethod(methodSum)

      //method for sync
      val methodSync = new SMethod(
        name = s"create${c.getName}",
        result = PredefTypes.Unit,
        params = c.getAttributeConstructorParameters :+ SMethodParameter("id", PredefEcoreTypes.EcoreObject),
        implementation = Seq(
          SMethodStatement(content = s"mapping += (id -> new ${c.getName}(${c.getAllConstructorParameters.map(m => if (m.getType.isInstanceOf[GenericSequence]) "Set.empty" else if (m.getType.isInstanceOf[SClass]) "null" else m.getName).mkString(", ")}))", usedTypes = Set(c))), //TODO
        overrides = true)
      creatorSync.addMethod(methodSync)

      var types = c.getAttributeConstructorParameters.map(m => m.getType)
      var s = c.getAttributeConstructorParameters.map(m => s"""obj.eGet(obj.eClass().getEStructuralFeature("${m.getName}"))${if (m.getTypeName == "String") ".toString()" else s".asInstanceOf[${m.getTypeName}${if(m.getType.isInstanceOf[SEnum]) ".Value" else ""}]"} """).mkString(", ")
      if (s.isEmpty()) {
        s = "obj"
      } else {
        s = s + ", obj"
      }
      createObjImpl = createObjImpl :+ SMethodStatement(content = s"""case "${c.getName}" => creator.create${c.getName}(${s})""", types.toSet)
    })

    sModel.getRelationalCompartments.foreach(r => {
      var rc = r.asInstanceOf[SRelationalCompartmentClass]
      //method for trait
      val methodTrait = new SMethod(
        name = s"create${r.getName}",
        result = PredefTypes.Unit,
        params = Seq(SMethodParameter("s", PredefEcoreTypes.EcoreObject), SMethodParameter("t", PredefEcoreTypes.EcoreObject)),
        implementation = Seq.empty)
      creatorInterface.addMethod(methodTrait)

      //sum method
      val methodSum = new SMethod(
        name = s"create${r.getName}",
        result = PredefTypes.Unit,
        params = Seq(SMethodParameter("s", PredefEcoreTypes.EcoreObject), SMethodParameter("t", PredefEcoreTypes.EcoreObject)),
        implementation = Seq(
          SMethodStatement(content = s"val s1 = mapping.get(s).get.asInstanceOf[${rc.sClass.getName}]"),
          SMethodStatement(content = s"val t1 = mapping.get(t).get.asInstanceOf[${rc.tClass.getName}]"),
          SMethodStatement(content = s"(new ${rc.getName}(s1, t1)).initialize()", usedTypes = Set(rc, rc.sClass, rc.tClass))),
        overrides = true)
      creatorSum.addMethod(methodSum)

      //sync method
      val methodSync = new SMethod(
        name = s"create${r.getName}",
        result = PredefTypes.Unit,
        params = Seq(SMethodParameter("s", PredefEcoreTypes.EcoreObject), SMethodParameter("t", PredefEcoreTypes.EcoreObject)),
        implementation = Seq.empty,
        overrides = true)
      var implMethodSync = Seq(SMethodStatement(content = s"val s1 = mapping.get(s).get.asInstanceOf[${rc.sClass.getName}]", usedTypes = Set(rc.sClass, rc.tClass)),
        SMethodStatement(content = s"val t1 = mapping.get(t).get.asInstanceOf[${rc.tClass.getName}]"))
      if (rc.connectedRef.getTypeElement.isInstanceOf[GenericSequence]) {
        //add -> add method
        implMethodSync = implMethodSync :+ SMethodStatement(content = s"s1.add${rc.connectedRef.getName.capitalize}(t1)")
      } else {
        //add -> set method
        implMethodSync = implMethodSync :+ SMethodStatement(content = s"s1.set${rc.connectedRef.getName.capitalize}(t1)")
      }
      if (rc.connectedRef.hasOpposite) {
        if (rc.connectedRef.oppositeRef.getTypeElement.isInstanceOf[GenericSequence]) {
          //add -> add method
          implMethodSync = implMethodSync :+ SMethodStatement(content = s"t1.add${rc.connectedRef.oppositeRef.getName.capitalize}(s1)")
        } else {
          //add -> set method
          implMethodSync = implMethodSync :+ SMethodStatement(content = s"t1.set${rc.connectedRef.oppositeRef.getName.capitalize}(s1)")
        }
      }
      methodSync.implementation = implMethodSync
      creatorSync.addMethod(methodSync)

      //TODO: anpassen
      val reference = rc.connectedRef.getMinimalConnection
      if (reference != null) {
        if (reference == rc.connectedRef) {
          createRefImpl = createRefImpl :+ SMethodStatement(content = s"""if (o1Name.contains("${rc.sClass.getName}") && sfName == "${reference.getName}" && o2Name.contains("${rc.tClass.getName}")) {""")
          createRefImpl = createRefImpl :+ SMethodStatement(content = s"creator.create${rc.getName}(o1, o2)")
          createRefImpl = createRefImpl :+ SMethodStatement(content = "}")
        } else {
          createRefImpl = createRefImpl :+ SMethodStatement(content = s"""if (o1Name.contains("${rc.tClass.getName}") && sfName == "${reference.getName}" && o2Name.contains("${rc.sClass.getName}")) {""")
          createRefImpl = createRefImpl :+ SMethodStatement(content = s"creator.create${rc.getName}(o2, o1)")
          createRefImpl = createRefImpl :+ SMethodStatement(content = "}")
        }
        stringList += reference.getName
      }
    })

    createObjImpl = createObjImpl :+ SMethodStatement(content = "case _ =>")
    createObjImpl = createObjImpl :+ SMethodStatement(content = "}")
    createObj.implementation = createObjImpl
    loader.addMethod(createObj)    

    createRefImpl = createRefImpl :+ SMethodStatement(content = "}})")
    createRefImpl = Seq(SMethodStatement(content = s"o1.eClass().getEAllReferences.forEach(sf => {"),
      SMethodStatement(content = s"val sfName = sf.getName"),
      SMethodStatement(content = s"""if(${stringList.map(str => s"""sfName == "${str}" """).mkString(" || ")}) {""")) ++ createRefImpl
    createRef.implementation = createRefImpl
    loader.addMethod(createRef)

    //add the new classes as model classes
    if (config.create == Creation.rolesync) {
      sModel.addProviderClass(creatorSync)
    } else {
      sModel.addProviderClass(creatorSum)
    }
    sModel.addProviderClass(creatorInterface)
    sModel.addProviderClass(loader)
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sRef: SReference): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }

}