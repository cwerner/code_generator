package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model.SType
import org.rosi_project.model_sync.generator.acr_model.SEnum
import org.rosi_project.model_management.sum.IViewCompartment
import org.rosi_project.model_management.sum.IViewTypeInfo
import org.rosi_project.model_management.sum.query._
import org.rosi_project.model_management.sum.join._
import org.rosi_project.model_management.sum.compartments._
import org.rosi_project.model_management.sum.roles.IRelationRole

object PredefRsumTypes {

  //JOIN TYPES
  private val IJOIN_COMPARTMENT_CLASS = classOf[IJoinCompartment]
  val         IJOIN_COMPARTMENT_STYPE = SType(IJOIN_COMPARTMENT_CLASS.getSimpleName, IJOIN_COMPARTMENT_CLASS.getPackage.getName) 
  
  private val JOIN_INFO_CLASS = classOf[IJoinInfo]
  val         JOIN_INFO_STYPE = SType(JOIN_INFO_CLASS.getSimpleName, JOIN_INFO_CLASS.getPackage.getName)
  
  private val RSUM_JOIN_TYPE_CLASS = RsumJoinType.getClass
  val         RSUM_JOIN_TYPE_STYPE = new SEnum(RSUM_JOIN_TYPE_CLASS.getSimpleName.replace("$", ""), RSUM_JOIN_TYPE_CLASS.getPackage.getName, Set.empty)
    
  //VIEW TYPES
  private val IVIEW_COMPARTMENT_CLASS = classOf[IViewCompartment]
  val         IVIEW_COMPARTMENT_STYPE = SType(IVIEW_COMPARTMENT_CLASS.getSimpleName, IVIEW_COMPARTMENT_CLASS.getPackage.getName) 
  
  private val VIEWTYPE_INFO_CLASS = classOf[IViewTypeInfo]
  val         VIEWTYPE_INFO_STYPE = SType(VIEWTYPE_INFO_CLASS.getSimpleName, VIEWTYPE_INFO_CLASS.getPackage.getName) 
    
  private val AVIEW_ROLE_CLASS = classOf[IViewCompartment#AViewRole]
  val         AVIEW_ROLE_STYPE = SType(AVIEW_ROLE_CLASS.getSimpleName)//AVIEW_ROLE_CLASS.getPackage.getName)  
  
  //QUERY TYPES
  private val CHECKING_OPTION_CLASS = CheckingOption.getClass
  val         CHECKING_OPTION_STYPE = new SEnum(CHECKING_OPTION_CLASS.getSimpleName.replace("$", ""), CHECKING_OPTION_CLASS.getPackage.getName, Set.empty)
  
  private val QUERY_HELPER_CLASS = classOf[QueryHelper]
  val         QUERY_HELPER_STYPE = SType(QUERY_HELPER_CLASS.getSimpleName, QUERY_HELPER_CLASS.getPackage.getName) 
  
  private val IQUERY_VIEW_COMPARTMENT_CLASS = classOf[QueryFactory]
  val         IQUERY_VIEW_COMPARTMENT_STYPE = SType(IQUERY_VIEW_COMPARTMENT_CLASS.getSimpleName, IQUERY_VIEW_COMPARTMENT_CLASS.getPackage.getName) 
  
  private val AQUERY_VIEW_ROLE_CLASS = classOf[QueryFactory#QueryFactoryElement]
  val         AQUERY_VIEW_ROLE_STYPE = SType(AQUERY_VIEW_ROLE_CLASS.getSimpleName) //, AQUERY_VIEW_ROLE_CLASS.getPackage.getName) 

  //RELATIONAL COMPARTMENT TYPES
  private val IRELATIONROLE_CLASS = classOf[IRelationRole]
  val         IRELATIONROLE_STYPE = SType(IRELATIONROLE_CLASS.getSimpleName, IRELATIONROLE_CLASS.getPackage.getName)
  
  private val IAGGREGTATION_CLASS = classOf[IAggregation]
  val         IAGGREGTATION_STYPE = SType(IAGGREGTATION_CLASS.getSimpleName, IAGGREGTATION_CLASS.getPackage.getName)
  
  private val IAGGREGTATION_SOURCE_CLASS = classOf[IAggregation#IAggregationSource]
  val         IAGGREGTATION_SOURCE_STYPE = SType(IAGGREGTATION_SOURCE_CLASS.getSimpleName, IAGGREGTATION_SOURCE_CLASS.getPackage.getName)
  
  private val IAGGREGTATION_TARGET_CLASS = classOf[IAggregation#IAggregationTarget]
  val         IAGGREGTATION_TARGET_STYPE = SType(IAGGREGTATION_TARGET_CLASS.getSimpleName, IAGGREGTATION_TARGET_CLASS.getPackage.getName)
  
  private val IASSOCIATION_CLASS = classOf[IAssociation]
  val         IASSOCIATION_STYPE = SType(IASSOCIATION_CLASS.getSimpleName, IASSOCIATION_CLASS.getPackage.getName)
  
  private val IASSOCIATION_SOURCE_CLASS = classOf[IAssociation#IAssociationSource]
  val         IASSOCIATION_SOURCE_STYPE = SType(IASSOCIATION_SOURCE_CLASS.getSimpleName, IASSOCIATION_SOURCE_CLASS.getPackage.getName)
  
  private val IASSOCIATION_TARGET_CLASS = classOf[IAssociation#IAssociationTarget]
  val         IASSOCIATION_TARGET_STYPE = SType(IASSOCIATION_TARGET_CLASS.getSimpleName, IASSOCIATION_TARGET_CLASS.getPackage.getName)
  
  private val ICOMPOSITION_CLASS = classOf[IComposition]
  val         ICOMPOSITION_STYPE = SType(ICOMPOSITION_CLASS.getSimpleName, ICOMPOSITION_CLASS.getPackage.getName)
  
  private val ICOMPOSITION_SOURCE_CLASS = classOf[IComposition#ICompositionSource]
  val         ICOMPOSITION_SOURCE_STYPE = SType(ICOMPOSITION_SOURCE_CLASS.getSimpleName, ICOMPOSITION_SOURCE_CLASS.getPackage.getName)
  
  private val ICOMPOSITION_TARGET_CLASS = classOf[IComposition#ICompositionTarget]
  val         ICOMPOSITION_TARGET_STYPE = SType(ICOMPOSITION_TARGET_CLASS.getSimpleName, ICOMPOSITION_TARGET_CLASS.getPackage.getName)
  
  private val IDIRECT_AGGREGATION_CLASS = classOf[IDirectAggregation]
  val         IDIRECT_AGGREGATION_STYPE = SType(IDIRECT_AGGREGATION_CLASS.getSimpleName, IDIRECT_AGGREGATION_CLASS.getPackage.getName)
  
  private val IDIRECT_AGGREGATION_SOURCE_CLASS = classOf[IDirectAggregation#IDirectAggregationSource]
  val         IDIRECT_AGGREGATION_SOURCE_STYPE = SType(IDIRECT_AGGREGATION_SOURCE_CLASS.getSimpleName, IDIRECT_AGGREGATION_SOURCE_CLASS.getPackage.getName)
  
  private val IDIRECT_AGGREGATION_TARGET_CLASS = classOf[IDirectAggregation#IDirectAggregationTarget]
  val         IDIRECT_AGGREGATION_TARGET_STYPE = SType(IDIRECT_AGGREGATION_TARGET_CLASS.getSimpleName, IDIRECT_AGGREGATION_TARGET_CLASS.getPackage.getName)
  
  private val IDIRECT_ASSOCIATION_CLASS = classOf[IDirectAssoziation]
  val         IDIRECT_ASSOCIATION_STYPE = SType(IDIRECT_ASSOCIATION_CLASS.getSimpleName, IDIRECT_ASSOCIATION_CLASS.getPackage.getName)
  
  private val IDIRECT_ASSOCIATION_SOURCE_CLASS = classOf[IDirectAssoziation#IDirectAssoziationSource]
  val         IDIRECT_ASSOCIATION_SOURCE_STYPE = SType(IDIRECT_ASSOCIATION_SOURCE_CLASS.getSimpleName, IDIRECT_ASSOCIATION_SOURCE_CLASS.getPackage.getName)
  
  private val IDIRECT_ASSOCIATION_TARGET_CLASS = classOf[IDirectAssoziation#IDirectAssoziationTarget]
  val         IDIRECT_ASSOCIATION_TARGET_STYPE = SType(IDIRECT_ASSOCIATION_TARGET_CLASS.getSimpleName, IDIRECT_ASSOCIATION_TARGET_CLASS.getPackage.getName)
  
  private val IDIRECT_COMPOSITION_CLASS = classOf[IDirectComposition]
  val         IDIRECT_COMPOSITION_STYPE = SType(IDIRECT_COMPOSITION_CLASS.getSimpleName, IDIRECT_COMPOSITION_CLASS.getPackage.getName)
  
  private val IDIRECT_COMPOSITION_SOURCE_CLASS = classOf[IDirectComposition#IDirectCompositionSource]
  val         IDIRECT_COMPOSITION_SOURCE_STYPE = SType(IDIRECT_COMPOSITION_SOURCE_CLASS.getSimpleName, IDIRECT_COMPOSITION_SOURCE_CLASS.getPackage.getName)
  
  private val IDIRECT_COMPOSITION_TARGET_CLASS = classOf[IDirectComposition#IDirectCompositionTarget]
  val         IDIRECT_COMPOSITION_TARGET_STYPE = SType(IDIRECT_COMPOSITION_TARGET_CLASS.getSimpleName, IDIRECT_COMPOSITION_TARGET_CLASS.getPackage.getName)
}