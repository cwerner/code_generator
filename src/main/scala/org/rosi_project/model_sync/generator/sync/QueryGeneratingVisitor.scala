package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.acr_model.types._
import org.rosi_project.model_sync.generator.PackageNames

class QueryGeneratingVisitor extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {
    
    val viewCompartment = new SClass(sModel.getName.capitalize + PackageNames.queryPostName, PackageNames.queryPkgName)
    var newInternalRoles: Seq[SInnerViewNaturalClass] = Seq.empty
    var newInternalRelationalRoles: Seq[SInnerViewRelationalClass] = Seq.empty
    viewCompartment.addParent(PredefRsumTypes.IQUERY_VIEW_COMPARTMENT_STYPE)
    
    var allTypes: Set[STypedElement] = sModel.getModelClasses ++ sModel.getRelationalCompartments ++ PredefRsumTypes.CHECKING_OPTION_STYPE ++ sModel.getModelEnums
    sModel.getModelClasses.foreach(_.getStructuralFeatures.foreach(s => {
      if (s.getTypeElement.sPackage != "")
        allTypes += s.getTypeElement
    }))    
        
    viewCompartment.augmentConstructor(SMethodStatement(s"""init("${viewCompartment.getName}")""", allTypes))     
    
    //Iterate over all Model classes and create natural internal role classes
    sModel.getModelClasses.filter(!_.getName.startsWith(PackageNames.queryHelperPrefix)).foreach(cls => {
      val internalClass = new SInnerViewNaturalClass(cls.getName + PackageNames.queryRolePostName,
          _externalClass = viewCompartment, 
          sumSource = cls)
      //sets the attributes from the source class
      var newAtts: Set[SAttribute] = Set.empty
      cls.getAttributes.foreach(attr => {
        var newAtt = SAttribute(attr.getName, attr.getTypeElement, attr.unique)
        newAtt.setVisibility(MethodVisibility.protectedVis)
        newAtt.isFinal = true
        newAtts += newAtt
      })
      internalClass.setAttributes(newAtts.toSeq)  
      newInternalRoles = newInternalRoles :+ internalClass
      viewCompartment.addInternalClass(internalClass)
    })
    
    //TODO: can be necessary to add all parents in one loop first
    //Now fill all natural internal roles with functionality
    newInternalRoles.foreach(inner => {
      val cls = inner.sumSource
      //set inheritance to abstract view role
      if (cls.isRootClass) {
        inner.addParent(PredefRsumTypes.AQUERY_VIEW_ROLE_STYPE)
      }
      //add initialize method if necessary
      inner.addMethod(ViewMethods.getIsRelationalMethod(false))
      inner.addMethod(QueryMethods.getInitializeMethod(inner))
      //add delete method
      inner.addMethod(ViewMethods.getDeleteElementNaturalMethod(inner))
      //add parents
      cls.getAllParents().foreach(p => {
        val existing: Option[STypedElement] = newInternalRoles.find(existing => existing.getName == p.getName + PackageNames.queryRolePostName)
        if (!existing.isEmpty) {
          inner.addParent(existing.get)
        }
      })      
      //add getter and setter for attributes in the inner class
      inner.getAttributes.foreach(attr => {
        inner.addMethod(ViewMethods.getViewAttributeGetter(attr))
        inner.addMethod(QueryMethods.getViewAttributeSetter(attr))
      })
      //remove all attributes
      inner.setAttributes(Seq.empty)
    })
    
    //Iterate over all Relational compartments and create relational internal role classes
    sModel.getRelationalCompartments.foreach(r => {
      var sourceAtt: SAttribute = null
      var targetAtt: SAttribute = null
      //set as attributes the other classes
      var sourceRole: SInnerViewNaturalClass = null
      var targetRole: SInnerViewNaturalClass = null
      r.getAttributes.foreach(attr => {
        newInternalRoles.foreach(inte => {
          if (inte.getName == attr.getTypeElement.getName + PackageNames.queryRolePostName) {
            if (attr.getName == "sInstance") {
              sourceRole = inte
              sourceAtt = new SAttribute("source", inte)
              sourceAtt.isFinal = true
              sourceAtt.setVisibility(MethodVisibility.privateVis)
            } else {
              targetRole = inte
              targetAtt = new SAttribute("target", inte)
              targetAtt.isFinal = true
              targetAtt.setVisibility(MethodVisibility.privateVis)
            }
          }
        })
      })
      val internalClass = new SInnerViewRelationalClass(r.getName + PackageNames.queryRolePostName, _externalClass = viewCompartment, 
          sumSource = r.asInstanceOf[SRelationalCompartmentClass], viewSource = sourceRole, viewTarget = targetRole)      
      internalClass.setAttributes(sourceAtt ++ targetAtt)
      internalClass.addMethod(ViewMethods.getIsRelationalMethod(true))
      internalClass.addMethod(QueryMethods.getInitializeMethod(internalClass))
      internalClass.addMethod(ViewMethods.getDeleteElementRelationMethod(internalClass, true, true))
      ViewMethods.getSourceAndTargetGetter(internalClass).foreach(internalClass.addMethod(_))
      internalClass.addParent(PredefRsumTypes.AQUERY_VIEW_ROLE_STYPE)
      QueryMethods.createAllReferenceLinks(internalClass)
      newInternalRelationalRoles = newInternalRelationalRoles :+ internalClass
      viewCompartment.addInternalClass(internalClass)
    })
    
    //Create methods to create elements from the view
    QueryMethods.getCreateNaturalRoleMethods(newInternalRoles.toSet).foreach(viewCompartment.addMethod(_))
    
    //add the view to the list for generating views
    sModel.addProviderClass(viewCompartment)
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }
  
  override def visit(sRef: SReference): Unit = {
    // pass
  } 
}