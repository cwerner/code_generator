package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.support.ExtendedString.stringToExtended
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes
import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.acr_model.types.SSet

object QueryMethods  {
  
  /**
   * Special because of Helper classes. 
   */
  def getCreateNaturalRoleMethods(naturalRoles: Set[SInnerViewNaturalClass]): Seq[SMethod] = {
    var methods: Seq[SMethod] = Seq.empty
    naturalRoles.foreach(nr => {
      if (!nr.getName.startsWith("Helper")) {
        val method = new SMethod(
            name = s"create${nr.sumSource.getName}",
            result = nr,
            params = Seq.empty,
            implementation = Seq(SMethodStatement(content = s"return new ${nr.getName}()")))
        methods = methods :+ method
      }
    })
    methods
  }
  
  /**
   * Is also split here now.
   */
  def getInitializeMethod(nat: SInnerViewNaturalClass): SMethod = {
    var helper = ""
    if (nat.sumSource.isAbstract || nat.sumSource.isInterface) helper = "Helper"
    val method = new SMethod(
      name = "getCreationObject",
      result = PredefTypes.Object,
      params = Seq.empty,
      implementation = Seq(SMethodStatement(content = s"return new $helper${nat.sumSource.getName}(" + 
          s"${nat.sumSource.getAllConstructorParameters.map(HelperFunctions.classEmptyConstructorParameterCreation(_)).mkString(", ")})")), 
      true)    
    method.setVisibility(MethodVisibility.protectedVis)
    method
  }
    
  /**
   * Is also split here now.
   */
  def getInitializeMethod(ref: SInnerViewRelationalClass): SMethod = {
    val method = new SMethod(
      name = "getCreationObject",
      result = PredefTypes.Object,
      params = Seq.empty,
      implementation = Seq(SMethodStatement(content = "return")), 
      true)
    var statements: Seq[SMethodStatement] = Seq.empty
    if (ref.sumSource.connectedRef.hasOpposite) {
      statements = Seq(SMethodStatement(content = s"target.set${ref.sumSource.connectedRef.oppositeRef.getName.capitalize}Intern(this)"))
    }
    statements = statements ++ Seq(SMethodStatement(content = s"source.set${ref.sumSource.connectedRef.getName.capitalize}Intern(this)"),
        SMethodStatement(content = s"val sp: ${ref.viewSource.sumSource.getName} = source.player.right.get.asInstanceOf[${ref.viewSource.sumSource.getName}]",
            usedTypes = Set(ref.viewSource.sumSource)),
        SMethodStatement(content = s"val tp: ${ref.viewTarget.sumSource.getName} = target.player.right.get.asInstanceOf[${ref.viewTarget.sumSource.getName}]",
            usedTypes = Set(ref.viewTarget.sumSource)),
        SMethodStatement(content = s"val v: ${ref.sumSource.getName} = new ${ref.sumSource.getName}(sp, tp)",
            usedTypes = Set(ref.sumSource)),
        SMethodStatement(content = "return v",
            usedTypes = Set.empty))    
    method.implementation = statements
    method.setVisibility(MethodVisibility.protectedVis)
    method
  }
  
  /**
   * Special are the attribute filter options.
   */
  def getViewAttributeSetter(struc: SStructuralFeature): SMethod = {
    new SMethod(
      name = s"set${struc.getName.firstLetterToUpperCase}View",
      result = PredefTypes.Unit,
      params = Seq(SMethodParameter(struc.getName.toLowerCase(), struc.getTypeElement), SMethodParameter("check", PredefRsumTypes.CHECKING_OPTION_STYPE)),
      implementation = Seq(SMethodStatement(content = s"+this set${struc.getName.firstLetterToUpperCase}(${struc.getName.toLowerCase()})", usedTypes = Set(struc.getTypeElement)), 
          SMethodStatement(content = s"""connected.addAttributeFilter("${struc.getName}", ${struc.getName.toLowerCase()}.toString(), check)""")))
  }
  
  def createAllReferenceLinks(refClass: SInnerViewRelationalClass): Unit = {
    val targetClass: SInnerViewNaturalClass = refClass.viewTarget
    val sourceClass: SInnerViewNaturalClass = refClass.viewSource
    val realRef: SReference = refClass.sumSource.connectedRef 
    val oppoRef: SReference = refClass.sumSource.connectedRef.oppositeRef
    createReferenceLinks(refClass, realRef, sourceClass, targetClass, true)
    if (oppoRef != null) {
      createReferenceLinks(refClass, oppoRef, targetClass, sourceClass, false)
    }      
  }
  
  private def createReferenceLinks(refClass: SInnerViewRelationalClass, realRef: SReference, sourceClass: SInnerViewNaturalClass, targetClass: SInnerViewNaturalClass, isReal: Boolean): Unit = {
    val getStatement: String = if (isReal) "getTarget" else "getSource"
    val thisStatement: String = if (isReal) "(this, v)" else "(v, this)"  
    if (realRef.upperBound == 1) {
      //add delete role for attributes because query is not connected with RSUM
      sourceClass.getMethods.foreach(m => {
        if (m.getName == "deleteElement") {
          m.implementation = m.implementation :+ SMethodStatement(s"if (${realRef.getName} != null) ${realRef.getName}.deleteElement()")
        }
      })      
      
      //upper Bound = 1
      //variable settings
      sourceClass.augmentConstructor(SMethodStatement(s"private var ${realRef.getName}: ${refClass.getDeepName} = null"))
      //internal variables
      val removeMethodInternal = new SMethod(
        name = s"remove${realRef.getName.capitalize}Intern",
        result = PredefTypes.Unit,
        params = Seq(SMethodParameter("v", refClass)), //TODO: remove this then each remove internal method has different input parameters
        implementation = Seq(SMethodStatement(content = s"${realRef.getName} = null")))
      removeMethodInternal.setVisibility(MethodVisibility.privateExternalClass)
      sourceClass.addMethod(removeMethodInternal)
      val setMethodInternal = new SMethod(
        name = s"set${realRef.getName.capitalize}Intern",
        result = PredefTypes.Unit,
        params = Seq(SMethodParameter("v", refClass)),
        implementation = Seq(SMethodStatement(content = s"${realRef.getName} = v")))
      setMethodInternal.setVisibility(MethodVisibility.privateExternalClass)
      sourceClass.addMethod(setMethodInternal)
      //external variables
      sourceClass.addMethod(new SMethod(
        name = s"get${realRef.getName.capitalize}",
        result = targetClass,
        params = Seq.empty,
        implementation = Seq(SMethodStatement(content = s"return ${realRef.getName}.${getStatement}()"))))
      sourceClass.addMethod(new SMethod(
        name = s"set${realRef.getName.capitalize}",
        result = PredefTypes.Boolean,
        params = Seq(SMethodParameter("v", targetClass)),
        implementation = Seq(SMethodStatement(content = "if (v == null) return false"),
            SMethodStatement(content = s"if (!containsRole(v.asInstanceOf[${PredefRsumTypes.AQUERY_VIEW_ROLE_STYPE.getName}])) return false"),
            SMethodStatement(content = s"if (${realRef.getName} != null) {"),
            SMethodStatement(content = s"if (${realRef.getName}.${getStatement}() == v) return false"),
            SMethodStatement(content = s"${realRef.getName}.deleteElement()"),
            SMethodStatement(content = "}"),
            SMethodStatement(content = s"new ${refClass.getName}${thisStatement}"),
            SMethodStatement(content = "return true"))))
    } else {
      //add delete role for attributes
      sourceClass.getMethods.foreach(m => {
        if (m.getName == "deleteElement") {
          m.implementation = m.implementation :+ SMethodStatement(s"${realRef.getName}.foreach(_.deleteElement())")
        }
      })     
      
      //upper Bound = endless
      //variable settings
      sourceClass.augmentConstructor(SMethodStatement(s"private var ${realRef.getName}: Set[${refClass.getDeepName}] = Set.empty"))
      //internal variables
      val removeMethodInternal = new SMethod(
        name = s"remove${realRef.getName.capitalize}Intern",
        result = PredefTypes.Unit,
        params = Seq(SMethodParameter("v", refClass)),
        implementation = Seq(SMethodStatement(content = s"${realRef.getName} -= v")))
      removeMethodInternal.setVisibility(MethodVisibility.privateExternalClass)
      sourceClass.addMethod(removeMethodInternal)
      val setMethodInternal = new SMethod(
        name = s"set${realRef.getName.capitalize}Intern",
        result = PredefTypes.Unit,
        params = Seq(SMethodParameter("v", refClass)),
        implementation = Seq(SMethodStatement(content = s"${realRef.getName} += v")))
      setMethodInternal.setVisibility(MethodVisibility.privateExternalClass)
      sourceClass.addMethod(setMethodInternal)
      //external variables
      sourceClass.addMethod(new SMethod(
        name = s"get${realRef.getName.capitalize}",
        result = SSet(targetClass),
        params = Seq.empty,
        implementation = Seq(SMethodStatement(content = s"var vs: Set[${targetClass.getDeepName}] = Set.empty"), 
            SMethodStatement(content = s"${realRef.getName}.foreach{ v => vs += v.${getStatement}()}"), 
            SMethodStatement(content = "return vs"))))
      sourceClass.addMethod(new SMethod(
        name = s"has${realRef.getName.capitalize}",
        result = PredefTypes.Boolean,
        params = Seq(SMethodParameter("v", targetClass)),
        implementation = Seq(SMethodStatement(content = s"return get${realRef.getName.capitalize}.contains(v)"))))
      sourceClass.addMethod(new SMethod(
        name = s"add${realRef.getName.capitalize}",
        result = PredefTypes.Boolean,
        params = Seq(SMethodParameter("v", targetClass)),
        implementation = Seq(SMethodStatement(content = s"if (has${realRef.getName.capitalize}(v) || !containsRole(v.asInstanceOf[${PredefRsumTypes.AQUERY_VIEW_ROLE_STYPE.getName}])) return false"),
            SMethodStatement(content = s"new ${refClass.getName}${thisStatement}"),
            SMethodStatement(content = "return true"))))
      sourceClass.addMethod(new SMethod(
        name = s"remove${realRef.getName.capitalize}",
        result = PredefTypes.Boolean,
        params = Seq(SMethodParameter("v", targetClass)),
        implementation = Seq(SMethodStatement(content = s"if (!has${realRef.getName.capitalize}(v)) return false"),
            SMethodStatement(content = s"${realRef.getName}.foreach{ h =>"),
            SMethodStatement(content = s"if (h.${getStatement}() == v) {"),
            SMethodStatement(content = "h.deleteElement()"),
            SMethodStatement(content = "return true"),
            SMethodStatement(content = "}}"),
            SMethodStatement(content = "return true"))))
    }
  }
}