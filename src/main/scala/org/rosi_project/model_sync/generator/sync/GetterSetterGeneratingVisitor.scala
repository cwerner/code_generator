package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.acr_model.types.GenericSequence

/**
 * Service to extend [[SClass SClasses]] with getter and setter methods for all attributes.
 *
 * @author Rico Bergmann
 */
class GetterSetterGeneratingVisitor extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {
    sModel.getModelClasses.foreach(cls => {
      //add all getter and setter for all structural features
      cls.getStructuralFeatures.foreach(attr => {
        attr.setVisibility(MethodVisibility.protectedVis)
        val getter = new SGetter(attr)
        cls.addMethod(getter)
        if (attr.getTypeElement.isInstanceOf[GenericSequence]) {
          val adder = new SSetterAdd(attr, attr.getTypeElement.asInstanceOf[GenericSequence].typeParam)
          cls.addMethod(adder)
          val remover = new SSetterRemove(attr, attr.getTypeElement.asInstanceOf[GenericSequence].typeParam)
          cls.addMethod(remover)
        } else {
          val setter = new SSetter(attr)
          cls.addMethod(setter)
        }
      })
    })
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sRef: SReference): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }

}
