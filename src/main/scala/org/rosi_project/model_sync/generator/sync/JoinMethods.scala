package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.model_join.representation.grammar.JoinExpression.JoinType
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes
import org.rosi_project.model_sync.generator.support.ExtendedString.stringToExtended

object JoinMethods {

  //JOIN OBJECT METHODS
  def getJoinTypeMethod(joinType: JoinType): SMethod = {
    val met = new SMethod(
      name = "getJoinType",
      result = PredefRsumTypes.RSUM_JOIN_TYPE_STYPE,
      params = Seq.empty,
      implementation = Seq())
    if (joinType == JoinType.NATURAL) {
      met.implementation = Seq(SMethodStatement(content = s"RsumJoinType.natural", usedTypes = Set()))
    }
    if (joinType == JoinType.OUTER) {
      met.implementation = Seq(SMethodStatement(content = s"RsumJoinType.outer", usedTypes = Set()))
    }
    if (joinType == JoinType.THETA) {
      met.implementation = Seq(SMethodStatement(content = s"RsumJoinType.theta", usedTypes = Set()))
    }
    met
  }

  def getInstanceBaseModelMethod(cls: SClass): SMethod = {
    new SMethod(
      name = "isInstanceBaseModel",
      result = PredefTypes.Boolean,
      params = Seq(SMethodParameter("obj", PredefTypes.Object)),
      implementation = Seq(SMethodStatement(content = s"obj.isInstanceOf[${cls.getName}]", usedTypes = Set(cls))))
  }

  def getInstanceOtherModelMethod(cls: SClass): SMethod = {
    new SMethod(
      name = "isInstanceOtherModel",
      result = PredefTypes.Boolean,
      params = Seq(SMethodParameter("obj", PredefTypes.Object)),
      implementation = Seq(SMethodStatement(content = s"obj.isInstanceOf[${cls.getName}]", usedTypes = Set(cls))))
  }

  def getInstanceOfMethod(cls: SClass): SMethod = {
    new SMethod(
      name = "isInstanceOf",
      result = PredefTypes.Boolean,
      params = Seq(SMethodParameter("obj", PredefTypes.Object)),
      implementation = Seq(SMethodStatement(content = s"obj.isInstanceOf[${cls.getName}]", usedTypes = Set(cls))))
  }

  def getNewInstanceMethod(cls: SClass): SMethod = {
    new SMethod(
      name = "getNewInstance",
      result = PredefRsumTypes.IJOIN_COMPARTMENT_STYPE,
      params = Seq(SMethodParameter("b", PredefTypes.Object), SMethodParameter("o", PredefTypes.Object)),
      implementation = Seq(SMethodStatement(content = s"val j = new ${cls.getName}(null, null)", usedTypes = Set(cls)),
        SMethodStatement(content = "objectInitialize(j, b, o)", usedTypes = Set()),
        SMethodStatement(content = "j", usedTypes = Set())))
  }

  def getMatchMethod(base: SClass, other: SClass, joinAtts: Set[SStructuralFeature], joinString: String): SMethod = {
    val met = new SMethod(
      name = "matchTwoObjects",
      result = PredefTypes.Boolean,
      params = Seq(SMethodParameter("b", PredefTypes.Object), SMethodParameter("o", PredefTypes.Object)),
      implementation = Seq())
    met.implementation = Seq(SMethodStatement(content = s"val base = b.asInstanceOf[${base.getName}]", usedTypes = Set(base)),
      SMethodStatement(content = s"val other = o.asInstanceOf[${other.getName}]", usedTypes = Set(other)))
    if (joinString == null) {
      met.implementation = met.implementation :+ SMethodStatement(content = s"${joinAtts.map(HelperFunctions.attributeEqualCreation(_)).mkString(" && ")}")
    } else {
      met.implementation = met.implementation :+ SMethodStatement(content = joinString)
    }
    met
  }

  //JOIN CLASS METHODS
  def getJoinInfoMethod(obj: SClass): SMethod = {
    new SMethod(
      name = "getJoinInfo",
      result = PredefRsumTypes.JOIN_INFO_STYPE,
      params = Seq.empty,
      implementation = Seq(SMethodStatement(content = s"${obj.getName}", usedTypes = Set(obj))))
  }

  //GETTER
  def getBaseAttributeGetter(struc: SStructuralFeature): SMethod = {
    new SMethod(
      name = s"get${struc.getName.firstLetterToUpperCase}",
      result = struc.getTypeElement,
      params = Seq.empty,
      implementation = Seq(SMethodStatement(content = s"+baseRole get${struc.getName.firstLetterToUpperCase}()", usedTypes = Set(struc.getTypeElement))))
  }

  def getOtherAttributeGetter(struc: SStructuralFeature): SMethod = {
    new SMethod(
      name = s"get${struc.getName.firstLetterToUpperCase}",
      result = struc.getTypeElement,
      params = Seq.empty,
      implementation = Seq(SMethodStatement(content = "if (otherObj != null) {", usedTypes = Set()),
        SMethodStatement(content = s"return +otherRole get${struc.getName.firstLetterToUpperCase}()", usedTypes = Set(struc.getTypeElement)),
        SMethodStatement(content = "}", usedTypes = Set()),
        SMethodStatement(content = s"return ${HelperFunctions.classEmptyType(struc.getTypeElement.getName)}", usedTypes = Set())))
  }

  //SETTER  
  def getBaseAttributeSetter(struc: SStructuralFeature): SMethod = {
    new SMethod(
      name = s"set${struc.getName.firstLetterToUpperCase}",
      result = PredefTypes.Unit,
      params = Seq(SMethodParameter(struc.getName.toLowerCase(), struc.getTypeElement)),
      implementation = Seq(SMethodStatement(content = s"+baseRole set${struc.getName.firstLetterToUpperCase}(${struc.getName.toLowerCase()})", usedTypes = Set(struc.getTypeElement))))
  }

  def getMixedAttributeSetter(struc: SStructuralFeature): SMethod = {
    new SMethod(
      name = s"set${struc.getName.firstLetterToUpperCase}View",
      result = PredefTypes.Unit,
      params = Seq(SMethodParameter(struc.getName.toLowerCase(), struc.getTypeElement)),
      implementation = Seq(SMethodStatement(content = s"+baseRole set${struc.getName.firstLetterToUpperCase}(${struc.getName.toLowerCase()})", usedTypes = Set(struc.getTypeElement)),
        SMethodStatement(content = "if (otherObj != null) {", usedTypes = Set()),
        SMethodStatement(content = s"+otherRole set${struc.getName.firstLetterToUpperCase}(${struc.getName.toLowerCase()})", usedTypes = Set(struc.getTypeElement)),
        SMethodStatement(content = "}", usedTypes = Set.empty)))
  }

  def getOtherAttributeSetter(struc: SStructuralFeature): SMethod = {
    new SMethod(
      name = s"set${struc.getName.firstLetterToUpperCase}View",
      result = PredefTypes.Unit,
      params = Seq(SMethodParameter(struc.getName.toLowerCase(), struc.getTypeElement)),
      implementation = Seq(SMethodStatement(content = "if (otherObj != null) {", usedTypes = Set()),
        SMethodStatement(content = s"+otherRole set${struc.getName.firstLetterToUpperCase}(${struc.getName.toLowerCase()})", usedTypes = Set(struc.getTypeElement)),
        SMethodStatement(content = "}", usedTypes = Set.empty)))
  }
}