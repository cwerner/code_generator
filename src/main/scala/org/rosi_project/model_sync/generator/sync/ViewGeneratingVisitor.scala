package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.acr_model.types._
import org.rosi_project.model_sync.generator.PackageNames

class ViewGeneratingVisitor extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {
    
    val viewCompartment = new SViewClass(sModel.getName.capitalize + PackageNames.viewPostName, PackageNames.viewPkgName)
    var newInternalRoles: Seq[SInnerViewNaturalClass] = Seq.empty
    var newInternalRelationalRoles: Seq[SInnerViewRelationalClass] = Seq.empty
    viewCompartment.addParent(PredefRsumTypes.IVIEW_COMPARTMENT_STYPE)
    
    //Iterate over all Model classes and create natural internal role classes
    sModel.getModelClasses.foreach(cls => {
      val internalClass = new SInnerViewNaturalClass(cls.getName + PackageNames.viewRolePostName,
          _sClassType = cls.sClassType,
          _externalClass = viewCompartment, 
          sumSource = cls)
      //sets the attributes from the source class
      var newAtts: Set[SAttribute] = Set.empty
      cls.getAttributes.foreach(attr => {
        var newAtt = SAttribute(attr.getName, attr.getTypeElement, attr.unique)
        newAtt.setVisibility(MethodVisibility.protectedVis)
        newAtt.isFinal = true
        newAtts += newAtt
      })
      internalClass.setAttributes(newAtts.toSeq)  
      newInternalRoles = newInternalRoles :+ internalClass
      viewCompartment.addInternalClass(internalClass)
    })
    
    //Now add the parents to all natural internal roles first
    newInternalRoles.foreach(inner => {
      val cls = inner.sumSource
      //set inheritance to abstract view role
      if (cls.isRootClass) {
        inner.addParent(PredefRsumTypes.AVIEW_ROLE_STYPE)
      }
      //add parents
      cls.getAllParents().foreach(p => {
        val existing: Option[STypedElement] = newInternalRoles.find(existing => existing.getName == p.getName + PackageNames.viewRolePostName)
        if (!existing.isEmpty) {
          inner.addParent(existing.get)
        }
      })
    })
    
    //add now the messages because they can depend on the parents
    newInternalRoles.foreach(inner => {
      //add initialize method if necessary
      if (!inner.isAbstract && !inner.isInterface) {
        inner.addMethod(ViewMethods.getIsRelationalMethod(false))
        inner.addMethod(ViewMethods.getCreationObjectMethod(inner))
      }    
      //add delete method
      inner.addMethod(ToStringMethods.onlyStringToStringMethod("VNR: " + inner.getName))
      inner.addMethod(ViewMethods.getDeleteElementNaturalMethod(inner))
      //add getter and setter for attributes in the inner class
      inner.getAttributes.foreach(attr => {
        inner.addMethod(ViewMethods.getViewAttributeGetter(attr))
        inner.addMethod(ViewMethods.getViewAttributeSetter(attr))
      })      
    })
    
    //Iterate over all Relational compartments and create relational internal role classes
    sModel.getRelationalCompartments.foreach(r => {
      var sourceAtt: SAttribute = null
      var targetAtt: SAttribute = null
      //set as attributes the other classes
      var sourceRole: SInnerViewNaturalClass = null
      var targetRole: SInnerViewNaturalClass = null
      r.getAttributes.foreach(attr => {
        newInternalRoles.foreach(inte => {
          if (inte.getName == attr.getTypeElement.getName + PackageNames.viewRolePostName) {
            if (attr.getName == "sInstance") {
              sourceRole = inte
              sourceAtt = new SAttribute("source", inte)
              sourceAtt.isFinal = true
              sourceAtt.setVisibility(MethodVisibility.privateVis)
            } else {
              targetRole = inte
              targetAtt = new SAttribute("target", inte)
              targetAtt.isFinal = true
              targetAtt.setVisibility(MethodVisibility.privateVis)
            }
          }
        })
      })
      val internalClass = new SInnerViewRelationalClass(r.getName + PackageNames.viewRolePostName, _externalClass = viewCompartment, 
          sumSource = r.asInstanceOf[SRelationalCompartmentClass], viewSource = sourceRole, viewTarget = targetRole)      
      internalClass.setAttributes(sourceAtt ++ targetAtt)
      internalClass.addMethod(ViewMethods.getIsRelationalMethod(true))
      internalClass.addMethod(ViewMethods.getCreationObjectMethod(internalClass, true, true))      
      internalClass.addMethod(ViewMethods.getDeleteElementRelationMethod(internalClass, true, true))
      internalClass.addMethod(ToStringMethods.onlyStringToStringMethod("VRR: " + internalClass.getName))
      ViewMethods.getSourceAndTargetGetter(internalClass).foreach(internalClass.addMethod(_))
      internalClass.addParent(PredefRsumTypes.AVIEW_ROLE_STYPE)
      ViewMethods.createAllReferenceLinks(internalClass, true, true)
      newInternalRelationalRoles = newInternalRelationalRoles :+ internalClass
      viewCompartment.addInternalClass(internalClass)
    })
    
    //add methods that must be override from the view trait
    viewCompartment.addMethod(ViewMethods.getIsViewableMethod((sModel.getRelationalCompartments ++ sModel.getModelClasses).toSet))
    viewCompartment.addMethod(ViewMethods.getViewNameMethod(viewCompartment))
    viewCompartment.addMethod(ViewMethods.getGetNaturalRoleMethod(newInternalRoles.toSet))
    viewCompartment.addMethod(ViewMethods.getGetRelationalRoleMethod(newInternalRelationalRoles.toSet))
    viewCompartment.addMethod(ToStringMethods.onlyStringToStringMethod("VC: " + viewCompartment.getName))
    //Create methods to create elements from the view
    ViewMethods.getCreateNaturalRoleMethods(newInternalRoles.toSet).foreach(viewCompartment.addMethod(_))
    
    //add the view to the list for generating views
    sModel.addViewCompartment(viewCompartment)
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }
  
  override def visit(sRef: SReference): Unit = {
    // pass
  }
}