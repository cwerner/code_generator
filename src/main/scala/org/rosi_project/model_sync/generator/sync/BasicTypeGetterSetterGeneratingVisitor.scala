package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._

class BasicTypeGetterSetterGeneratingVisitor extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {
    sModel.getModelClasses.foreach(cls => {
      //add all getter and setter for each normal attribute
      cls.getAttributes.foreach(attr => {
        attr.setVisibility(MethodVisibility.protectedVis)
        val getter = new SGetter(attr)
        val setter = new SSetter(attr)
        cls.addMethod(getter)
        cls.addMethod(setter)
      })
    })
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sRef: SReference): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }
}