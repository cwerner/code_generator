package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.model_join.representation.grammar.ModelJoinExpression
import org.rosi_project.model_management.sum.join.RsumJoinType
import org.rosi_project.model_sync.model_join.representation.grammar.JoinExpression.JoinType
import org.rosi_project.model_sync.model_join.representation.grammar.ThetaJoinExpression

class JoinGeneratingVisitor(joinExpression: ModelJoinExpression) extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {

    joinExpression.getJoins.forEach(j => {
      println("Type: " + j.getType + " Base: " + j.getBaseModel.getResourcePath + " " + j.getBaseModel.getResourceName + " Other: " + j.getOtherModel + " Target: " + j.getTarget)
      println(j)
      j.getKeeps.forEach(k => {
        println(k)
      })
    })

    joinExpression.getJoins.forEach(j => {
      val baseType: Option[STypedElement] = STypeRegistry.query(j.getBaseModel().getResourceName, j.getBaseModel().getResourcePath)
      val otherType: Option[STypedElement] = STypeRegistry.query(j.getOtherModel().getResourceName, j.getOtherModel().getResourcePath)
      var targetType: Option[STypedElement] = STypeRegistry.query(j.getTarget().getResourceName, j.getTarget().getResourcePath)
      
      println("B: " + baseType + " O: " + otherType + " T: " + targetType + " Same: " + j.isSameElement)
      
      if (j.isSameElement || baseType.isEmpty || otherType.isEmpty || !targetType.isEmpty) {
        return
      }

      val baseClass = baseType.get.asInstanceOf[SClass]
      val otherClass = otherType.get.asInstanceOf[SClass]
      
      var rsumJoinType = RsumJoinType.natural
      var compareString: String = null 
      
      j.getType match {
        case JoinType.NATURAL => rsumJoinType = RsumJoinType.natural
        case JoinType.OUTER => rsumJoinType = RsumJoinType.outer
        case JoinType.THETA => 
          rsumJoinType = RsumJoinType.theta
          compareString = j.asInstanceOf[ThetaJoinExpression].getCondition.get
      }
      
      var otherAttsBase: Set[SStructuralFeature] = Set.empty
      var otherAttsOther: Set[SStructuralFeature] = Set.empty
      var joinAtts: Set[SStructuralFeature] = Set.empty

      baseClass.getDeepStructuralFeatures.foreach(ba => {
        var foundOne = false
        otherClass.getDeepStructuralFeatures.foreach(oa => {
          if (oa.getName == ba.getName && oa.getTypeElement.getName == ba.getTypeElement.getName) {
            joinAtts += ba
            foundOne = true
          }
        })
        if (!foundOne) {
          otherAttsBase += ba
        }
      })

      otherClass.getDeepStructuralFeatures.foreach(oa => {
        var foundOne = false
        joinAtts.foreach(ba => {
          if (oa.getName == ba.getName && oa.getTypeElement.getName == ba.getTypeElement.getName) {
            foundOne = true
          }
        })
        if (!foundOne) {
          otherAttsOther += oa
        }
      })
      
      println("J: " + joinAtts + " T: " + rsumJoinType)
      println("B: " + otherAttsBase)
      println("O: " + otherAttsOther)
      
      val joinObject = new SClass(j.getTarget().getResourceName + "Object", j.getTarget().getResourcePath, SClassType.normalObject)
      val joinClass = new SJoinClass(j.getTarget().getResourceName, j.getTarget().getResourcePath, 
          base = baseClass, other = otherClass, joinType = rsumJoinType, 
          joinAttributes = joinAtts, innerAttributes = joinAtts ++ otherAttsBase ++ otherAttsOther, joinObject)

      //add parents
      joinClass.addParent(PredefRsumTypes.IJOIN_COMPARTMENT_STYPE)
      joinObject.addParent(PredefRsumTypes.JOIN_INFO_STYPE)     

      //add functions to the join object
      joinObject.addMethod(JoinMethods.getJoinTypeMethod(j.getType))
      joinObject.addMethod(JoinMethods.getInstanceBaseModelMethod(baseClass))
      joinObject.addMethod(JoinMethods.getInstanceOtherModelMethod(otherClass))
      joinObject.addMethod(JoinMethods.getNewInstanceMethod(joinClass))
      joinObject.addMethod(JoinMethods.getInstanceOfMethod(joinClass))
      joinObject.addMethod(JoinMethods.getMatchMethod(baseClass, otherClass, joinAtts, compareString))
      joinObject.addMethod(ToStringMethods.onlyStringToStringMethod(joinObject.getName))
      
      //add functions to the join class
      val attributesJoinClass = Seq(new SAttribute("base", baseClass), new SAttribute("other", otherClass))
      attributesJoinClass.foreach(a => {
        a.setVisibility(MethodVisibility.privateVis)
        a.isFinal = true
      })

      joinClass.setAttributes(attributesJoinClass)
      //add base functions
      joinClass.augmentConstructor(new SMethodStatement("initialize(base, other)", Set()))
      joinClass.addMethod(JoinMethods.getJoinInfoMethod(joinObject))
      //add getter and setter functions
      joinAtts.foreach(a => {
        joinClass.addMethod(JoinMethods.getBaseAttributeGetter(a))
        joinClass.addMethod(JoinMethods.getMixedAttributeSetter(a))
      })
      otherAttsBase.foreach(a => {
        joinClass.addMethod(JoinMethods.getBaseAttributeGetter(a))
        joinClass.addMethod(JoinMethods.getBaseAttributeSetter(a))
      })
      otherAttsOther.foreach(a => {
        joinClass.addMethod(JoinMethods.getOtherAttributeGetter(a))
        joinClass.addMethod(JoinMethods.getOtherAttributeSetter(a))
      })
      //add to String method
      joinClass.addMethod(ToStringMethods.joinToStringMethod(joinClass.getName))

      sModel.addJoinClass(joinClass)
      sModel.addJoinClass(joinObject)
      
      //add in Type Registry
      STypeRegistry.addType(joinClass, null)
    })
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }

  override def visit(sRef: SReference): Unit = {
    // pass
  }

}