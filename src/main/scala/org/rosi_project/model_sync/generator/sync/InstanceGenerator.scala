package org.rosi_project.model_sync.generator.sync

import scala.collection.JavaConverters._
import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.util.ClassAndInstance
import org.eclipse.emf.ecore.EObject
import org.rosi_project.model_sync.generator.util.InstanceLine
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes
import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.emf.common.util.EList
import org.rosi_project.model_sync.util.EMFUtilForGenerator
import java.util.List
import org.rosi_project.model_sync.generator.PackageNames

/**
 * Service to create an instance creation object.
 *
 * @author Christopher Werner
 */
class InstanceGenerator(val clsins: ClassAndInstance) extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {
    //println("++++++++++++++++++++++++++++++++++++++++++++++")
    val example = new SClass("ExampleCase", PackageNames.examplePkgName, SClassType.normalObject)
    val contents = clsins.obj.eAllContents().asScala
    var counter = 0
    var lines: Seq[InstanceLine] = Seq.empty
    var sType = STypeRegistry.getFromClass(clsins.obj.eClass())
    if (sType != null) {
      lines = lines :+ new InstanceLine(counter, clsins.obj, sType)
      counter += 1
    }
    contents.foreach(o => {
      sType = STypeRegistry.getFromClass(o.eClass())
      if (sType != null) {
        lines = lines :+ new InstanceLine(counter, o, sType)
        counter += 1
      }
    })
    //construction of instances
    lines.foreach(l => {
      example.augmentConstructor(new SMethodStatement(l.getLine(), Set(l.usedType)))
    })
    //add values for instances
    lines.foreach(l => {
      l.usedType.getAllConstructorParameters.foreach(cp => {
        if (!STypeRegistry.isDefaultType(cp.getType.getName)) {
          var structs: Seq[EStructuralFeature] = l.obj.eClass().getEAllStructuralFeatures.asScala
          structs.foreach(att => {
            if (att.getName == cp.getName) {
              val obj = l.obj.eGet(att)
              //println("### " + att.getName)
              if (obj.isInstanceOf[EObject]) {
                lines.foreach(lnow => {
                  if (lnow.obj == obj.asInstanceOf[EObject]) {
                    //println("Found: " + lnow.counter)
                    var s = s"${l.getName()}.set${att.getName.capitalize}(${lnow.getName()})"
                    example.augmentConstructor(new SMethodStatement(s))
                  }
                })
              } else {
                val listi: List[EObject] = EMFUtilForGenerator.getList(obj)
                val liste = listi.asScala
                val s = s"${l.getName()}.set${att.getName.capitalize}(Set(${liste.map(setValues(_, lines)).mkString(", ")}))"
                example.augmentConstructor(new SMethodStatement(s))
              }
            }
          })
        }
      })
    })
    example.addParent(PredefTypes.App)
    sModel.addProviderClass(example)
    //println("++++++++++++++++++++++++++++++++++++++++++++++")
    // pass

  }

  def setValues(eo: EObject, lines: Seq[InstanceLine]): String = {
    lines.foreach(lnow => {
      if (lnow.obj == eo) {
        return lnow.getName()
      }
    })
    ""
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sRef: SReference): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }

}