package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.support.ExtendedString.stringToExtended
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes
import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.acr_model.types.SSet

object ViewMethods {
  
  def getDeleteElementRelationMethod(ref: SInnerViewRelationalClass, createNormal: Boolean, createOpposite: Boolean): SMethod = {
    val method = new SMethod(
      name = "deleteElement",
      result = PredefTypes.Unit,
      params = Seq.empty,
      implementation = Seq.empty, 
      true)
    var impl: Seq[SMethodStatement] = Seq.empty
    if (createNormal) {
      impl = impl :+ SMethodStatement(content = s"source.remove${ref.sumSource.connectedRef.getName.capitalize}Intern(this)")
    }
    if (createOpposite && ref.sumSource.connectedRef.hasOpposite) {
      impl = impl :+ SMethodStatement(content = s"target.remove${ref.sumSource.connectedRef.oppositeRef.getName.capitalize}Intern(this)")
    }
    impl = impl :+ SMethodStatement(content = "super.deleteElement()")
    method.implementation = impl
    method
  }
  
  def getDeleteElementNaturalMethod(ref: SInnerViewNaturalClass): SMethod = {
    val method = new SMethod(
      name = "deleteElement",
      result = PredefTypes.Unit,
      params = Seq.empty,
      implementation = Seq(SMethodStatement(content = "super.deleteElement()")), 
      true)
    method
  }
    
  def getSourceAndTargetGetter(ref: SInnerViewRelationalClass): Seq[SMethod] = {
    (new SMethod(
      name = "getSource",
      result = ref.viewSource,
      params = Seq.empty,
      implementation = Seq(SMethodStatement("source"))) ++
    new SMethod(
      name = "getTarget",
      result = ref.viewTarget,
      params = Seq.empty,
      implementation = Seq(SMethodStatement("target"))))
  }
  
  def getCreateNaturalRoleMethods(naturalRoles: Set[SInnerViewNaturalClass]): Seq[SMethod] = {
    var methods: Seq[SMethod] = Seq.empty
    naturalRoles.foreach(nr => {
      if (!nr.isAbstract && !nr.isInterface) {
        val method = new SMethod(
            name = s"create${nr.sumSource.getName}",
            result = nr,
            params = nr.getAllConstructorParameters,
            implementation = Seq(SMethodStatement(content = s"return new ${nr.getName}(${nr.getAllConstructorParameters.map(m => m.getName).mkString(", ")})")))
        methods = methods :+ method
      }
    })
    methods
  }
  
  def getIsRelationalMethod(relational: Boolean): SMethod = {
    val method = new SMethod(
      name = "isRelational",
      result = PredefTypes.Boolean,
      params = Seq.empty,
      implementation = Seq(SMethodStatement(content = s"return ${relational}")), 
      true)    
    method.setVisibility(MethodVisibility.protectedVis)
    method
  }
  
  def getCreationObjectMethod(nat: SInnerViewNaturalClass): SMethod = {
    val method = new SMethod(
      name = "getCreationObject",
      result = PredefTypes.Object,
      params = Seq.empty,
      implementation = Seq.empty,
      true) 
    if (nat.sumSource.isInstanceOf[SJoinClass]) {
      val jcls = nat.sumSource.asInstanceOf[SJoinClass]      
      method.implementation = Seq(SMethodStatement(content = s"return new ${jcls.getName}(new ${jcls.base.getName}(" + 
          s"${jcls.base.getAllConstructorParameters.map(HelperFunctions.initialAttributeDoing(_, nat.getDeepStructuralFeatures)).mkString(", ")}), new ${jcls.other.getName}(" +
          s"${jcls.other.getAllConstructorParameters.map(HelperFunctions.initialAttributeDoing(_, nat.getDeepStructuralFeatures)).mkString(", ")}))", usedTypes = Set(jcls.other, jcls.base)))
    } else {  
      method.implementation = Seq(SMethodStatement(content = s"return new ${nat.sumSource.getName}(" + 
          s"${nat.sumSource.getAllConstructorParameters.map(HelperFunctions.initialAttributeDoing(_, nat.getDeepStructuralFeatures)).mkString(", ")})"))
    }
    method.setVisibility(MethodVisibility.protectedVis)
    method
  }
    
  def getCreationObjectMethod(ref: SInnerViewRelationalClass, createNormal: Boolean, createOpposite: Boolean): SMethod = {
    val method = new SMethod(
      name = "getCreationObject",
      result = PredefTypes.Object,
      params = Seq.empty,
      implementation = Seq(SMethodStatement(content = "return")), 
      true)
    var statements: Seq[SMethodStatement] = Seq.empty
    if (ref.sumSource.connectedRef.hasOpposite && createOpposite) {
      statements = Seq(SMethodStatement(content = s"target.set${ref.sumSource.connectedRef.oppositeRef.getName.capitalize}Intern(this)"))
    }
    if (createNormal) {
      statements = statements ++ SMethodStatement(content = s"source.set${ref.sumSource.connectedRef.getName.capitalize}Intern(this)")
    }
    statements = statements ++ Seq(SMethodStatement(content = s"""val sp: ${ref.sumSource.sClass.getName} = getPlayerOfType(source, "${ref.sumSource.sClass.getName}").asInstanceOf[${ref.sumSource.sClass.getName}]""",
            usedTypes = Set(ref.viewSource.sumSource)),
        SMethodStatement(content = s"""val tp: ${ref.sumSource.tClass.getName} = getPlayerOfType(target, "${ref.sumSource.tClass.getName}").asInstanceOf[${ref.sumSource.tClass.getName}]""",
            usedTypes = Set(ref.viewTarget.sumSource)),
        SMethodStatement(content = s"val v: ${ref.sumSource.getName} = new ${ref.sumSource.getName}(sp, tp)",
            usedTypes = Set(ref.sumSource)),
        SMethodStatement(content = s"return v"))    
    method.implementation = statements
    method.setVisibility(MethodVisibility.protectedVis)
    method
  }
  
  def getGetNaturalRoleMethod(types: Set[SInnerViewNaturalClass]): SMethod = {
    val method = new SMethod(
      name = "getNaturalRole",
      result = PredefRsumTypes.AVIEW_ROLE_STYPE,
      params = Seq(SMethodParameter("className", PredefTypes.Object)),
      implementation = Seq.empty)
    var statements: Seq[SMethodStatement] = Seq.empty 
    types.foreach(t => {
      if (!t.isAbstract && !t.isInterface) {        
        var statement = s"if (className.isInstanceOf[${t.sumSource.getName}]) return new " +
          s"${t.getName}(${t.getAllConstructorParameters.map(HelperFunctions.classEmptyConstructorParameterCreation(_)).mkString(", ")})"
        statements = statements :+ SMethodStatement(content = statement, usedTypes = Set(t))
      }
    })
    statements = statements :+ SMethodStatement(content = "return null")
    method.implementation = statements
    method.setVisibility(MethodVisibility.protectedVis)
    method
  }
  
  def getGetRelationalRoleMethod(types: Set[SInnerViewRelationalClass]): SMethod = {
    val method = new SMethod(
      name = "getRelationalRole",
      result = PredefRsumTypes.AVIEW_ROLE_STYPE,
      params = Seq(SMethodParameter("className", PredefTypes.Object), SMethodParameter("sourceRole", PredefRsumTypes.AVIEW_ROLE_STYPE), SMethodParameter("targetRole", PredefRsumTypes.AVIEW_ROLE_STYPE)),
      implementation = Seq.empty)
    
    var statements: Seq[SMethodStatement] = Seq.empty 
    types.foreach(t => {
      var statement = s"if (className.isInstanceOf[${t.sumSource.getName}]) return new " +
        s"${t.getName}(sourceRole.asInstanceOf[${t.viewSource.getName}], targetRole.asInstanceOf[${t.viewTarget.getName}])"
      statements = statements :+ SMethodStatement(content = statement, usedTypes = Set(t))
    })
    statements = statements :+ SMethodStatement(content = "return null")
    method.implementation = statements    
    method.setVisibility(MethodVisibility.protectedVis)
    method
  }
    
  def getIsViewableMethod(types: Set[SClass]): SMethod = {
    val method = new SMethod(
      name = "isViewable",
      result = PredefTypes.Boolean,
      params = Seq(SMethodParameter("className", PredefTypes.Object)),
      implementation = Seq.empty)
    var newTypes: Set[STypedElement] = Set.empty
    types.filter(_.isInstanceOf[SJoinClass]).foreach(cls => {
      newTypes = newTypes + cls.asInstanceOf[SJoinClass].base + cls.asInstanceOf[SJoinClass].other
    })
    if (types.isEmpty) {
      method.implementation = Seq(SMethodStatement(content = "return false", usedTypes = newTypes))
    } else {
      val statement = s"if (${types.filter(t => !t.isAbstract && !t.isInterface).map(HelperFunctions.classNameCreation(_)).mkString(" || ")}) return true"
      method.implementation = Seq(SMethodStatement(content = statement, usedTypes = types.asInstanceOf[Set[STypedElement]]),
          SMethodStatement(content = "return false", usedTypes = newTypes))
    }
    method.setVisibility(MethodVisibility.protectedVis)
    method
  }  
      
  def getViewNameMethod(viewCompartment: SClass): SMethod = {
    new SMethod(
      name = "getViewName",
      result = PredefTypes.String,
      params = Seq.empty,
      implementation = Seq(SMethodStatement(content = s"${viewCompartment.getName}.getViewName()", usedTypes = Set(PredefTypes.String))),
      overrides = true)
  }
  
  def getViewAttributeGetter(struc: SStructuralFeature): SMethod = {
    new SMethod(
      name = s"get${struc.getName.firstLetterToUpperCase}View",
      result = struc.getTypeElement,
      params = Seq.empty,
      implementation = Seq(SMethodStatement(content = s"+this get${struc.getName.firstLetterToUpperCase}()", usedTypes = Set(struc.getTypeElement))))
  }
  
  def getViewAttributeSetter(struc: SStructuralFeature): SMethod = {
    new SMethod(
      name = s"set${struc.getName.firstLetterToUpperCase}View",
      result = PredefTypes.Unit,
      params = Seq(SMethodParameter(struc.getName.toLowerCase(), struc.getTypeElement)),
      implementation = Seq(SMethodStatement(content = s"+this set${struc.getName.firstLetterToUpperCase}(${struc.getName.toLowerCase()})", usedTypes = Set(struc.getTypeElement)), 
          SMethodStatement(content = s"+this changeSomething()")))
  }
  
  def createAllReferenceLinks(refClass: SInnerViewRelationalClass, createNormal: Boolean, createOpposite: Boolean): Unit = {
    val targetClass: SInnerViewNaturalClass = refClass.viewTarget
    val sourceClass: SInnerViewNaturalClass = refClass.viewSource
    val realRef: SReference = refClass.sumSource.connectedRef
    val oppoRef: SReference = refClass.sumSource.connectedRef.oppositeRef
    if (createNormal) {
      createReferenceLinks(refClass, realRef, sourceClass, targetClass, true)
    }
    if (createOpposite) {
      if (oppoRef != null) {
        createReferenceLinks(refClass, oppoRef, targetClass, sourceClass, false)
      }
    }
  }

  private def createReferenceLinks(refClass: SInnerViewRelationalClass, realRef: SReference, sourceClass: SInnerViewNaturalClass, targetClass: SInnerViewNaturalClass, isReal: Boolean): Unit = {
    val getStatement: String = if (isReal) "getTarget" else "getSource"
    val thisStatement: String = if (isReal) "(this, v)" else "(v, this)"
    if (realRef.upperBound == 1) {
      //upper Bound = 1
      //variable settings
      sourceClass.augmentConstructor(SMethodStatement(s"private var ${realRef.getName}: ${refClass.getDeepName} = null"))
      //internal variables
      val removeMethodInternal = new SMethod(
        name = s"remove${realRef.getName.capitalize}Intern",
        result = PredefTypes.Unit,
        params = Seq(SMethodParameter("v", refClass)), //TODO: remove this then each remove internal method has different input parameters
        implementation = Seq(SMethodStatement(content = s"${realRef.getName} = null")))
      removeMethodInternal.setVisibility(MethodVisibility.privateExternalClass)
      sourceClass.addMethod(removeMethodInternal)
      val setMethodInternal = new SMethod(
        name = s"set${realRef.getName.capitalize}Intern",
        result = PredefTypes.Unit,
        params = Seq(SMethodParameter("v", refClass)),
        implementation = Seq(SMethodStatement(content = s"${realRef.getName} = v")))
      setMethodInternal.setVisibility(MethodVisibility.privateExternalClass)
      sourceClass.addMethod(setMethodInternal)
      //external variables
      sourceClass.addMethod(new SMethod(
        name = s"get${realRef.getName.capitalize}",
        result = targetClass,
        params = Seq.empty,
        implementation = Seq(SMethodStatement(content = s"return ${realRef.getName}.${getStatement}()"))))
      sourceClass.addMethod(new SMethod(
        name = s"set${realRef.getName.capitalize}",
        result = PredefTypes.Boolean,
        params = Seq(SMethodParameter("v", targetClass)),
        implementation = Seq(SMethodStatement(content = "if (v == null) return false"),
          SMethodStatement(content = "if (!containsRole(v.asInstanceOf[AViewRole])) return false"),
          SMethodStatement(content = s"if (${realRef.getName} != null) {"),
          SMethodStatement(content = s"if (${realRef.getName}.${getStatement}() == v) return false"),
          SMethodStatement(content = s"${realRef.getName}.deleteElement()"),
          SMethodStatement(content = "}"),
          SMethodStatement(content = s"new ${refClass.getName}${thisStatement}"),
          SMethodStatement(content = "return true"))))
    } else {
      //upper Bound = endless
      //variable settings
      sourceClass.augmentConstructor(SMethodStatement(s"private var ${realRef.getName}: Set[${refClass.getDeepName}] = Set.empty"))
      //internal variables
      val removeMethodInternal = new SMethod(
        name = s"remove${realRef.getName.capitalize}Intern",
        result = PredefTypes.Unit,
        params = Seq(SMethodParameter("v", refClass)),
        implementation = Seq(SMethodStatement(content = s"${realRef.getName} -= v")))
      removeMethodInternal.setVisibility(MethodVisibility.privateExternalClass)
      sourceClass.addMethod(removeMethodInternal)
      val setMethodInternal = new SMethod(
        name = s"set${realRef.getName.capitalize}Intern",
        result = PredefTypes.Unit,
        params = Seq(SMethodParameter("v", refClass)),
        implementation = Seq(SMethodStatement(content = s"${realRef.getName} += v")))
      setMethodInternal.setVisibility(MethodVisibility.privateExternalClass)
      sourceClass.addMethod(setMethodInternal)
      //external variables
      sourceClass.addMethod(new SMethod(
        name = s"get${realRef.getName.capitalize}",
        result = SSet(targetClass),
        params = Seq.empty,
        implementation = Seq(SMethodStatement(content = s"var vs: Set[${targetClass.getDeepName}] = Set.empty"),
          SMethodStatement(content = s"${realRef.getName}.foreach{ v => vs += v.${getStatement}()}"),
          SMethodStatement(content = "return vs"))))
      sourceClass.addMethod(new SMethod(
        name = s"has${realRef.getName.capitalize}",
        result = PredefTypes.Boolean,
        params = Seq(SMethodParameter("v", targetClass)),
        implementation = Seq(SMethodStatement(content = s"return get${realRef.getName.capitalize}.contains(v)"))))
      sourceClass.addMethod(new SMethod(
        name = s"add${realRef.getName.capitalize}",
        result = PredefTypes.Boolean,
        params = Seq(SMethodParameter("v", targetClass)),
        implementation = Seq(SMethodStatement(content = s"if (has${realRef.getName.capitalize}(v) || !containsRole(v.asInstanceOf[AViewRole])) return false"),
          SMethodStatement(content = s"new ${refClass.getName}${thisStatement}"),
          SMethodStatement(content = "return true"))))
      sourceClass.addMethod(new SMethod(
        name = s"remove${realRef.getName.capitalize}",
        result = PredefTypes.Boolean,
        params = Seq(SMethodParameter("v", targetClass)),
        implementation = Seq(SMethodStatement(content = s"if (!has${realRef.getName.capitalize}(v)) return false"),
          SMethodStatement(content = s"${realRef.getName}.foreach{ h =>"),
          SMethodStatement(content = s"if (h.${getStatement}() == v) {"),
          SMethodStatement(content = "h.deleteElement()"),
          SMethodStatement(content = "return true"),
          SMethodStatement(content = "}}"),
          SMethodStatement(content = "return true"))))
    }
  }
}