package org.rosi_project.model_sync.generator.sync

import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes
import org.rosi_project.model_sync.generator.PackageNames

class GenerateQueryHelperVisitor extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {
    var newClasses: Set[SClass] = Set.empty
    //iterate over all abstract classes or interfaces
    sModel.getModelClasses.foreach(cls => {
      if (cls.isAbstract || cls.isInterface) {
        val newClass = new SClass(PackageNames.queryHelperPrefix + cls.getName, cls.getPackage)
        newClass.addParent(cls)
        newClass.addParent(PredefRsumTypes.QUERY_HELPER_STYPE)
        newClass.addMethod(new SMethod(
          name = "equals",
          result = PredefTypes.Boolean,
          params = Seq(SMethodParameter("that", PredefTypes.Any)),
          implementation = Seq(SMethodStatement(content = s"that.isInstanceOf[${cls.getName}]", usedTypes = Set.empty)), 
          true))
        newClasses += newClass
      }
    })
    newClasses.foreach(sModel.addModelClass(_))
  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }
  
  override def visit(sRef: SReference): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }
}