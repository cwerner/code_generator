package org.rosi_project.model_sync.generator.util

import scala.collection.JavaConverters._
import org.eclipse.emf.ecore.EObject
import org.rosi_project.model_sync.generator.acr_model.STypedElement
import org.eclipse.emf.ecore.EClass
import org.rosi_project.model_sync.generator.sync.HelperFunctions
import org.rosi_project.model_sync.generator.acr_model.SMethodParameter
import org.rosi_project.model_sync.generator.acr_model.STypeRegistry
import org.eclipse.emf.ecore.EStructuralFeature

class InstanceLine(val counter: Int, val obj: EObject, val usedType: STypedElement, val lineString: String = "") {
  
  def getLine(): String = {
    if (obj != null) {
      return s"val i${counter} = new ${usedType.getName} (${usedType.getAllConstructorParameters.map(classEmptyConstructorParameterCreation(_)).mkString(", ")})"
    }
    return lineString
  }
  
  def getName(): String = "i" + counter
  
  def classEmptyConstructorParameterCreation(t: SMethodParameter): String = {
    if (STypeRegistry.isDefaultType(t.getType.getName)) {
      var structs: Seq[EStructuralFeature] = obj.eClass().getEAllStructuralFeatures.asScala
      structs.foreach(att => {
        if (att.getName == t.getName) {
          if (t.getType.getName == "String" && obj.eGet(att) != null) {
            return s""" "${obj.eGet(att)}" """
          } else {
            return obj.eGet(att) + ""
          }
        }    
      })
    }
    return HelperFunctions.classEmptyType(t.getTypeName)
  }
}