package org.rosi_project.model_sync.generator.support

/** Adds more methods to a `String`.
  *
  * @author Rico Bergmann
  */
class ExtendedString(wrappedString: String) {

  /** Turns the first character of a `String` into its upper-case equivalent.
    *
    * If no such equivalent exists, nothing will happen. If the `String` is empty neither.
    */
  def firstLetterToUpperCase: String =
    wrappedString.headOption.map(h => h.toUpper + wrappedString.tail).getOrElse("")

}

/** The companion provides the implicit conversion.
  */
object ExtendedString {

  implicit def stringToExtended(str: String): ExtendedString = new ExtendedString(str)

}

