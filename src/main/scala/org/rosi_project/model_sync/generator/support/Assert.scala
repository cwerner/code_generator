package org.rosi_project.model_sync.generator.support

/** Utilities to enforce some domain constraints. If the constraint fails, most methods will throw
  * an `AssertionError` (unless stated otherwise).
  *
  * @author Rico Bergmann
  */
object Assert {

  /** Ensures that an object is not `null`. Will throw an `AssertionError` if it is.
    *
    * @param obj the object to check
    * @param msg the message to display in case the constraint fails
    */
  def notNull(obj: AnyRef, msg: String): Unit = isTrue(obj != null, msg)

  /** Ensures that some condition holds (i.e. evaluates to `true`). Will throw an `AssertionError`
    * if it `false`.
    *
    * @param cond the condition to evaluate
    * @param msg the message to display in case the constraint fails
    */
  def isTrue(cond: Boolean, msg: String): Unit = if (!cond) {
      throw new AssertionError(msg)
  }

  /** Ensures that some condition does '''not''' hold (i.e. evaluates to `false`). Will throw an
    * `AssertionError` if it is `true`.
    *
    * @param cond the condition to evaluate
    * @param msg the message to display in case the constraint fails
    */
  def isFalse(cond: Boolean, msg: String): Unit = isTrue(!cond, msg)

}
