package org.rosi_project.model_sync.generator

/** Contains various helper classes that are independent of the actual domain business logic.
  *
  * @author Rico Bergmann
  */
package object support {

}
