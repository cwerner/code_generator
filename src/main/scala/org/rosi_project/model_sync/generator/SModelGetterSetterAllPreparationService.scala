package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.acr_model.SModel
import org.rosi_project.model_sync.generator.sync.GetterSetterGeneratingVisitor

/** Simple service that adds getter and setter for Attributes and References.
  *
  * @author Christopher Werner
  */
class SModelGetterSetterAllPreparationService {

  /** Adds getter and setter for Attributes and References.
    *
    * @param sModel the model to augment
    */
  def prepareModel(sModel: SModel): Unit = {
    val getterSetterVisitor = new GetterSetterGeneratingVisitor
    sModel.accept(getterSetterVisitor)
  }

}
