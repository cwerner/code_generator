package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.acr_model.SModel
import org.rosi_project.model_sync.generator.ui.SyncUiVisitor

/** Simple service to add all the necessary adaption classes of an [[SModel]] to make applicable for
  * synchronization user interface.
  *
  * @author Christopher Werner
  */
class SModelSyncUiPreparationService {
  
  /** Add UI provider functionality too a [[SModel]] with the necessary methods and statements to make it usable
    * in the user interface.
    *
    * @param sModel the model to augment
    */
  def prepareModel(sModel: SModel, modelCfg: ModelConfig): Unit = {
    if (modelCfg != null) {
      val syncUiVisitor = new SyncUiVisitor(modelCfg)
      sModel.accept(syncUiVisitor)
    }
  }
}