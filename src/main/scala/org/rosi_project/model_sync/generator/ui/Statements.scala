package org.rosi_project.model_sync.generator.ui

import org.rosi_project.model_sync.generator.acr_model.SMethodStatement

/** Contains method statements which are used for throughout the synchronization.
  *
  * @see [[SMethodStatement]]
  *
  * @author Rico Bergmann
  */
object Statements {

  val ModelRegistration = SMethodStatement("val registered = ModelRegistry.registerNewModel(this)", Set(PredefUiTypes.MODEL_REGISTRY_STYPE))

}
