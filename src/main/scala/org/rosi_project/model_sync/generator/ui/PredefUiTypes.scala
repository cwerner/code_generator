package org.rosi_project.model_sync.generator.ui

import org.rosi_project.model_sync.generator.acr_model.SType
import org.rosi_project.model_sync.provider._
import org.rosi_project.model_sync.instances._

object PredefUiTypes {
  
  val RES_PATH = "res/"
  
  private val DISPLAYABLE_MODEL_CLASS = classOf[DisplayableModel]
  val         DISPLAYABLE_MODEL_STYPE = SType(DISPLAYABLE_MODEL_CLASS.getSimpleName, DISPLAYABLE_MODEL_CLASS.getPackage.getName)
  
  private val DISPLAYABLE_MODEL_FOR_INITIALIZATION_CLASS = classOf[DisplayableModelForInitialization]
  val         DISPLAYABLE_MODEL_FOR_INITIALIZATION_STYPE = SType(DISPLAYABLE_MODEL_FOR_INITIALIZATION_CLASS.getSimpleName, DISPLAYABLE_MODEL_FOR_INITIALIZATION_CLASS.getPackage.getName)

  private val DISPLAYABLE_MODEL_FOR_INTEGRATION_CLASS = classOf[DisplayableModelForIntegration]
  val         DISPLAYABLE_MODEL_FOR_INTEGRATION_STYPE = SType(DISPLAYABLE_MODEL_FOR_INTEGRATION_CLASS.getSimpleName, DISPLAYABLE_MODEL_FOR_INTEGRATION_CLASS.getPackage.getName)
  
  private val MODEL_SYNC_PROVIDER_CLASS = classOf[ModelSyncProvider]
  val         MODEL_SYNC_PROVIDER_STYPE = SType(MODEL_SYNC_PROVIDER_CLASS.getSimpleName, MODEL_SYNC_PROVIDER_CLASS.getPackage.getName)
  
  private val MODEL_REGISTRY_CLASS = ModelRegistry.getClass
  val         MODEL_REGISTRY_STYPE = SType(MODEL_REGISTRY_CLASS.getSimpleName.replace("$", ""), MODEL_REGISTRY_CLASS.getPackage.getName)
  
  private val MODEL_INSTANCE_CONSTRUCTOR_CLASS = classOf[ModelInstanceConstructor]
  val         MODEL_INSTANCE_CONSTRUCTOR_STYPE = SType(MODEL_INSTANCE_CONSTRUCTOR_CLASS.getSimpleName, MODEL_INSTANCE_CONSTRUCTOR_CLASS.getPackage.getName)
  
  private val MODEL_INSTANCE_MODIFIER_CLASS = classOf[ModelInstanceModifier]
  val         MODEL_INSTANCE_MODIFIER_STYPE = SType(MODEL_INSTANCE_MODIFIER_CLASS.getSimpleName, MODEL_INSTANCE_MODIFIER_CLASS.getPackage.getName)
}