package org.rosi_project.model_sync.generator.ui

import org.rosi_project.model_sync.generator.acr_model.{SClass, SImport, SMethod, SMethodParameter, SMethodStatement, SType, types => acr}
import org.rosi_project.model_sync.generator.sync.string2SeqMethodImpl
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes

/** Creates implementations of [[ModelInstanceConstructor]] for model classes.
  *
  * @param modelClass the class for which the constructor should be wrapped
  * @author Rico Bergmann
  */
class ConstructorTemplate(modelClass: SClass) extends SClass(_name = s"${modelClass.getName}Constructor", _sPackage = s"${modelClass.sPackage}.visualize") {

  addParent(PredefUiTypes.MODEL_INSTANCE_CONSTRUCTOR_STYPE)

  private val constructorParams: String = generateConstructorParams

  constructorStatements = constructorStatements :+ SMethodStatement(s"private val ${ConstructorTemplate.ConstructorAttr} = classOf[${modelClass.getName}].getConstructor($constructorParams)")

  methods = Seq(
    new SMethod(
      name = "getParameters",
      result = acr.SSeq(acr.PredefTypes.Parameter),
      params = Seq.empty,
      implementation = s"${ConstructorTemplate.ConstructorAttr}.getParameters",
      overrides = true
    ),
    new SMethod(
      name = "invoke",
      result = PredefTypes.AnyRef,
      params = Seq(SMethodParameter("args", acr.SSeq(acr.PredefTypes.Object))),
      implementation = s"${ConstructorTemplate.ConstructorAttr}.newInstance(args: _*)",
      overrides = true
    ),
    new SMethod(
      name = "toString",
      result = PredefTypes.String,
      params = Seq.empty,
      implementation = s""" "${modelClass.getName} Constructor" """,
      overrides = true
    )
  )

  override def getNecessaryImports: Set[SImport] = {
    (modelClass.getUsedTypes.filter(_.getPackage != "").map(elem => SImport(elem.getPackage, elem.getName)) 
      ++ super.getNecessaryImports 
      + SImport(modelClass.getPackage, modelClass.getName))      
  }

  private def generateConstructorParams: String = {
    modelClass.getDeepStructuralFeatures.map(attr => s"classOf[${attr.getTypeElement.getName}]").mkString(", ")
  }
}

object ConstructorTemplate {

  val ConstructorAttr = "constructor"

}
