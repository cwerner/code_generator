package org.rosi_project.model_sync.generator.ui

import org.rosi_project.model_sync.generator.Model
import org.rosi_project.model_sync.generator.acr_model.{SClass, SMethod, types => acr}
import org.rosi_project.model_sync.generator.support.ExtendedString.stringToExtended
import org.rosi_project.model_sync.generator.sync.string2SeqMethodImpl
import scala.collection.Seq

/** Creates implementations of [[ModelSyncProvider]] for models.
  *
  * @param model the model which should be initialized
  * @author Rico Bergmann
  */
class ModelProviderTemplate(model: Model) extends SClass(_name = "ModelProvider", _sPackage = s"${Model.parseClass(model.primaryClass)._1}.visualize") {

  addParent(PredefUiTypes.MODEL_SYNC_PROVIDER_STYPE)

  methods = Seq(
    new SMethod (
      name = "getInitialDisplayableModel",
      result = PredefUiTypes.DISPLAYABLE_MODEL_FOR_INITIALIZATION_STYPE,
      params = Seq.empty,
      implementation = s"new Initialized${Model.parseClass(model.primaryClass)._2.firstLetterToUpperCase}"
    ),
    new SMethod (
      name = "getDisplayableModelsForIntegration",
      result = acr.SSeq(PredefUiTypes.DISPLAYABLE_MODEL_FOR_INTEGRATION_STYPE),
      params = Seq.empty,
      implementation = "Seq.empty"
    )
  )

}
