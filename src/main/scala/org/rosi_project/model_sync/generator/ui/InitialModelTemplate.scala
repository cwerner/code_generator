package org.rosi_project.model_sync.generator.ui

import java.io.File
import org.rosi_project.model_sync.generator.{Model, ModelDataException}
import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.acr_model.{types => acr}
import org.rosi_project.model_sync.generator.support.ExtendedString.stringToExtended
import org.rosi_project.model_sync.generator.sync.string2SeqMethodImpl
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes

/** Creates implementations of [[DisplayableModelForInitialization]] for models.
  *
  * @param model the model which should be initialized
  * @author Rico Bergmann
  */
class InitialModelTemplate(model: Model) extends SClass(_name = s"Initialized${Model.parseClass(model.primaryClass)._2.firstLetterToUpperCase}", _sPackage = s"${Model.parseClass(model.primaryClass)._1}.visualize") {

  addParent(SType("DisplayableModelForInitialization", "org.rosi_project.model_sync.provider"))

  private val primaryClassComps = Model.parseClass(model.primaryClass)
  private val primaryClass: SClass = STypeRegistry.query(primaryClassComps._2, primaryClassComps._1).getOrElse(throw new ModelDataException).asInstanceOf[SClass]
  private val additionalClasses: Seq[SClass] = model.additionalClasses.getOrElse(List()).map(Model.parseClass).map(cls => STypeRegistry.query(cls._2, cls._1).getOrElse(throw new ModelDataException).asInstanceOf[SClass])
  private val allModelClasses = primaryClass +: additionalClasses

  private var modelImagePath = "None"
  model.image.foreach(image => {
    val imageFile = new File(image)
    modelImagePath = s"""Some(new File("${PredefUiTypes.RES_PATH + imageFile.getName}"))"""
  })


  methods = Seq(
    new SMethod (
      name = "getName",
      result = PredefTypes.String,
      params = Seq.empty,
      implementation = s""" "${model.name}" """,
      true
    ),
    new SMethod (
      name = "getInstanceClass",
      result = SType("Class[_ <: AnyRef]"),
      params = Seq.empty,
      implementation = Seq(SMethodStatement(s"classOf[${primaryClass.getName}]", Set(primaryClass))),
      true
    ),
    new SMethod (
      name = "getModelDiagram",
      result = acr.PredefTypes.option(acr.PredefTypes.File),
      params = Seq.empty,
      implementation = modelImagePath,
      true
    ),

    new SMethod (
      name = "getConstructors",
      result = acr.SSeq(PredefUiTypes.MODEL_INSTANCE_CONSTRUCTOR_STYPE),
      params = Seq.empty,
      implementation = s"Seq(${allModelClasses
        .filter(!_.isAbstract)
        .map(cls => 
          s"new ${cls.getName}Constructor").mkString(", ")})",
      true
    ),
    new SMethod (
      name = "getUpdateMethods",
      result = acr.SSeq(PredefUiTypes.MODEL_INSTANCE_MODIFIER_STYPE),
      params = Seq.empty,
      implementation = s"Seq(${allModelClasses
          .filter(_.getStructuralFeatures.nonEmpty)
          .map(cls => {
            cls.getStructuralFeatures.map(attr => s"new ${cls.getName}${attr.getName.firstLetterToUpperCase}Modifier").mkString(", ")             
              })
          .mkString(", ")})",
      true
    ),
    new SMethod(
      name = "toString",
      result = PredefTypes.String,
      params = Seq.empty,
      implementation = s""" "${getName}" """,
      overrides = true
    )
    /*new SMethod (
      name = "getInstances",
      result = acr.SSeq(SType.AnyRef),
      params = Seq.empty,
      implementation = "Seq.empty", // TODO
      true
    )*/
  )

  if (model.nested.nonEmpty) {

    methods = methods :+ new SMethod(
      name = "getNestedModels",
      result = acr.SSeq(PredefUiTypes.DISPLAYABLE_MODEL_STYPE),
      params = Seq.empty,
      implementation = s"Seq( ${model.nested.getOrElse(List()).map(nested => "new Initialized"  + Model.parseClass(nested.primaryClass)._2.firstLetterToUpperCase).mkString(", ")} )",
      overrides = true
    )

  }

  augmentConstructor(Statements.ModelRegistration)

  /*override def getNecessaryImports: Set[SImport] = {
    val base = super.getNecessaryImports
    
    if (primaryClass.isDefaultPackage) {
      base
    } else {
      base + SImport(primaryClass.getPackage, primaryClass.getName)
    }
  }*/

}
