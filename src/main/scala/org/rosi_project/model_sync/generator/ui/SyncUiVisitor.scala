package org.rosi_project.model_sync.generator.ui

import org.rosi_project.model_sync.generator.ModelConfig
import org.rosi_project.model_sync.generator.acr_model._

/**
 * Add the UI Provider classes for the visualisation in the user interface.
 */
class SyncUiVisitor(val modelCfg: ModelConfig) extends SModelVisitor {

  override def visit(sModel: SModel): Unit = {
    var additionalSyncClasses: Seq[SClass] = Seq.empty
    sModel.getModelClasses.foreach(cls => {
      if (!cls.isAbstract && !cls.isInterface) {
        additionalSyncClasses = additionalSyncClasses :+ new ConstructorTemplate(cls)
      }
      cls.getStructuralFeatures.foreach(struc => {
        additionalSyncClasses = additionalSyncClasses :+ new ModifierTemplate(cls, struc)
      })
    })

    additionalSyncClasses.foreach(sModel.addProviderClass)

    modelCfg.init.nested.foreach(_.foreach(model => sModel.addProviderClass(new InitialModelTemplate(model))))
    sModel.addProviderClass(new InitialModelTemplate(modelCfg.init))
    sModel.addProviderClass(new ModelProviderTemplate(modelCfg.init))

  }

  override def visit(sClass: SClass): Unit = {
    // pass
  }

  override def visit(sAttr: SAttribute): Unit = {
    // pass
  }

  override def visit(sRef: SReference): Unit = {
    // pass
  }

  override def visit(sMethod: SMethod): Unit = {
    // pass
  }

  override def visit(sType: SType): Unit = {
    // pass
  }
}