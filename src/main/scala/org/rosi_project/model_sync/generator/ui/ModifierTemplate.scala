package org.rosi_project.model_sync.generator.ui

import org.rosi_project.model_sync.generator.acr_model.{GenericSType, SStructuralFeature, SClass, SImport, SMethod, SMethodParameter, SMethodStatement, SType, STypedElement, types => acr}
import org.rosi_project.model_sync.generator.support.ExtendedString.stringToExtended
import org.rosi_project.model_sync.generator.sync.string2SeqMethodImpl
import scala.collection.Seq
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes

/** Creates implementations of [[ModelInstanceModifier]] for attributes of model classes.
  *
  * @param modelClass the class which contains the attribute
  * @param attribute the attribute for which the modifier should be created
  * @author Rico Bergmann
  */
class ModifierTemplate(modelClass: SClass, attribute: SStructuralFeature) extends SClass(_name = s"${modelClass.getName}${attribute.getName.firstLetterToUpperCase}Modifier", _sPackage = s"${modelClass.sPackage}.visualize") {

  addParent(PredefUiTypes.MODEL_INSTANCE_MODIFIER_STYPE)

  constructorStatements = constructorStatements :+
    SMethodStatement(s"""private val ${ModifierTemplate.MethodAttr} = classOf[${modelClass.getName}].getMethod("set${attribute.getName.firstLetterToUpperCase}", classOf[${attribute.getTypeElement.getName}])""")

  methods = Seq(
    new SMethod (
      name = "getParameters",
      result = acr.SSeq(acr.PredefTypes.Parameter),
      params = Seq.empty,
      implementation = s"${ModifierTemplate.MethodAttr}.getParameters",
      overrides = true
    ),
    new SMethod (
      name = "invoke",
      result = PredefTypes.Unit,
      params = Seq(SMethodParameter("instance", PredefTypes.AnyRef), SMethodParameter("args", acr.SSeq(PredefTypes.AnyRef))),
      implementation = s"${ModifierTemplate.MethodAttr}.invoke(instance, args: _*)",
      overrides = true
    ),
    new SMethod(
      name = "toString",
      result = PredefTypes.String,
      params = Seq.empty,
      implementation = s""" "Change ${attribute.getName}" """,
      overrides = true
    )
  )

  override def getNecessaryImports: Set[SImport] = {
    val attrImports = attribute.getTypeElement match {
      case gt : GenericSType => SImport.generateImports(gt, gt.typeParam)
      case t : STypedElement => SImport.generateImports(t)
    }
    modelClass.getNecessaryImports ++ super.getNecessaryImports + SImport(modelClass.getPackage, modelClass.getName) ++ attrImports
  }

}

object ModifierTemplate {

  val MethodAttr = "method"

}
