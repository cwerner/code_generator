package org.rosi_project.model_sync.generator

import java.io.File
import scala.io.Source
import net.liftweb.json._

/** Wrapper for the different command line options for the [[Generator]]
  *
  * @author Rico Bergmann
  */
case class GeneratorConfig(sources: Seq[String] = Seq(), 
    cleanUp: Boolean = false, 
    outDir: File = new File(System.getProperty("user.dir")), 
    workDir: File = null,
    modelJoin: String = null, //can be more than one
    syncLanguage: String = null, //can be more than one
    create: Creation.Value = Creation.rolesync) {
  
  def hasWorkDir: Boolean = workDir != null
  
  private def getEcoreNames: Seq[String] = sources.map(_.split("/").last.replace(".ecore", ""))
  
  def getCombinedEcoreName: String = getEcoreNames.mkString
  
  def getModelConfigFromEcore(source: String): ModelConfig = { 
    var modelCfgs: ModelConfig = null
    var f = new File(source.replace(".ecore", ".sync.json"))
    //println("---File: " + f.exists())
    if (f == null || !f.exists()) return null
    val modelJson = Source.fromFile(f).getLines.mkString
    implicit val jsonFormats: Formats = DefaultFormats
    parse(modelJson).extract[ModelConfig]
  }
  
  def getAllModelConfigs: Set[ModelConfig] = { 
    var configs: Set[ModelConfig] = Set.empty
    sources.foreach(s => {
      var modelCfg = getModelConfigFromEcore(s)
      if (modelCfg != null) {
        configs += modelCfg
      }
    })
    configs
  }

  def getModelPath(source: String): File = new File(source).getParentFile
  
  def getModelJoinFile: File = if(modelJoin == null) null else new File(modelJoin) 
  
  def getModelJoinName: String = modelJoin.split("/").last.replace(".modeljoin", "").capitalize
  
  def getSnchronizationFile: File = if(syncLanguage == null) null else new File(syncLanguage)

}
