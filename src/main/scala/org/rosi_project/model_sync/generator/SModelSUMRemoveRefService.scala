package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.acr_model.SModel
import org.rosi_project.model_sync.generator.sync.ReferenceRemoveVisitor

/** Remove references from model classes.
  *
  * @author Christopher Werner
  */
class SModelSUMRemoveRefService {
  
  /** Remove reference from model classes.
    *
    * @param sModel the model to augment
    */
  def prepareModel(sModel: SModel): Unit = {
    val removeReferencesVisitor = new ReferenceRemoveVisitor

    //order is important !!!!    
    sModel.accept(removeReferencesVisitor)
  }
}