package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.acr_model.SModel
import org.rosi_project.model_sync.generator.util.ClassAndInstance
import org.rosi_project.model_sync.generator.sync.InstanceGenerator

/** Simple service to create instances.
  *
  * @author Christopher Werner
  */
class SModelSyncInstanceService {
  
  /** Add a new class to the [[SModel]] to instantiate it.
    *
    * @param sModel the model to augment
    */
  def prepareModel(sModel: SModel, clsAins: ClassAndInstance): Unit = {
    val insgenerator = new InstanceGenerator(clsAins)
    sModel.accept(insgenerator)   
  }
}