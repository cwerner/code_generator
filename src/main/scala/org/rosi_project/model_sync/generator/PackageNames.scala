package org.rosi_project.model_sync.generator

object PackageNames {
  
  val multiInhertitanceWithTraits = false
  
  var sourcePkgPrefix: String = "" //sync. , sum.
  
  val viewPkgName: String = "view"
  val viewPostName: String = "View"
  val viewRolePostName: String = "Role"
  
  val queryPkgName: String = "query"
  val queryPostName: String = "Query"
  val queryRolePostName: String = "Role"
  val queryHelperPrefix: String = "Helper"
  
  val joinPkgName: String = "join"
  
  val examplePkgName: String = "example"
  
  val creationPkgName: String = "creation"
  
}