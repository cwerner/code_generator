package org.rosi_project.model_sync.generator

import org.eclipse.emf.ecore._
import org.rosi_project.model_sync.generator.conversion.SModelGenerator
import org.rosi_project.model_sync.generator.env.{ CompilationException, JarPackaginException }
import org.rosi_project.model_sync.generator.io.{ ClassWritingException, ModelImagePreparationException, SModelFSWriter }

import scala.reflect.io.File
import org.rosi_project.model_sync.generator.acr_model.SModel
import org.rosi_project.model_sync.generator.acr_model.STypeRegistry
import org.rosi_project.model_sync.model_join.representation.parser.antlr.AntlrBackedModelJoinParser
import org.rosi_project.model_sync.generator.util.ClassAndInstance

/**
 * The `Generator` runs the whole workflow of generating a JAR of compiled Scala source files
 * based on an ECORE file of some sync model.
 *
 * @param config the configuration that tells the generator where to locate the necessary files.
 *
 * @author Rico Bergmann
 */
class Generator(config: GeneratorConfig) {

  /**
   * Starts the generation.
   */
  def run(): Unit = {
    //set package name of scala model files
    if (config.create == Creation.rolesum)
      PackageNames.sourcePkgPrefix = "sum."
    if (config.create == Creation.rolesync)
      PackageNames.sourcePkgPrefix = "sync."
    if (config.create == Creation.rolecomb)
      PackageNames.sourcePkgPrefix = ""
        
    try {
      if (config.sources.isEmpty) {
        println("Error: There must be any source file to generate something!!!")
        return
      }

      //read the ecore models
      var ecoreModels: Map[String, ClassAndInstance] = Map.empty
      try {
        config.sources.foreach(s => {
          ecoreModels = ecoreModels + (s -> (new EcoreLoader loadEcore s))
        })
      } catch {
        case e: Exception => throw new EcoreLoadException(e)
      }

      //create s models from the incoming ecore models
      var sModels: Map[SModel, ClassAndInstance] = Map.empty
      ecoreModels.foreach(ec => {
        sModels = sModels + ((new SModelGenerator convert (ec._2.pkg, ec._1)) -> ec._2)
      })

      sModels.foreach(sm => {
        /*general stuff, that should always be created */
        //create relational compartments (are only write out if not rolesync)          
        new SModelRelationalCompartmentPreparationService prepareModel (sm._1)

        //create the new classes for rsum and rsync
        if (config.create == Creation.rolesync) {
          //instance generation for rsync
          if (sm._2.obj != null) {
            new SModelSyncInstanceService prepareModel (sm._1, sm._2)
          }
          //add getter and setter for all methods
          new SModelGetterSetterAllPreparationService prepareModel (sm._1)
          //create the EMF Ecore reading models
          new SModelSUMReadEMFService prepareModel (sm._1, config)
          //add sync stuff
          new SModelOnlySyncService prepareModel (sm._1)
          //add ui provider stuff
          new SModelSyncUiPreparationService prepareModel (sm._1, config.getModelConfigFromEcore(sm._1.getSourceName))
        } else {
          //add basic setter and getter
          new SModelGetterSetterAttrPreparationService prepareModel (sm._1)
          if (config.create == Creation.rolecomb) {
            //add sync stuff
            new SModelOnlySyncService prepareModel (sm._1)
          }
          //create the EMF Ecore reading models
          new SModelSUMReadEMFService prepareModel (sm._1, config)
          //instance generation for combi means rsum
          if (sm._2.obj != null) {
            new SModelCombiInstanceService prepareModel (sm._1, sm._2)
          }
          if (config.create == Creation.rolecomb) {
            //add ref methods
            new SModelCombiRefMethodService prepareModel (sm._1)
          }
          //remove references
          new SModelSUMRemoveRefService prepareModel (sm._1)
          //create view and query stuff
          new SModelSUMPreparationService prepareModel (sm._1)
        }

      })

      // write the model and create the JAR
      println("... Writing model")

      var finalModel: SModel = null
      sModels.foreach(s => {
        if (finalModel == null) {
          finalModel = s._1
        } else {
          finalModel.addOtherModel(s._1)
        }
      })

      if (config.create != Creation.rolesync) {
        //create join stuff
        val modelJoinFile = config.getModelJoinFile
        if (modelJoinFile != null && modelJoinFile.exists()) {
          var modelJoin = new AntlrBackedModelJoinParser().readOrThrow(modelJoinFile);
          println(modelJoin);
          //val modelJoin = DefaultModelJoinParser.read(modelJoinFile).get
          modelJoin.setName(config.getModelJoinName)
          new SModelJoinPreparationService prepareModel (finalModel, modelJoin)
        }
      }

      if (config.hasWorkDir) {
        finalModel.accept(
          new SModelFSWriter(
            config,
            outputDir = File(config.outDir).toDirectory,
            workingDir = File(config.workDir),
            keepClassFiles = !config.cleanUp,
            modelCfgs = config.getAllModelConfigs,
            currentDir = config.getModelPath(finalModel.getSourceName)))
      } else {
        finalModel.accept(
          new SModelFSWriter(
            config,
            outputDir = File(config.outDir).toDirectory,
            keepClassFiles = !config.cleanUp,
            modelCfgs = config.getAllModelConfigs,
            currentDir = config.getModelPath(finalModel.getSourceName)))
      }
      println("... Done")
    } catch {
      case ele: EcoreLoadException =>
        throw ele
        println(s"** ERROR ** could not load ecore model: $ele")
      case mipe: ModelImagePreparationException =>
        throw mipe
        println(s"** ERROR ** could not prepare model images: $mipe")
      case cwe: ClassWritingException =>
        throw cwe
        println(s"** ERROR ** could not write classes: $cwe")
      case ce: CompilationException =>
        throw ce
        println(s"** ERROR ** could not compile classes: $ce")
      case jpe: JarPackaginException =>
        throw jpe
        println(s"** ERROR ** could not package JAR: $jpe")
    }
  }

}
