package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.acr_model.SModel
import org.rosi_project.model_sync.generator.util.ClassAndInstance
import org.rosi_project.model_sync.generator.sync.InstanceCombiGenerator

/** Simple service to create instances.
  * For the combination of sum and sync.
  * 
  * @author Christopher Werner
  */
class SModelCombiInstanceService {
  
  /** Add a new class to the [[SModel]] to instantiate it.
    *
    * @param sModel the model to augment
    */
  def prepareModel(sModel: SModel, clsAins: ClassAndInstance): Unit = {
    val insgenerator = new InstanceCombiGenerator(clsAins)
    sModel.accept(insgenerator)   
  }
}