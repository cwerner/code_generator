package org.rosi_project.model_sync.generator

/** Representation of a model configuration (i.e. a number of corresponding models). It tells the
  * generator how the model looks like, which types are needed, etc.
  *
  * This class as well as the derived types form a a reification of ''JSON'' files.
  *
  * @param name the name of the model
  * @param init the initial sub-model to display
  * @param integration sub-models which should be integrable
  *
  * @see [[ModelSyncProvider]]
  *
  * @author Rico Bergmann
  */
case class ModelConfig(name: String, init: Model, integration: Option[List[Model]])

/** Representation of a single model.
  *
  * Classes are expected to look like `package.ClassName`
  *
  * @param name the model's name
  * @param primaryClass the main class of the model (see [[DisplayableModel.getInstanceClass]]
  * @param additionalClasses derived classes of the model
  * @param image graphical (e.g. UML) representation
  * @param nested nested models
  *
  * @see [[DisplayableModel]]
  */
case class Model(name: String, primaryClass: String, additionalClasses: Option[List[String]], image: Option[String], nested: Option[List[Model]])

/** The companion provides some utility methods.
  */
object Model {

  /** Decomposes a model name into its package and class name components.
    */
  def parseClass(qualifiedName: String): (String, String) = {
    val lastSeparator = qualifiedName.lastIndexOf(".")
    val pckg = qualifiedName.substring(0, lastSeparator)
    val className = qualifiedName.substring(lastSeparator + 1, qualifiedName.length)
    (pckg, className)
  }

}

/** Exception to indicate that a (''JSON'') model does not contain the expected data for the
  * Generator to work correctly.
  */
class ModelDataException extends RuntimeException
