package org.rosi_project.model_sync.generator

import org.eclipse.emf.common.util.EList
import org.rosi_project.model_sync.generator.acr_model.SModel

/** Provides services to convert EMF ecore-models to corresponding instances of [[SModel]].
  *
  * @author Rico Bergmann
  */
package object conversion {

  /** Converts an EMF list instance to a corresponding Scala `List`.
    *
    * This converter is necessary as `EList` is an instance of `java.util.List` and may thus not be
    * used like a Scala list conveniently.
    *
    * @param eList the EMF list. May be `null`.
    * @tparam T the elements in the EMF list
    * @return the Scala list containing all the elements of the original list. Will be `null` if
    *         `elist` is.
    */
  implicit def elist2slist[T](eList: EList[T]): List[T] = {
    if (eList == null) {
      null
    } else {
      var res: List[T] = List()
      eList.forEach(elem => res = res :+ elem)
      res
    }
  }

}
