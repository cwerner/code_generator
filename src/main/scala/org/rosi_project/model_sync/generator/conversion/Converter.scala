package org.rosi_project.model_sync.generator.conversion

/** A converter converts instance of a source type `S` to a target type `T`.
  *
  * @tparam S the source type
  * @tparam T the target type
  * @author Rico Bergmann
  */
trait Converter[S, T] {

  /** Converts the source object.
    *
    * @param source the object to convert
    * @return the converted object
    */
  def convert(source: S, sourceName: String): T

}
