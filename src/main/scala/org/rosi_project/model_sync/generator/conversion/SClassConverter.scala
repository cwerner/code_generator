package org.rosi_project.model_sync.generator.conversion

import org.eclipse.emf.ecore.{EAttribute, EClass, EReference}
import org.rosi_project.model_sync.generator.acr_model._
import org.rosi_project.model_sync.generator.acr_model.types.SList
import org.rosi_project.model_sync.generator.acr_model.types.SSet
import org.rosi_project.model_sync.generator.PackageNames

/** Converter to generate instances of [[SClass]] based on [[EClass EClasses]].
  *
  * @author Rico Bergmann
  */
class SClassConverter extends Converter[EClass, SClass] {

  override def convert(source: EClass, sourceName: String): SClass = {
    var attrs: List[SAttribute] = List.empty
    var refs: List[SReference] = List.empty
    var pars: List[STypedElement] = List.empty

    // fetch the attributes of the SClass
    (source.getEAttributes: List[EAttribute]).foreach(eAttr => { 
      //println("Attribute: " +  eAttr.getEAttributeType.getName + " pkg: " + eAttr.getEAttributeType.getEPackage.getNsPrefix + " pre: " + PackageNames.sourcePkgPrefix)
      val attrType: STypedElement = STypeRegistry
        // check if the attribute type is already known and registered
        .query(eAttr.getEAttributeType.getName, PackageNames.sourcePkgPrefix + eAttr.getEAttributeType.getEPackage.getNsPrefix)
        // otherwise create a new type
        .getOrElse {
          val newAttr = EmfTypeTranslator
            // if the type is wrapped by EMF (such as EString), use the corresponding scala type
            .getSClassFromEmf(eAttr.getEAttributeType)
            // otherwise create a new class (as the attribute should instance of a class rather than a type in this case)
            .getOrElse(new SClass(eAttr.getEAttributeType.getName, PackageNames.sourcePkgPrefix + eAttr.getEAttributeType.getEPackage.getNsPrefix))

          // finally save the type
          STypeRegistry.addType(newAttr, null)
        }
      
      attrs ::= SAttribute(eAttr.getName, attrType, eAttr.isUnique())
    })

    //fetch the references of the SClass
    (source.getEReferences: List[EReference])
      .foreach(eRef => {
        val refName: String = eRef.getName //Attribute name       
        val refType: String = eRef.getEReferenceType.getName //Name of the attribute type
        val refTypePckg: String = PackageNames.sourcePkgPrefix + eRef.getEReferenceType.getEPackage.getNsPrefix //Package name of the attribute type
        val containment: Boolean = eRef.isContainment() //Containment relation or not
        val oppositeERef: EReference = eRef.getEOpposite() //Opposite reference
        var oppositeRef: SReference = null //Opposite reference
        val upperBound: Int = eRef.getUpperBound() //Upper bound
        val lowerBound: Int = eRef.getLowerBound() //Lower bound
        
        var newTypeCreated: Boolean = false
        
        //println("Referenz: " +  refType + " pkg: " + refTypePckg)

        var sRefType: STypedElement = STypeRegistry
          // check if the attribute type is already known and registered
          .query(refType, refTypePckg)
          // otherwise create a new type
          .getOrElse {
            newTypeCreated = true
            
            val newAttr = EmfTypeTranslator
              // if the type is wrapped by EMF (such as EString), use the corresponding scala type
              .getSClassFromEmf(refType)
              // otherwise create a new class (as the attribute should instance of a class rather than a type in this case)
              .getOrElse(new SClass(refType, refTypePckg))
              
            // finally save the type
            STypeRegistry.addType(newAttr, null)            
          }
        
        //handle opposite ref
        if (oppositeERef != null) {          
          //if the ref has an opposite
          var classRef = sRefType.asInstanceOf[SClass]
          if (!classRef.getReferences.isEmpty) {
            //opposite class type exists and has references
            classRef.getReferences.foreach { r =>
              if (r.getName == oppositeERef.getName) {
                oppositeRef = r
              }
            }
          }
        }

        if (eRef.getUpperBound > 1 || eRef.getUpperBound == -1) {
          sRefType = SSet(sRefType)
        }        
        
        var newReference = SReference(refName, sRefType, upperBound, lowerBound, containment, oppositeRef) 
        if (oppositeRef != null) {
          oppositeRef.oppositeRef = newReference
        }
        
        refs ::= newReference       
    })

    val eClassParents: List[EClass] = source.getESuperTypes.toArray(new Array[EClass](0)).toList
    
    eClassParents.foreach { p =>
      pars ::= STypeRegistry
        // check if we already know the parent
        .query(p.getName, PackageNames.sourcePkgPrefix + p.getEPackage.getNsPrefix)
        // otherwise we need to create and register it
        .getOrElse {
          val parentSClass: SClass = new SClass(p.getName, PackageNames.sourcePkgPrefix + p.getEPackage.getNsPrefix)
          // register the parent (it will be visited and completely inflated later on)
          STypeRegistry.addType(parentSClass, null)
          parentSClass
        }
    }

    /* `convert` may be called on two different occasions: either for a completely new type or
     * for a type that was already created before when another type was being inflated by `convert`.
     *  In the first case we need to set up all the necessary attributes of the SClass and therefore
     *  create it from scratch.
     *  Otherwise the most basic attributes (i.e. name and package) have already been set up and
     *  have to be left unchanged (they are values, actually). However parent and attributes of the
     *  type have not been inflated during the first visit (as the only important part then was to
     *  have the type known). Therefore we will set these now.
     */

    val currentClass: Option[STypedElement] = STypeRegistry.query(source.getName, PackageNames.sourcePkgPrefix + source.getEPackage.getNsPrefix)

    currentClass.map {
      case clazz: SClass =>
        clazz.setAttributes(attrs)
        clazz.setReferences(refs)
        pars.foreach(p => {
            clazz.addParent(p)
          })
        clazz
      case sType =>
        sys.error(s"sType should have been a class: $sType")
    }.getOrElse {
      var sClassType = SClassType.normalClass
      if (source.isAbstract) {
        sClassType = SClassType.abstactClass
      }
      if (source.isInterface) {
        sClassType = SClassType.normalTrait
      }
      val createdClass: SClass = new SClass(source.getName, PackageNames.sourcePkgPrefix + source.getEPackage.getNsPrefix, sClassType)
      createdClass.setAttributes(attrs)
      createdClass.setReferences(refs)
      pars.foreach(p => {
          createdClass.addParent(p)
        })
      STypeRegistry.addType(createdClass, null)
      createdClass
    }

  }

  /** Checks whether there is more than one parent class.
    *
    * This method is just to express the constraint more explicitly.
    */
  private def violatesSingleInheritance(parents: List[EClass]): Boolean = parents.length > 1

}
