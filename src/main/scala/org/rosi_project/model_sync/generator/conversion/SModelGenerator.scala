package org.rosi_project.model_sync.generator.conversion

import org.eclipse.emf.ecore.{ EClass, EGenericType, EPackage }
import org.rosi_project.model_sync.generator.acr_model.{ SModel, SType, STypeRegistry, ComplexSModel }

import scala.collection.JavaConverters._
import org.rosi_project.model_sync.generator.acr_model.SClass
import org.eclipse.emf.ecore.EEnum
import org.rosi_project.model_sync.generator.PackageNames
import org.rosi_project.model_sync.generator.acr_model.SClassType

/**
 * Converter to generate an [[SModel]] from ecore.
 *
 * @author Rico Bergmann
 */
class SModelGenerator extends Converter[EPackage, SModel] {

  override def convert(source: EPackage, sourceName: String): SModel = {
    //val packageName = if (source.getName != null) source.getName else ""
    var contents = source.eAllContents().asScala
    val model = new ComplexSModel(source.getName, sourceName, source.getNsURI)

    println("... Converting ecore model")
    println("First run creates all classes, important for abstract and interface relations")
    contents.foreach {
      case ec: EClass =>
        var sClassType = SClassType.normalClass
        if (ec.isAbstract) {
          sClassType = SClassType.abstactClass
        }
        if (ec.isInterface || PackageNames.multiInhertitanceWithTraits) {
          sClassType = SClassType.normalTrait
        }
        STypeRegistry.addType(new SClass(ec.getName, PackageNames.sourcePkgPrefix + ec.getEPackage.getNsPrefix, sClassType), ec)
      case ee: EEnum =>
        model.addModelEnums(new SEnumConverter convert (ee, sourceName))
      case _ =>
      // we only care about classes. Types will be registered as soon as they are needed as
      // attributes
    }
    contents = source.eAllContents().asScala
    println("Second run add references, attributes, and class hierarchies")
    contents.foreach {
      case ec: EClass =>
        //println(ec)
        model.addModelClass(new SClassConverter convert (ec, sourceName))
      case _ =>
      // we only care about classes. Types will be registered as soon as they are needed as
      // attributes
    }
    println("... Conversion finished")
    println(s"Generated model: $model")

    model
  }

}
