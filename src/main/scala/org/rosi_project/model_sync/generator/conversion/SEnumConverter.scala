package org.rosi_project.model_sync.generator.conversion

import org.eclipse.emf.ecore.EEnum
import org.rosi_project.model_sync.generator.acr_model.SEnum
import org.eclipse.emf.ecore.EEnumLiteral
import org.rosi_project.model_sync.generator.acr_model.STypeRegistry
import org.rosi_project.model_sync.generator.PackageNames

class SEnumConverter extends Converter[EEnum, SEnum] {

  override def convert(source: EEnum, sourceName: String): SEnum = {

    var strings: Set[String] = Set.empty 
    
    //fetch the name from the literals
    (source.getELiterals: List[EEnumLiteral]).foreach(elit => {
      strings += elit.getName
    }) 
    
    val createdEnum: SEnum = new SEnum(source.getName, PackageNames.sourcePkgPrefix + source.getEPackage.getNsPrefix, strings)
    STypeRegistry.addType(createdEnum, null)
    createdEnum
  }
}