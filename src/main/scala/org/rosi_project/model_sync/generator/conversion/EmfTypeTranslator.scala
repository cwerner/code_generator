package org.rosi_project.model_sync.generator.conversion

import java.util.Objects

import org.eclipse.emf.ecore.EDataType
import org.rosi_project.model_sync.generator.acr_model.SType
import org.rosi_project.model_sync.generator.acr_model.types.PredefTypes
import org.rosi_project.model_sync.generator.acr_model.types.PredefEcoreTypes

/** Service to map an instance of [[EDataType]] to its corresponding [[SType]] (which wrap Scala's
  * native types).
  *
  * @author Rico Bergmann
  */
object EmfTypeTranslator {

  private val typeMap: Map[String, SType] = Map(
    "Double" -> PredefTypes.Double, //map normal data types to its normal data types
    "Float" -> PredefTypes.Float,
    "Int" -> PredefTypes.Integer,
    "Char" -> PredefTypes.Char,
    "Byte" -> PredefTypes.Byte,
    "Boolean" -> PredefTypes.Boolean,
    "Long" -> PredefTypes.Long,
    "Short" -> PredefTypes.Short,
    "String" -> PredefTypes.String,      
    "EBoolean" -> PredefTypes.Boolean, //map EMF data types to its normal data types
    "EByte" -> PredefTypes.Byte,
    "EChar" -> PredefTypes.Char,
    "EDate" -> PredefTypes.Date,
    "EDouble" -> PredefTypes.Double,    
    "EFloat" -> PredefTypes.Float,
    "EInt" -> PredefTypes.Integer,
    "EJavaObject" -> PredefTypes.Object,
    "ELong" -> PredefTypes.Long,
    "EShort" -> PredefTypes.Short,
    "EString" -> PredefTypes.String,
    "EObject" -> PredefTypes.Object,
    "EStructuralFeature" -> PredefEcoreTypes.EcoreStructuralFeature,
    "EOperation" -> PredefEcoreTypes.EcoreOperation,
  )

  /** Maps an EMF data type to its corresponding Scala type.
    *
    * @param dataType the source type. May not be `null`.
    * @return the matching type. If there is none, the `Option` will be empty.
    */
  def getSClassFromEmf(dataType: EDataType): Option[SType] = {
    Objects.requireNonNull(dataType, "Data type may not be null")
    typeMap.get(dataType.getName)
  }

  /** Maps an EMF data type to its corresponding Scala type.
    *
    * @param dataType the source type. May not be `null`.
    * @return the matching type. If there is none, the `Option` will be empty.
    */
  def getSClassFromEmf(dataType: String): Option[SType] = {
    Objects.requireNonNull(dataType, "Data type may not be null")
    typeMap.get(dataType)
  }

}
