package org.rosi_project.model_sync.generator.conversion

import org.eclipse.emf.ecore.EPackage
import org.rosi_project.model_sync.generator.acr_model.SModel

/** Exception to indicate that a EMF model may not be converted to an [[SModel]] because some
  * constraints are violated.
  *
  * @author Rico Bergmann
  */
class UnconvertibleEmfException(val model: EPackage, val message: String = "") extends RuntimeException(message) {

  override def toString: String = s"UnconvertibleException: $model ${if (message.isEmpty) "" else s"($message)"}"

}
