package org.rosi_project.model_sync.generator

import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.emf.ecore.{ EObject, EPackage }
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl
import scala.collection.JavaConverters._
import java.io.File
import org.rosi_project.model_sync.generator.util.ClassAndInstance

/**
 * Simple service to load an ECORE model from a file.
 *
 * @author Rico Bergmann
 */
class EcoreLoader {

  /**
   * Fetches an ecore model from XML.
   *
   * @param path where to find the model
   * @return the model described by the XML
   */
  def loadEcore(path: String = "assets/ttc17.ecore"): ClassAndInstance = {
    require(null != path && path.nonEmpty)
    val resourceSet = new ResourceSetImpl()

    resourceSet.getResourceFactoryRegistry.getExtensionToFactoryMap.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl())

    //resourceSet.getResourceFactoryRegistry.getExtensionToFactoryMap.put("ecore", new EcoreResourceFactoryImpl())
    //resourceSet.getResourceFactoryRegistry.getExtensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl())
    val res = resourceSet.getResource(URI.createFileURI(path), true)
    require(null != res)
    require(!res.getContents.isEmpty)

    val f = new File(path.replace(".ecore", ".ttmodel"))
    if (f != null && f.exists()) {
      val univEPackage = res.getContents().get(0);
      resourceSet.getPackageRegistry().put("https://www.transformation-tool-contest.eu/2019/tt", univEPackage);
      val myModel = resourceSet.getResource(URI.createURI(path.replace(".ecore", ".ttmodel")), true);
      return new ClassAndInstance(res.getContents.toArray(new Array[EObject](0)).toList.find(_.isInstanceOf[EPackage]).map((p: EObject) => p.asInstanceOf[EPackage]).orNull,
          myModel.getContents().toArray(new Array[EObject](0)).toList.head)
    }
    return new ClassAndInstance(res.getContents.toArray(new Array[EObject](0)).toList.find(_.isInstanceOf[EPackage]).map((p: EObject) => p.asInstanceOf[EPackage]).orNull, null)
  }

}

/**
 * Exception to indicate that the model images may not be copied into the JAR.
 *
 * @param cause the causing exception
 */
class EcoreLoadException(cause: Exception) extends RuntimeException(cause)
