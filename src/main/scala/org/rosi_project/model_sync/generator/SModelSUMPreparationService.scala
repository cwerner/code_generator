package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.acr_model.SModel
import org.rosi_project.model_sync.generator.sync._

/** Add functionality for Queries and Views to the RSUM.
  *
  * @author Christopher Werner
  */
class SModelSUMPreparationService {
  
  /** Add functionality for Queries and Views to the RSUM.
    *
    * @param sModel the model to augment
    */
  def prepareModel(sModel: SModel): Unit = {
    val queryHelperVisitor = new GenerateQueryHelperVisitor
    val viewGenerationVisitor = new ViewGeneratingVisitor
    val queryGenerationVisitor = new QueryGeneratingVisitor

    //order is important !!!!
    sModel.accept(viewGenerationVisitor)
    sModel.accept(queryHelperVisitor)
    sModel.accept(queryGenerationVisitor)
  }
}