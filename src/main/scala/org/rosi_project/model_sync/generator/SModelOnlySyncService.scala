package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.acr_model.SModel
import org.rosi_project.model_sync.generator.sync.SyncEnhancingVisitor

/** Simple service to perform only extension and method statement add.
  *
  * @author Christopher Werner
  */
class SModelOnlySyncService {
  
  /** Augments a [[SModel]] with the necessary methods and statements to make it usable in a
    * synchronization context.
    *
    * @param sModel the model to augment
    */
  def prepareModel(sModel: SModel): Unit = {
    val syncNotificationVisitor = new SyncEnhancingVisitor
    sModel.accept(syncNotificationVisitor)    
  }
}