package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.sync.BasicTypeGetterSetterGeneratingVisitor
import org.rosi_project.model_sync.generator.acr_model.SModel

class SModelGetterSetterAttrPreparationService {
  
  /** Adds getter and setter for Attributes.
    *
    * @param sModel the model to augment
    */
  def prepareModel(sModel: SModel): Unit = {
    val getterSetterVisitor = new BasicTypeGetterSetterGeneratingVisitor   
    sModel.accept(getterSetterVisitor)
  }
}