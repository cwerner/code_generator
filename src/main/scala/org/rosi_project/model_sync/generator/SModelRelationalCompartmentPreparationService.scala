package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.acr_model.SModel
import org.rosi_project.model_sync.generator.sync._

/** Simple service to create relational compartments for each relation.
  *
  * @author Christopher Werner
  */
class SModelRelationalCompartmentPreparationService {
  
  /** Create relational compartments for each relation.
    *
    * @param sModel the model to augment
    */
  def prepareModel(sModel: SModel): Unit = {
    val relationCompartmentVisitor = new RelationCompartmentGeneratingVisitor
    sModel.accept(relationCompartmentVisitor)
  }
}