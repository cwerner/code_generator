package org.rosi_project.model_sync.generator

import org.rosi_project.model_sync.generator.sync.SumModelReadingVisitor
import org.rosi_project.model_sync.generator.acr_model.SModel

/** Simple service to create the new emf ecore model instances.
  *
  * @author Christopher Werner
  */
class SModelSUMReadEMFService {
  
  /** Add classes to read emf and ecore models.
    *
    * @param sModel the model to augment
    */
  def prepareModel(sModel: SModel, config: GeneratorConfig): Unit = {
    val sumModelReadingVisitor = new SumModelReadingVisitor(config)
    sModel.accept(sumModelReadingVisitor)    
  }
}