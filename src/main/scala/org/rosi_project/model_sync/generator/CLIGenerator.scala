package org.rosi_project.model_sync.generator

import java.{io => jio}

import scopt.OptionParser

/** Main entry into the application. It is intended to run as a console application and should be
  * called like the following: `generator [-o DIR] [-c] ECORE` with the following options:
  *
  *  - `-o | --outdir DIR` will write the generated `model.jar` to the directory `DIR`
  *  - `-c | --cleanup` will remove the generated `.scala` files and only keep the `.class` in the
  *   generated `.jar` file
  *  - `ECORE` should specify the ecore (XML) files which contains the model to create the scala
  *   files for
  *
  * @author Rico Bergmann
  */
object CLIGenerator extends App {

  val parser = new OptionParser[GeneratorConfig](programName="modelgen") {
    head("modelgen", "0.1")

    arg[Seq[jio.File]]("ecore").required().action( (ef, conf) =>
      conf.copy(sources = ef.map(f => f.getAbsolutePath))
    ).text("The ecore (XML) file of the model")

    opt[jio.File]('o', "outdir").optional().action( (dir, conf) =>
      conf.copy(outDir =  dir)
    ).text("The directory to place the generated model in (current dir by default)")

    opt[jio.File]('w', "workdir").optional().action( (dir, conf) =>
      conf.copy(workDir = dir)
    ).text("The directory to place the generated .scala files in (temp dir by default)")

    opt[Boolean]('c', "cleanup").optional().action( (c, conf) =>
      conf.copy(cleanUp = c)
    ).text("Remove the generated .scala files and only keep the compiled .class files")
    
    opt[jio.File]('j', "modelJoin").optional().action( (m, conf) =>
      conf.copy(modelJoin = m.getAbsolutePath)
    ).text("The description of a model join query to generate a view from.")
    
    opt[jio.File]('s', "syncLanguage").optional().action( (s, conf) =>
      conf.copy(syncLanguage = s.getAbsolutePath)
    ).text("The description of a synchronisation query to generate a synchronization behaviour from.")
  }

  parser.parse(args, GeneratorConfig()) match {
    case Some(config) =>
      new Generator(config).run()
    case None =>
  }

}
